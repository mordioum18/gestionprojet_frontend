/**
 * Created by hp on 22/11/17.
 */
import {Utilisateur} from './utilisateur';
import {Evenement} from './evenement';

export class ValidationActivite {
  id: number;
  dateCreation: any;
  dateModification: any;
  dateValidation: any;
  commentaire: string;
  documents: any;
  evenement: Evenement;
  utilisateur: Utilisateur;
}
