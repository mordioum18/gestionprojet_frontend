/**
 * Created by hp on 17/11/17.
 */
import {Projet} from './projet';
import {Activite} from './activite';
import {Contrainte} from './contrainte';
import {ValidationLot} from './validation-lot';
import {SuiviRessource} from './suivi-ressource';
import {LotOrganisation} from './lot-organisation';

export class Lot {
  id: number;
  dateExecutionPrevisionnelle: Date;
  dateExecutionReelle: Date;
  dateFinPrevisionnelle: Date;
  dateFinReelle: Date;
  dureeExecutionPrevisionnelle: number;
  dureeExecutionReelle: number;
  libelle: string;
  description: string;
  dateCreation: Date;
  dateModification: Date;
  budget: number;
  validationLots: ValidationLot[];
  activites: Activite[];
  lotDecaissements: any;
  lotOrganisations: LotOrganisation[];
  contraintes: Contrainte[];
  projet: Projet;
}
