/**
 * Created by hp on 22/11/17.
 */
import {Lot} from './lot';
import {Decaissement} from './decaissement';

export class LotDecaissement {
  pourcentage: number;
  lot: Lot;
  decaissement: Decaissement;
}
