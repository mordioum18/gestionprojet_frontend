/**
 * Created by hp on 17/11/17.
 */
import {Organisation} from './organisation';
import {ActiviteRessource} from './activite-ressource';

export class Utilisateur {
  id: number;
  prenom: string;
  nom: string
  telephone: string;
  email: string;
  username: string;
  profil: string;
  passwordPlain: string;
  dateCreation: any;
  dateModification: any;
  utilisateurEvenements: any;
  evenementsOrganises: any;
  projets: any;
  organisation: Organisation;
  activiteRessources: ActiviteRessource[];
}
