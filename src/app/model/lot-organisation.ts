/**
 * Created by hp on 22/11/17.
 */
import {Lot} from './lot';
import {Organisation} from './organisation';
import {LotOrganisationPk} from './lot-organisation-pk';

export class LotOrganisation {
  dateDebutAttribution: any;
  dateFinAttribution: any;
  pk: LotOrganisationPk;
}
