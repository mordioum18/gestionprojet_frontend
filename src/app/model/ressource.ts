import {RessourceGroupe} from './ressource-groupe';
import {Projet} from './projet';
import {ActiviteRessource} from './activite-ressource';
import {SuiviRessource} from './suivi-ressource';

export class Ressource {
  id: number;
  dateCreation: Date;
  dateModification: Date;
  nom: string;
  email: string;
  unite: any;
  quantitePrevue: number;
  typeRessource: any;
  projet: Projet;
  ressourceGroupe: RessourceGroupe;
  activiteRessources: ActiviteRessource[];
  disponibiliteAuPlusTot: Date
  disponibiliteAuPlusTard: Date;
  suiviRessources: SuiviRessource[];
}


