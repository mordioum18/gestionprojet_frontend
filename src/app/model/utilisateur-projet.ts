import {UtilisateurProjetPk} from "./utilisateur-projet-pk";

export class UtilisateurProjet {
  pk: UtilisateurProjetPk;
  role: string;
}
