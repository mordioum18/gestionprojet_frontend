/**
 * Created by hp on 22/11/17.
 */
import {Evenement} from './evenement';
import {Utilisateur} from './utilisateur';
import {Ressource} from './ressource';
import {Suivi} from './suivi';

export class SuiviRessource extends Suivi {
  ressource: Ressource;
  quantite: number;
}
