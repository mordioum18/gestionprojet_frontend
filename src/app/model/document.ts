/**
 * Created by hp on 22/11/17.
 */
import {Suivi} from './suivi';
import {Validation} from './validation';
import {Decaissement} from "./decaissement";
import {SuiviActivite} from './suivi-activite';

export class Document {
  id: number;
  dateCreation: Date;
  dateModification: Date;
  nom: string;
  chemin: string;
  extension: string;
  description: string;
  suivi: SuiviActivite;
  validation: Validation;
  decaissement: Decaissement;
  typeDocument: string;
}
