/**
 * Created by hp on 22/11/17.
 */
import {Utilisateur} from './utilisateur';
import {Activite} from "./activite";
import {Document} from "./document";

export class Decaissement {
  id: number;
  libelle: string;
  description: string;
  dateCreation: any;
  dateModification: any;
  montant: number;
  dateDecaissement: any;
  utilisateur: Utilisateur;
  activite: Activite;
  documents: Document[];

}
