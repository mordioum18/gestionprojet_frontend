import {Utilisateur} from './utilisateur';
import {Projet} from './projet';

export class UtilisateurProjetPk {
  utilisateur: Utilisateur;
  projet: Projet;
}
