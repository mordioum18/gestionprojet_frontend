/**
 * Created by hp on 17/11/17.
 */
import {TypeProjet} from './type-projet';
import {Utilisateur} from './utilisateur';
import {UtilisateurProjet} from "./utilisateur-projet";
import {Lot} from "./lot";
import {Financement} from "./financement";
import {Ressource} from './ressource';
import {RessourceGroupe} from './ressource-groupe';

export class Projet {
  id: number;
  nom: string;
  description: string;
  coutTotal: number;
  dateExecutionFinanciere: Date;
  dateDemarrageReelle: Date;
  dateDemarragePrevisionnelle: Date;
  dateFinPrevisionnelle: Date;
  dateFinRelle: Date;
  dureeExecutionPrevisionnelle: number;
  dureeExecutionReelle: number;
  dateReceptionPrevisionnelle: Date;
  dateReceptionReelle: Date;
  dateCreation: Date;
  dateModification: Date;
  statutProjet: string;
  projets: Projet[];
  financements: Financement[];
  utilisateurProjets: UtilisateurProjet[];
  lots: Lot[];
  evenements: any;
  projetParent: Projet;
  typeProjet: TypeProjet;
  createur: Utilisateur;
  ressources: Ressource[];
  ressourceGroupes: RessourceGroupe[];
}
