import {Lot} from './lot';
import {Organisation} from './organisation';

export class LotOrganisationPk {
  lot: Lot;
  organisation: Organisation;
}
