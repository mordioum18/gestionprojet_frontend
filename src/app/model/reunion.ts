/**
 * Created by hp on 22/11/17.
 */
import {Utilisateur} from './utilisateur';

export class Reunion {
  /*inheritance*/
  id: number;
  dateCreation: any;
  dateModification: any;
  dateDebut: any;
  dateFin: any;
  titre: string;
  description: string;
  participants: any;
  projets: any;
  utilisateur: Utilisateur;
}
