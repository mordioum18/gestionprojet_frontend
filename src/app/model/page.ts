/**
 * Created by hp on 17/11/17.
 */
import {Projet} from './projet';

export class Page {
  content: Projet[];
  totalElements: number;
  last: boolean;
  totalPages: number;
  sort: any;
  numberOfElements: number;
  first: boolean;
  size: number;
  number: number;
}
