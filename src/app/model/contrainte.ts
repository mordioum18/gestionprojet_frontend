/**
 * Created by hp on 22/11/17.
 */
import {Activite} from './activite';
import {Lot} from './lot';

export class Contrainte {
  id: number;
  libelle: string;
  description: string;
  dateCreation: any;
  dateModification: any;
  activite: Activite;
  lot: Lot;
  constructor() { }
}
