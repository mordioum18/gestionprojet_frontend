/**
 * Created by hp on 22/11/17.
 */
import {Utilisateur} from './utilisateur';
import {Evenement} from './evenement';
import {Activite} from "./activite";
import {Document} from "./document";

export class Suivi {
  id: number;
  dateCreation: Date;
  dateModification: Date;
  dateSuivi: Date;
  observation: string;
  utilisateur: Utilisateur;
  evenement: Evenement;
}
