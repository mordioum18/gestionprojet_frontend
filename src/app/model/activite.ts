/**
 * Created by hp on 17/11/17.
 */
import {Lot} from './lot';
import {LienTache} from './LienTache';
import {Decaissement} from "./decaissement";
import {SuiviActivite} from "./suivi-activite";
import {ValidationActivite} from './validation-activite';
import {Contrainte} from './contrainte';
import {ActiviteRessource} from './activite-ressource';

export class Activite {
  id: number;
  dateFinPrevisionnelle: Date;
  dateExecutionPrevisionnelle: Date;
  dateFinReelle: Date;
  dateExecutionReelle: Date;
  dureeExecutionPrevisionnelle: number;
  dureeExecutionReelle: number;
  libelle: string;
  description: string;
  budget: number;
  tauxExecutionPhysique: number;
  dateCreation: any;
  dateModification: any;
  statutActivite: any;
  suiviActivites: SuiviActivite[];
  validationActivites: ValidationActivite[];
  contraintes: Contrainte[];
  lot: Lot;
  activiteParent: Activite;
  successeurs: Activite[];
  decaissements: Decaissement[];
  debutAuPlustart: Date;
  activiteRessources: ActiviteRessource[];
}
