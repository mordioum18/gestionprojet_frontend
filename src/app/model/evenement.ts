/**
 * Created by hp on 22/11/17.
 */
import {Utilisateur} from './utilisateur';

export class Evenement {
  id: number;
  dateCreation: any;
  dateModification: any;
  dateDebut: any;
  dateFin: any;
  titre: string;
  description: string;
  utilisateurEvenements: any;
  projets: any;
  utilisateur: Utilisateur;
}
