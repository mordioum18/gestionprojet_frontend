/**
 * Created by hp on 22/11/17.
 */
import {Evenement} from './evenement';
import {Utilisateur} from './utilisateur';

export class ValidationLot {
  id: number;
  dateCreation: any;
  dateModification: any;
  dateValidation: any;
  commentaire: string;
  documents: any;
  evenement: Evenement;
  utilisateur: Utilisateur;
}
