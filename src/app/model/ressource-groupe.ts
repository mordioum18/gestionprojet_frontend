import {Ressource} from './ressource';
import {Projet} from './projet';

export class RessourceGroupe {
  id: number;
  dateCreation: Date;
  dateModification: Date;
  nom: string;
  projet: Projet;
  ressources: Ressource[];
}
