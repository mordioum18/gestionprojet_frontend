/**
 * Created by hp on 24/11/17.
 */
import {Utilisateur} from './utilisateur';
import {Evenement} from './evenement';

export class UtilisateurEvenement {
  etat: boolean;
  utilisateur: Utilisateur;
  evenement: Evenement;
}
