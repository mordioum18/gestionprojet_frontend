import {ActiviteRessourcePk} from './activite-ressource-pk';
import {Utilisateur} from './utilisateur';

export class ActiviteRessource {
  quantite: number;
  coutParUnite: number;
  roleDansLaTache: string;
  dateAssignation: Date; // date de cration de lassignation
  dateModification: Date; // date de modification des informations de lassignation
  pk: ActiviteRessourcePk;
  responsable: Utilisateur;
}
