import {Activite} from './activite';
import {Ressource} from './ressource';

export class ActiviteRessourcePk {
  activite: Activite;
  ressource: Ressource;
}
