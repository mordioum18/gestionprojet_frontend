/**
 * Created by hp on 22/11/17.
 */
import {Utilisateur} from './utilisateur';
import {Evenement} from './evenement';
import {Activite} from "./activite";
import {Suivi} from "./suivi";
import {Document} from './document';

export class SuiviActivite extends Suivi{
  activite: Activite;
  retard: number;
  niveauRealisation: number;
  documents: Document[];
}
