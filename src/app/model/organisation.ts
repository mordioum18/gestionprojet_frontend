import {LotOrganisation} from './lot-organisation';

/**
 * Created by hp on 22/11/17.
 */
export class Organisation {
  id: number;
  dateCreation: any;
  dateModification: any;
  raisonSociale: string;
  adresse: string;
  telephone: string;
  email: string;
  siteWeb: string;
  utilisateurs: any;
  lotOrganisations: LotOrganisation[];
}
