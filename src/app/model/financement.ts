/**
 * Created by hp on 22/11/17.
 */
import {Projet} from './projet';

export class Financement {
  id: number;
  dateCreation: Date;
  dateModification: Date;
  dateFinancement: Date
  montant: number;
  libelle: string;
  description: string;
  projet: Projet;
}
