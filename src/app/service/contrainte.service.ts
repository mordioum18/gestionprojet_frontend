import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ServerService} from './server.service';
import {UtilsService} from './utils.service';
import {Contrainte} from '../model/contrainte';
import {Observable} from 'rxjs/Observable';
import {Financement} from '../model/financement';

@Injectable()
export class ContrainteService {
  private _baseUrl: any;

  constructor(private http: HttpClient, private server: ServerService) {
    this._baseUrl = this.server.getBaseUrl();
  }

  postContrainte(projet: number, contrainte: Contrainte): Observable<Contrainte> {
    if (this.server.accessToken == null) {
      this.server.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this.server.accessToken)
      .set('projet', projet.toString());
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .post(this._baseUrl + '/api/contraintes', contrainte, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch(this.handleError);
  }

  updateContrainte(contrainte: Contrainte): Observable<Contrainte> {
    if (this.server.accessToken == null) {
      this.server.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this.server.accessToken)
      .set('projet', contrainte.lot.projet.id.toString());
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .put(this._baseUrl + '/api/contraintes/' + contrainte.id, contrainte, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch((e: any) => Observable.throw(this.handleError(e)));
  }

  updateContrainteWithConstraint(contrainte: Contrainte): Observable<Contrainte> {
    if (this.server.accessToken == null) {
      this.server.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this.server.accessToken)
      .set('projet', contrainte.activite.lot.projet.id.toString());
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .put(this._baseUrl + '/api/contraintes/' + contrainte.id, contrainte, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch((e: any) => Observable.throw(this.handleError(e)));
  }

  deleteContrainte(projet: number, contrainte: number): Observable<Contrainte> {
    if (this.server.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this.server.accessToken)
        .set('projet', projet.toString());
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .delete(this._baseUrl + '/api/contraintes/' + contrainte, {params: params, headers: headers, responseType: 'text'})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this.server.goToLogin();
    }
  }

  contraintesLot(projet: string, lot: string): Observable<Contrainte[]> {
    if (this.server.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this.server.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/lots/' + lot + '/contraintes', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this.server.goToLogin();
    }
  }

  contraintesActivite(projet: string, activite: string): Observable<Contrainte[]> {
    if (this.server.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this.server.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/activites/' + activite + '/contraintes', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this.server.goToLogin();
    }
  }

  private handleError(error: any): Observable<any> {
    console.log('#################### Constrains : errors ###################');
    return Observable.throw(error);
  }
}
