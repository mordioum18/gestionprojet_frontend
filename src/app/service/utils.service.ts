/**
 * Created by hp on 22/11/17.
 */
import {Injectable} from '@angular/core';
import {Lot} from '../model/lot';
import {Projet} from "../model/projet";

@Injectable()
export class UtilsService {

  constructor() {
  }
  date_diff_indays(initial: number, final: number): number {
    return Math.floor((final - initial) / (1000 * 60 * 60 * 24));
  }
  dateFormatTache(dateTime: any): string{
    const date = new Date(dateTime);
    let dd = date.getDate();
    let d = dd.toString();
    let mm = date.getMonth() + 1;
    let m = mm.toString();
    let yyyy = date.getFullYear();
    if(dd < 10) {
      d = '0' + dd;
    }
    if(mm < 10) {
      m = '0' + mm;
    }
    return yyyy + '-' + m + '-' + d;
  }
  dateFormatFr(dateTime: any): string {
    const date = new Date(dateTime);
    let dd = date.getDate();
    let d = dd.toString();
    let mm = date.getMonth() + 1;
    let m = mm.toString();
    let yyyy = date.getFullYear();
    if(dd < 10) {
      d = '0' + dd;
    }
    if(mm < 10) {
      m = '0' + mm;
    }
    return d + '-' + m + '-' + yyyy;
  }

  public lisibilite_nombre(nbr: number): string {
    const nombre = '' + nbr;
    let retour = '';
    let count = 0;
    for (let i = nombre.length - 1 ; i >= 0 ; i--) {
      if (count !== 0 && count % 3 === 0) {
        retour = nombre[i] + ' ' + retour ;
      }  else {
        retour = nombre[i] + retour ;
      }
      count++;
    }
    // alert('nb : '+nbr+' => '+retour);
    return retour;
  }
}
