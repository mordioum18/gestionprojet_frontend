/**
 * Created by hp on 22/11/17.
 */
import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import {Utilisateur} from '../model/utilisateur';

@Injectable()
export class PartageService {
  public header: string = null;
  public nomProjet: string = null;
  public idProjet: number = null;
  public utilisateur: Utilisateur = null;
  getHeader(): string{
    return this.header;
  }
  setHeader(header: string): void{
    this.header = header;
  }
  getNomProjet(): string{
    return this.nomProjet;
  }
  setNomProjet(nom: string): void{
    this.nomProjet = nom;
  }
  getIdProjet(): number{
    return this.idProjet;
  }
  setIdProjet(id: number): void{
    this.idProjet = id;
  }
}
