import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ServerService} from './server.service';
import {UtilsService} from './utils.service';
import {Observable} from 'rxjs/Observable';
import {RessourceGroupe} from '../model/ressource-groupe';

@Injectable()
export class RessourceGroupeService {
  private _baseUrl: any;

  constructor(private http: HttpClient,
              private utilsService: UtilsService,
              private _serverService: ServerService) {
    this._baseUrl = this._serverService.getBaseUrl();
  }

  getRessourceGroupes(projet: number): Observable<RessourceGroupe[]> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/projets/' + projet + '/ressource-groupes', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  postRessourceGroupe(ressourceGroupe: RessourceGroupe, projet: string): Observable<RessourceGroupe> {
    if (this._serverService.accessToken == null){
      this._serverService.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this._serverService.accessToken)
      .set('projet', projet);
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .post(this._baseUrl + '/api/ressources-groupes', ressourceGroupe, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch((e: any) => Observable.throw(this.handleError(e)));
  }

  getRessourceGroupe(id: string, projet: string): Observable<RessourceGroupe> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/ressources-groupes/' + id, {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  private handleError(error: any): Observable<any> {
    console.log('#################### erreur RESSOURCE-GROUPE ###################');
    return Observable.throw(error);
  }
}
