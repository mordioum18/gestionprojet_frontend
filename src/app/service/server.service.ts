/**
 * Created by hp on 22/11/17.
 */
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Response} from '@angular/http';
import {Token} from '../model/Token';

@Injectable()
export class ServerService {
  private serverName: string;
  private serverPort: string;
  private _clientId: string;
  private _secret: string;

  constructor(private router: Router) {
    this.serverName = 'http://localhost';
    this.serverPort = '8080';
    this._clientId = 'projet-isep-diamniadio';
    this._secret = 'secret';
  }
  getBaseUrl() {
    return this.serverName + ':' + this.serverPort;
  }
  goToLogin(){
    this.router.navigate(['login']);
  }


  get clientId(): string {
    return this._clientId;
  }

  get secret(): string {
    return this._secret;
  }

  get accessToken(): string {
    if (!localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return null;
    }
    const token: Token = JSON.parse(localStorage.getItem('token'));
    const now = new Date();
    if (now.getTime() > token.expires_at){
      this.router.navigate(['login']);
      return null;
    }
    return token.access_token;
  }

  /**
   * extracting data from service
   * @param res
   * @returns {any[]|string|Uint8ClampedArray|Data|ArrayBuffer|Observable<Data>|*}
   */
  public extractData(res: Response) {
    const body = res.json();
    localStorage.removeItem('server_error');
    return body;
  }

  /**
   * handling errors from  service
   * @param error
   * @returns {ErrorObservable}
   */
  public handleError(resp: Response) {
    let errorJson: any;

    if (resp) {
      errorJson = resp.json();
      const error = errorJson.error;
      if (error) {
        if ((error.equals('invalid_token'))) {
          localStorage.removeItem('currentUser');
          window.location.reload();
        }
      } else {
        console.log('########## Erreur serveur ########');
        localStorage.setItem('server_error', JSON.stringify({'existe': true}));
        window.location.reload();
      }

      return Observable.throw(errorJson);
    }

  }
}
