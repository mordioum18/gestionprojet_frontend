/**
 * Created by hp on 22/11/17.
 */
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {ServerService} from './server.service';
import 'rxjs/Rx';
import {Utilisateur} from '../model/utilisateur';

@Injectable()
export class UtilisateurService {
  private _baseUrl: any;

  constructor(private http: HttpClient, private _serverService: ServerService) {
    this._baseUrl = this._serverService.getBaseUrl();
  }
  getUtilisateurByToken(token: string): Observable<Utilisateur> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', token);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/utilisateurs-token', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  postUrl(email: string): Observable<Utilisateur>{
    const params = new HttpParams()
      .set('email', email);
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .post(this._baseUrl + '/utilisateurs-token', {}, {params: params, headers: headers, responseType: 'text'})
      .map(response => {
        return response;
      })
      .catch(this.handleError);
  }
  postUtilisateur(utilisateur: Utilisateur): Observable<Utilisateur>{
    if (this._serverService.accessToken == null){
      this._serverService.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this._serverService.accessToken);
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .post(this._baseUrl + '/api/utilisateurs', utilisateur, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch(this.handleError);
  }
  updateUtilisateur(utilisateur: Utilisateur, token: string): Observable<Utilisateur>{
    const params = new HttpParams()
      .set('access_token', token);
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .put(this._baseUrl + '/api/utilisateurs/' + utilisateur.id, utilisateur, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch((e: any) => Observable.throw(this.handleError(e)));
  }

  deleteUtilisateur(utilisateur_id: string): Observable<Utilisateur> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .delete(this._baseUrl + '/api/utilisateurs/' + utilisateur_id, {params: params, headers: headers, responseType: 'text'})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  logout(): Observable<Utilisateur> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .delete(this._baseUrl + '/api/logout', {params: params, headers: headers, responseType: 'text'})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  private handleError(error: any): Observable<any> {
    console.log('#################### erreur UTILISATEUR ###################');
    return Observable.throw(error);
  }
}
