import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {ServerService} from './server.service';
import 'rxjs/Rx';
import {SuiviRessource} from '../model/suivi-ressource';

@Injectable()
export class SuiviRessourceService {
  private _baseUrl: any;

  constructor(private http: HttpClient, private _serverService: ServerService) {
    this._baseUrl = this._serverService.getBaseUrl();
  }
  postSuiviRessource(suiviRessource: SuiviRessource): Observable<SuiviRessource> {
    if (this._serverService.accessToken == null) {
      this._serverService.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this._serverService.accessToken)
      .set('projet', suiviRessource.ressource.projet.id.toString());
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .post(this._baseUrl + '/api/suivi-ressources', suiviRessource, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch(this.handleError);
  }
  updateSuiviRessource(suiviRessource: SuiviRessource): Observable<SuiviRessource> {
    if (this._serverService.accessToken == null) {
      this._serverService.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this._serverService.accessToken)
      .set('projet', suiviRessource.ressource.projet.id.toString());
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .put(this._baseUrl + '/api/suivi-ressources/' + suiviRessource.id, suiviRessource, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch((e: any) => Observable.throw(this.handleError(e)));
  }

  deleteSuiviRessource(id: number, projetId: number): Observable<SuiviRessource> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projetId.toString());
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .delete(this._baseUrl + '/api/suivi-ressources/' + id, {params: params, headers: headers, responseType: 'text'})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  private handleError(error: any): Observable<any> {
    console.log('#################### erreur SUIVI RESSOURCE ###################');
    return Observable.throw(error);
  }

}
