/**
 * Created by hp on 22/11/17.
 */
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {ServerService} from './server.service';
import 'rxjs/Rx';
import {Enum} from '../model/Enum';

@Injectable()
export class EnumService {
  private _baseUrl: any;

  constructor(private http: HttpClient, private _serverService: ServerService) {
    this._baseUrl = this._serverService.getBaseUrl();
  }
  getRoles(): Observable<Enum[]> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/roles', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  getStatuts(): Observable<Enum[]> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/statuts', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  getRessourceTypes(): Observable<Enum[]> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/ressource-types', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  getRessourceUnites(): Observable<Enum[]> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/ressource-unites', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  private handleError(error: any): Observable<any> {
    console.log('#################### erreur ENUMS ###################');
    return Observable.throw(error);
  }
}
