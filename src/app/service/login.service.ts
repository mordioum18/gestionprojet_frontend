/**
 * Created by hp on 22/11/17.
 */
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {ServerService} from './server.service';
import 'rxjs/Rx';
import {Token} from '../model/Token';

@Injectable()
export class LoginService {
  private _baseUrl: string;

  constructor(private http: HttpClient, private _serverService: ServerService) {
    this._baseUrl = this._serverService.getBaseUrl();
  }
  login(username: string, password: string): Observable<Token> {
    const params = new HttpParams()
                    .set('grant_type', 'password')
                    .set('username', username)
                    .set('password', password);
    const auth = 'Basic ' + btoa('projet-isep-diamniadio' + ':' + 'secret');
    const headers = new HttpHeaders().set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json')
      .set('authorization', auth);
    return this.http
      .post(this._baseUrl + '/oauth/token', {}, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Observable<any> {
    return Observable.throw(error.message || error);
  }
}
