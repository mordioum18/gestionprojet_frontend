/**
 * Created by hp on 22/11/17.
 */
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {ServerService} from './server.service';
import 'rxjs/Rx';
import {Financement} from '../model/financement';

@Injectable()
export class FinancementService {
  private _baseUrl: any;

  constructor(private http: HttpClient, private _serverService: ServerService) {
    this._baseUrl = this._serverService.getBaseUrl();
  }
  postFinancement(projet: number, financement: Financement): Observable<Financement>{
    if (this._serverService.accessToken == null){
      this._serverService.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this._serverService.accessToken)
      .set('projet', projet.toString());
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .post(this._baseUrl + '/api/financements', financement, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch(this.handleError);
  }
  updateFinancement(financement: Financement): Observable<Financement>{
    if (this._serverService.accessToken == null){
      this._serverService.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this._serverService.accessToken)
      .set('projet', financement.projet.id.toString());
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .put(this._baseUrl + '/api/financements/' + financement.id, financement, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch((e: any) => Observable.throw(this.handleError(e)));
  }

  deleteFinancement(projet: number, financement: number): Observable<Financement> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet.toString());
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .delete(this._baseUrl + '/api/financements/' + financement, {params: params, headers: headers, responseType: 'text'})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  private handleError(error: any): Observable<any> {
    console.log('#################### erreur FINANCEMENT ###################');
    return Observable.throw(error);
  }
}
