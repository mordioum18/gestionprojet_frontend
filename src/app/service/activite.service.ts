/**
 * Created by hp on 22/11/17.
 */
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {ServerService} from './server.service';
import {UtilsService} from './utils.service';
import 'rxjs/Rx';
import {Activite} from '../model/activite';
import {Link} from '../model/link';
import {LienTache} from '../model/LienTache';
import {Task} from '../model/task';
import {Decaissement} from '../model/decaissement';
import {SuiviActivite} from '../model/suivi-activite';
import {Ressource} from '../model/ressource';
import {ActiviteRessource} from '../model/activite-ressource';
import {Lot} from '../model/lot';
import {Contrainte} from '../model/contrainte';

@Injectable()
export class ActiviteService {
  private _baseUrl: any;

  constructor(private http: HttpClient,
              private utilsService: UtilsService,
              private _serverService: ServerService) {
    this._baseUrl = this._serverService.getBaseUrl();
  }
  getAllActivites(projet: number): Observable<Activite[]> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet.toString());
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/projets/' + projet + '/activites', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  getSuccesseurs(projet: number, activite: number): Observable<Activite[]> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet.toString());
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/activites/' + activite + '/successeurs', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  getLiens(projet: number): Observable<LienTache[]> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/projets/' + projet + '/liens', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  getTasks(projet: number): Observable<any> {
    return Observable.forkJoin(
      this.createData(projet),
      this.createLink(projet)
    ).map(([data, links]) =>{
          return {data: data, links: links};
      }
    );
  }
  getLots(projet: number): Observable<Lot[]> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/projets/' + projet + '/lots', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  createData(projet: number): Observable<any>{
    return Observable.forkJoin(
      this.getLots(projet),
      this.getAllActivites(projet)
    ).map(([lots, activites]) =>{
      const data = [];
      for (const lot of lots){
        const debutLot = this.utilsService.dateFormatTache(lot.dateExecutionPrevisionnelle);
        const finLot = this.utilsService.dateFormatTache(lot.dateFinPrevisionnelle);
        const task = {id: 'lot-' + lot.id, text: lot.libelle, type: 'lot', start_date: debutLot, end_date: finLot, parent: 'projet'};
        data.push(task);
      }
      for (const activite of activites){
        const task = new Task();
        task.id = activite.id;
        task.start_date = this.utilsService.dateFormatTache(activite.dateExecutionPrevisionnelle);
        task.end_date = this.utilsService.dateFormatTache(activite.dateFinPrevisionnelle);
        task.text = activite.libelle;
        task.type = 'activite';
        task.progress = activite.tauxExecutionPhysique / 100;
        if (activite.activiteParent){
          task.parent = activite.activiteParent.id;
        }
        else {
          task.parent = 'lot-' + activite.lot.id;
        }
        data.push(task);
      }
      return data;
      }
    );
  }
  createLink(projet: number): Observable<any>{
    const links = [];
    let i = 1;
    return this.getLiens(projet).map(
      liens => {
        for (const lienTache of liens){
          const link = new Link();
          link.id = i++;
          link.source = lienTache.pk.predecesseur.id;
          link.target = lienTache.pk.successeur.id;
          link.type = lienTache.codeType;
          links.push(link);
        }
        return links;
      }
    );
  }
  postActivite(projet: string, activite: Activite): Observable<Activite>{
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .post(this._baseUrl + '/api/activites', activite, {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    }
    else {
      this._serverService.goToLogin();
    }
  }
  postLien(projet: string, predecesseur: string, successeur: string, type: string): Observable<Activite>{
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet)
        .set('predecesseur', predecesseur)
        .set('successeur', successeur)
        .set('type', type);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .post(this._baseUrl + '/api/activites/liens', {}, {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    }
    else {
      this._serverService.goToLogin();
    }
  }
  deleteLien(projet: string, predecesseur: string, successeur: string): Observable<Activite>{
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet)
        .set('predecesseur', predecesseur)
        .set('successeur', successeur);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .delete(this._baseUrl + '/api/activites/liens', {params: params, headers: headers, responseType: 'text'})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    }
    else {
      this._serverService.goToLogin();
    }
  }
  updateActivite(projet: string, activite: Activite): Observable<Activite>{
    if (this._serverService.accessToken == null){
      this._serverService.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this._serverService.accessToken)
      .set('projet', projet);
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .put(this._baseUrl + '/api/activites/' + activite.id, activite, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch((e: any) => Observable.throw(this.handleError(e)));
  }
  getActivitesById(projet: string, activite_id: string): Observable<Activite> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/activites/' + activite_id, {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  deleteActivite(projet: string, activite_id: string): Observable<Activite> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .delete(this._baseUrl + '/api/activites/' + activite_id, {params: params, headers: headers, responseType: 'text'})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  getDecaissements(projet: number, activite: number): Observable<Decaissement[]> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet.toString());
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/activites/' + activite + '/decaissements', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  getSuivis(projet: number, activite: number): Observable<SuiviActivite[]> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet.toString());
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/activites/' + activite + '/suivis', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  getActiviteFilles(projet: number, activite: number): Observable<Activite[]> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet.toString());
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/activites/' + activite + '/activites', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  assignRessourceToActivite(idActivite: string, ressource: ActiviteRessource, projet: string): Observable<ActiviteRessource> {
   /* if (this._serverService.accessToken == null){
      this._serverService.goToLogin();
      return;
    }*/
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .post(this._baseUrl + '/api/activites/' + idActivite + '/ressource', ressource, {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch(this.handleError);
    } else {
      this._serverService.goToLogin();
    }

  }

  updateAssignRessourceToActivite(idActivite: string, ressource: ActiviteRessource, projet: string): Observable<ActiviteRessource> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .put(this._baseUrl + '/api/activites/' + idActivite + '/ressource', ressource, {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch(this.handleError);
    } else {
      this._serverService.goToLogin();
    }

  }

  getActiviteRessources(id: string, projet: string): Observable<ActiviteRessource[]> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/activites/' + id + '/activite-ressources', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }


  getRessources(id: string, projet: string): Observable<Ressource[]> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/activites/' + id + '/ressources', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  private handleError(error: any): Observable<any> {
    console.log('#################### erreur ACTIVITE ###################');
    return Observable.throw(error);
  }
}
