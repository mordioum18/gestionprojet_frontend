/**
 * Created by hp on 22/11/17.
 */
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {ServerService} from './server.service';
import 'rxjs/Rx';
import {Lot} from '../model/lot';
import {Activite} from "../model/activite";
import {Contrainte} from '../model/contrainte';
import {Organisation} from '../model/organisation';
import {LotOrganisation} from '../model/lot-organisation';

@Injectable()
export class LotService {
  private _baseUrl: any;

  constructor(private http: HttpClient, private _serverService: ServerService) {
    this._baseUrl = this._serverService.getBaseUrl();
  }
  getLots(projet: number): Observable<Lot[]> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/projets/' + projet + '/lots', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }
  getActivites(projet: string, lot: string): Observable<Activite[]> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/lots/' + lot + '/activites', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  assignOrganisationToLot(idLot: string, organisation: LotOrganisation, projet: string): Observable<LotOrganisation> {
    if (this._serverService.accessToken == null){
      this._serverService.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this._serverService.accessToken)
      .set('projet', projet);
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .post(this._baseUrl + '/api/lots/' + idLot + '/organisation', organisation, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch((e: any) => Observable.throw(this.handleError(e)));
  }

  getLotOrganisations(id: string, projet: string): Observable<LotOrganisation[]> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/lots/' + id + '/lots-organisations', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  getOrganisations(id: string, projet: string): Observable<Organisation[]> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/lots/' + id + '/organisations', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  postLot(projet: string, lot: Lot): Observable<Lot>{
    if (this._serverService.accessToken == null){
      this._serverService.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this._serverService.accessToken)
      .set('projet', projet);
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .post(this._baseUrl + '/api/lots', lot, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch((e: any) => Observable.throw(this.handleError(e)));
  }
  getLot(projetId: string, id: string): Observable<Lot> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projetId);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/lots/' + id, {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  updateLot(projet: string, lot: Lot): Observable<Lot>{
    if (this._serverService.accessToken == null){
      this._serverService.goToLogin();
      return;
    }
    const params = new HttpParams()
      .set('access_token', this._serverService.accessToken)
      .set('projet', projet)
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'Application/json')
      .set('Accept', 'Application/json');
    return this.http
      .put(this._baseUrl + '/api/lots/' + lot.id, lot, {params: params, headers: headers})
      .map(response => {
        return response;
      })
      .catch((e: any) => Observable.throw(this.handleError(e)));
  }

  deleteLot(projet: string, lot_id: string): Observable<Lot> {
    if (this._serverService.accessToken != null){
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .delete(this._baseUrl + '/api/lots/' + lot_id, {params: params, headers: headers, responseType: 'text'})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  getContraintes(projet: string, lot: string): Observable<Contrainte[]> {
    if (this._serverService.accessToken != null) {
      const params = new HttpParams()
        .set('access_token', this._serverService.accessToken)
        .set('projet', projet);
      const headers = new HttpHeaders();
      headers.set('Content-Type', 'Application/json')
        .set('Accept', 'Application/json');
      return this.http
        .get(this._baseUrl + '/api/lots/' + lot + '/contraintes', {params: params, headers: headers})
        .map(response => {
          return response;
        })
        .catch((e: any) => Observable.throw(this.handleError(e)));
    } else {
      this._serverService.goToLogin();
    }
  }

  private handleError(error: any): Observable<any> {
    console.log('#################### erreur LOT ###################');
    return Observable.throw(error);
  }
}
