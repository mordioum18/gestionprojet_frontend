import {Routes} from '@angular/router';

import {Dashboard1Component} from "./views/dashboards/dashboard1.component";
import {Dashboard2Component} from "./views/dashboards/dashboard2.component";
import {Dashboard3Component} from "./views/dashboards/dashboard3.component";
import {Dashboard4Component} from "./views/dashboards/dashboard4.component";
import {Dashboard41Component} from "./views/dashboards/dashboard41.component";
import {Dashboard5Component} from "./views/dashboards/dashboard5.component";
import {ProjetComponent} from "./views/dashboards/projet/projet.component";
import {ProjetDetailComponent} from "./views/dashboards/projet-detail/projet.detail.component";
import {ProjetSuiviComponent} from "./views/dashboards/projet-suivi/projet.suivi.component";
import {AnalyseProjetComponent} from "./views/dashboards/analyse/analyse.projet.component";

import {StarterViewComponent} from "./views/appviews/starterview.component";
import {LoginComponent} from "./views/appviews/login/login.component";
import {RegisterComponent} from "./views/appviews/register/register.component";

import {BlankLayoutComponent} from "./components/common/layouts/blankLayout.component";
import {BasicLayoutComponent} from "./components/common/layouts/basicLayout.component";
import {TopNavigationLayoutComponent} from './components/common/layouts/topNavigationLayout.component'
import {ProjetImpressionComponent} from './views/dashboards/projet/projet-print/projet-print.component';
import {LotDetailComponent} from './views/dashboards/lot-detail/lot-detail.component';
import {WelcomeComponent} from './views/appviews/welcome/welcome.component';
import {ProjetPrintComponent} from './views/dashboards/projet-print/projet-print.component';

export const ROUTES: Routes = [
  // Main redirect
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  // {path: '', redirectTo: 'starterview', pathMatch: 'full'},

  {
    path: 'dashboards', component: BasicLayoutComponent,
    children: [
      {path: 'dashboard1', component: Dashboard1Component},
      {path: 'dashboard2', component: Dashboard2Component},
      {path: 'dashboard3', component: Dashboard3Component},
      {path: 'dashboard4', component: Dashboard4Component},
      {path: 'dashboard5', component: Dashboard5Component},
    ]
  },
  {
    path: 'welcome', component: BasicLayoutComponent,
    children: [
      {path: '', component: WelcomeComponent},
    ]
  },
  {
    path: 'dashboards', component: TopNavigationLayoutComponent,
    children: [
      {path: 'dashboard41', component: Dashboard41Component},
      {path: 'projets', component: ProjetComponent},
      {path: 'projets/:id', component: ProjetDetailComponent },
      {path: 'projet-suivi/:id', component: ProjetSuiviComponent },
      {path: 'projet-analyse/:id', component: AnalyseProjetComponent },
      {path: 'lots/:projetId/:lotId', component: LotDetailComponent }
    ]
  },
  {path: 'imprimer-projet', component: ProjetImpressionComponent },
  {path: 'projet-print/:id', component: ProjetPrintComponent },
  {
    path: '', component: BasicLayoutComponent,
    children: [
      {path: 'starterview', component: StarterViewComponent}
    ]
  },
  {
    path: '', component: BlankLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'register/:access', component: RegisterComponent },
    ]
  },

  // Handle all other routes
  {path: '**',  redirectTo: 'login'}
  // {path: '**',  redirectTo: 'starterview'}
];
