import { NgModule } from '@angular/core';
import {RouterModule} from "@angular/router";
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {LOCALE_ID} from '@angular/core';

import {ROUTES} from "./app.routes";
import { AppComponent } from './app.component';

// App views
import {DashboardsModule} from "./views/dashboards/dashboards.module";
import {AppviewsModule} from "./views/appviews/appviews.module";
import {HttpClientModule} from '@angular/common/http';
import {ServerService} from './service/server.service';

// App modules/components
import {LayoutsModule} from './components/common/layouts/layouts.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    DashboardsModule,
    LayoutsModule,
    AppviewsModule,
    RouterModule.forRoot(ROUTES)
  ],
  // providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  providers: [{provide: LOCALE_ID, useValue: 'fr-FR'}, ServerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
