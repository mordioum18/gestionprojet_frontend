import { Component, Input, OnInit} from '@angular/core';
import {Color, BaseChartDirective} from 'ng2-charts';

@Component({
  selector: 'app-chart',
  templateUrl: './bar-chart.component.html'
})
export class BarChartComponent implements OnInit{
  @Input() chartLabels: string[];
  @Input() chartType: string;
  chartLegend: boolean = true;
  chartOptions: any;

  @Input() chartData: any[];
  ngOnInit(): void {
   this.chartOptions = {
      scaleShowVerticalLines: false,
      responsive: true,
      maintainAspectRatio: false,
      scales : {
        xAxes: [
          {
            display: true
          }
          ],
         yAxes: [{
           scaleLabel: {
             display: true,
             labelString: 'Evolution'
           },
           ticks: {
             steps : 10,
             stepValue : 10,
             min: 0,
             max : 100,
             callback: function(value, index, values) {
               return value + '%';
             }
           }
         }]
       }
    };
  }

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }

}
