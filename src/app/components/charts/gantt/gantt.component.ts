import {Component, ElementRef, OnInit, OnChanges, Input, OnDestroy, ViewChild, AfterViewInit} from '@angular/core';

import 'dhtmlx-gantt';
import {} from '@types/dhtmlxgantt';
import 'dhtmlx-gantt/codebase/locale/locale_fr';
import 'dhtmlx-gantt/codebase/ext/dhtmlxgantt_tooltip';
import 'dhtmlx-gantt/codebase/ext/dhtmlxgantt_smart_rendering';
import 'dhtmlx-gantt/codebase/ext/dhtmlxgantt_quick_info';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Task} from '../../../model/task';
import {UtilsService} from '../../../service/utils.service';
import {ActiviteService} from '../../../service/activite.service';
import {LotService} from '../../../service/lot.service';
import {ProjetService} from '../../../service/projet.service';
import {Activite} from '../../../model/activite';
import {Projet} from '../../../model/projet';
import {Lot} from '../../../model/lot';
declare var jQuery:any;

@Component({
  selector: 'app-gantt',
  styleUrls: ['./gantt.component.css'],
  templateUrl: './gantt.component.html',
  providers: [ProjetService, LotService, ActiviteService]
})
export class GanttComponent implements OnInit, OnChanges, OnDestroy,AfterViewInit {
  @ViewChild('gantt_here') ganttContainer: ElementRef;
  @Input() tasks: any;
  @Input() projet: Projet;
  activite: Activite = new Activite();
  activiteSelected: Activite;
  lot: Lot = new Lot();
  modalShowActivite = false;
  titreModalActivite: string;
  titreModalLot: string;
  taskSelectedId: string;
  nombreElement: number;
  constructor(public ngxSmartModalService: NgxSmartModalService,
              private utilsService: UtilsService,
              private projetService: ProjetService,
              private lotService: LotService,
              private activiteService: ActiviteService) {
  }

  showForm(){
    this.modalShowActivite = true;
  }
  addActivite(activite): void{
    if (gantt.isTaskExists(activite.id)){
      const task = gantt.getTask(activite.id);
      task.text = activite.libelle;
      task.progress = activite.tauxExecutionPhysique / 100;
      task.start_date = new Date(activite.dateExecutionPrevisionnelle);
      task.end_date = new  Date(activite.dateFinPrevisionnelle);
      if (activite.activiteParent){
        task.parent = activite.activiteParent.id;
      }
      else {
        task.parent = 'lot-' + activite.lot.id;
      }
      gantt.updateTask(activite.id.toString());
      gantt.refreshData();
      if (task.$source.length > 0){
        const $source = task.$source;
        for (let i = 0; i < $source.length; i++) {
          this.updateSuccesseur(gantt.getLink($source[i].toString()).target.toString());
        }
      }

    }
    else{
      const tache = new Task();
      tache.id = activite.id;
      tache.start_date = this.utilsService.dateFormatTache(activite.dateExecutionPrevisionnelle);
      tache.end_date = this.utilsService.dateFormatTache(activite.dateFinPrevisionnelle);
      tache.text = activite.libelle;
      tache.type = 'activite';
      tache.progress = activite.tauxExecutionPhysique / 100;
      if (activite.activiteParent){
        tache.parent = activite.activiteParent.id;
      }
      else {
        tache.parent = 'lot-' + activite.lot.id;
      }
      this.nombreElement++;
      gantt.addTask(tache, tache.parent);
    }
    this.updateLot(activite.lot.id);
    this.updateProjet(activite.lot.projet.id);
  }
  addLot(lot: Lot): void{
    if (gantt.isTaskExists('lot-' + lot.id)){
      const task = gantt.getTask('lot-' + lot.id);
      task.text = lot.libelle;
      task.start_date = new Date(lot.dateExecutionPrevisionnelle);
      task.end_date = new  Date(lot.dateFinPrevisionnelle);
      task.parent = 'projet';
      gantt.updateTask('lot-' + lot.id);
      gantt.refreshData();
    }
    else{
      const debutLot = this.utilsService.dateFormatTache(lot.dateExecutionPrevisionnelle);
      const finLot = this.utilsService.dateFormatTache(lot.dateFinPrevisionnelle);
      const tache = {id: 'lot-' + lot.id, text: lot.libelle, type: 'lot', start_date: debutLot, end_date: finLot, parent: 'projet'};
      this.nombreElement++;
      gantt.addTask(tache, tache.parent);
    }
  }
  updateLot(idLot: number){
    this.lotService.getLot(this.projet.id.toString(), idLot.toString())
      .subscribe(lot => {
          for (let i = 0; i < this.projet.lots.length; i++){
            if (this.projet.lots[i].id === lot.id){
              this.projet.lots[i].dateExecutionPrevisionnelle = lot.dateExecutionPrevisionnelle;
              this.projet.lots[i].dateFinPrevisionnelle = lot.dateFinPrevisionnelle;
              this.projet.lots[i].budget = lot.budget;
              if (gantt.isTaskExists('lot-' + lot.id)){
                const task = gantt.getTask('lot-' + lot.id);
                task.text = lot.libelle;
                task.start_date = new Date(lot.dateExecutionPrevisionnelle);
                task.end_date = new  Date(lot.dateFinPrevisionnelle);
                gantt.updateTask('lot-' + lot.id);
                gantt.refreshData();
              }
              break;
            }
          }
        },
        error => {
          console.log('***************erreur***********');
        }
      );
  }
  updateProjet(idProjet: number){
    this.projetService.getProjetsById(idProjet.toString())
      .subscribe(projet => {
          this.projet.dateFinPrevisionnelle = projet.dateFinPrevisionnelle;
          this.projet.coutTotal = projet.coutTotal;
          if (gantt.isTaskExists('projet')){
            const task = gantt.getTask('projet');
            task.text = this.projet.nom;
            task.start_date = new Date(this.projet.dateDemarragePrevisionnelle);
            task.end_date = new  Date(this.projet.dateFinPrevisionnelle);
            gantt.updateTask('projet');
            gantt.refreshData();
          }
        },
        error => {
          console.log('***************erreur***********');
        }
      );
  }
  updateActivite(activite: Activite){
    const $this = this;
    $this.activiteService.updateActivite(this.projet.id.toString(), activite)
      .subscribe(act => {
          for(let i = 0; i < this.projet.lots.length; i++) {
            if (this.projet.lots[i].id === act.lot.id){
              for(let k = 0; k < this.projet.lots[i].activites.length; k++){
                if (this.projet.lots[i].activites[k].id === act.id){
                  this.projet.lots[i].activites.splice(k, 1);
                  this.projet.lots[i].activites.splice(k, 0, act);
                  break;
                }
              }
            }
          }
          $this.addActivite(act);
        },
        error => {
          gantt.alert('activité non supprimée');
          return false;
        }
      );
  }
  updateSuccesseur(successeur_id: string){
    this.activiteService.getActivitesById(this.projet.id.toString(), successeur_id)
      .subscribe(act => {
          this.addActivite(act);
        },
        error => {
          gantt.alert('activité non supprimée');
          return false;
        }
      );
  }

  ngOnInit(){
    const $this = this;
    gantt.config.xml_date = '%Y-%m-%d %H:%i';
    gantt.config.date_grid = '%d-%m-%Y %H:%i';
    // configuration echelle
    gantt.config.scale_unit = 'day';
    gantt.config.step = 1;
    // configuration drag and drop grid
    gantt.config.order_branch = false;
    // congfiguration drag and drop task
    gantt.config.drag_move = false;
    gantt.config.drag_resize = false;
    gantt.config.drag_progress = false;
    // configuration unité de durée
    gantt.config.duration_unit = 'day';
    gantt.config.duration_step = 1;
    // configuration de la hauteeur de l 'echelle temps
    gantt.config.scale_height = 100;
    // confifuration des niveau de l'echelle
    gantt.config.subscales = [
      {unit: 'week', step:1, date: '%W ème semaine'},
      {unit: 'month', step:1, date: '%F'},
      {unit: 'year', step:1, date: '%Y'},
    ];
    gantt.config.fit_tasks = true;
    // configuration min durée
    gantt.config.min_duration = 24*60*60*1000;
    // ouvrir les tries
    gantt.config.open_tree_initially = true;
    // activer le mode lecture seul
    gantt.config.readonly = false;
    gantt.config.quick_info_detached = true;
    gantt.config.columns = [
      {name:"text",       label:"Activité",  width:300, tree:true },
      {name:"duration",   label:"Durée", width: 200, align: "left" }
    ];
    gantt.templates.task_text=function(start, end, task){
      if (task.type =='jalon'){
        return "";
      }
      else {
        if (task.id.toString() === 'projet' || task.id.toString().match('^lot-[0-9]{1,}$')){
          return task.text;
        }
        else {
          const progression: number = parseFloat(task.progress);
          return (progression * 100 ) + '%';
        }
      }
    };
    gantt.templates.task_class=function(start, end, task){
      if (task.id.toString() === 'projet'){
        return 'task_projet';
      }
      else {
        if (task.id.toString().match('^lot-[0-9]{1,}$')){
          return 'task_lot';
        }
        else {
          if (task.id.toString().match('^jalon-[0-9]{1,}$')){
            return 'task_jalon_class';
          }
          else {
            if (!task.parent.toString().match('^lot-[0-9]{1,}$')){
              return 'sub_task';
            }
            else {
              return '';
            }
          }
        }
      }
    };
    // CSS en tete tableau des tache
    gantt.templates.grid_header_class = function(columnName, column){
      return "grid_header_class";
    };
    // CSS ligne tableau
    gantt.templates.grid_row_class = function(start, end, task){
      return 'ganttTable';
    };
    // Css  des liens
    gantt.templates.link_class = function(link){
      return "";
    };
    gantt.templates.progress_text = function(start, end, task){
      return "";
    };
    gantt.templates.leftside_text = function(start, end, task){
      return $this.utilsService.dateFormatFr(start.getTime());
    };
    gantt.templates.rightside_text = function(start, end, task){
      return $this.utilsService.dateFormatFr(end.getTime());
    };
    gantt.attachEvent('onDataRender', function(){
      $this.completTableGantt();
    });
    gantt.templates.scale_cell_class = function(scale){
      return 'style_scale';
    }
    gantt.templates.grid_header_class = function(columnName, column){
      return 'style_scale';
    };

    gantt.attachEvent('onAfterTaskAdd', (id, item) => {
      console.log('attach', item);
      gantt.message('Activité enregistrée');
    });
    gantt.attachEvent('onAfterTaskUpdate', (id, item) => {
      gantt.message('Activité modifiée');
    });
    gantt.attachEvent('onBeforeTaskDelete', function(id, item){
      return true;
    });
    gantt.attachEvent('onAfterTaskDelete', (id) => {
      $this.completTableGantt();
    });

    gantt.attachEvent('onBeforeLinkAdd', function(id, link){
        //gantt.message({type: 'error', text: 'Lien invalide'});
        if (link.type.toString() !== '0'){
          return false;
        }
        else {
          $this.activiteService.postLien($this.projet.id.toString(), link.source.toString(), link.target.toString(), link.type.toString())
            .subscribe(data => {
                $this.updateSuccesseur(link.target.toString());
                const task = gantt.getTask(link.target.toString());
                if (task.$source.length > 0){
                  const $source = task.$source;
                  for (let i = 0; i < $source.length; i++) {
                    $this.updateSuccesseur(gantt.getLink($source[i].toString()).target.toString());
                  }
                }
              },
              error => {
                return false;
              }
            );
        }
    });
    gantt.attachEvent('onAfterLinkAdd', (id, item) => {
      gantt.message('Lien enregistré');
    });
    gantt.attachEvent('onBeforeLinkDelete', function(id, item){
      $this.activiteService.deleteLien($this.projet.id.toString(), item.source.toString(), item.target.toString())
        .subscribe(data => {
            $this.updateSuccesseur(item.target.toString());
            const task = gantt.getTask(item.target.toString());
            if (task.$source.length > 0){
              const $source = task.$source;
              for (let i = 0; i < $source.length; i++) {
                $this.updateSuccesseur(gantt.getLink($source[i].toString()).target.toString());
              }
            }
          },
          error => {
            console.log('***************erreur***********');
            return false;
          }
        );
    });
    gantt.attachEvent('onAfterLinkDelete', (id) => {
      gantt.message('Lien supprimé');
    });

    gantt.attachEvent('onBeforeLinkUpdate', function(id, new_item){
      return false;
    });
    gantt.attachEvent('onBeforeLightbox', function(id) {
        return false;
    });
    gantt.attachEvent('onTaskClick', function(id, e){
      $this.taskSelectedId = id.toString();
      return false;
    });
    gantt.config.quickinfo_buttons = [];
    gantt.init(this.ganttContainer.nativeElement);

    gantt.attachEvent('onParse', function(){
    });
    gantt.parse(this.tasks);
  }
  ganttChangeScale(value){
    const tab: any[] = [];
    if (value === 'mois'){
      gantt.config.scale_unit = 'month';
      gantt.config.date_scale = '%F';
      tab.push({unit: 'year', step: 1, date: '%Y'});
    }
    if (value === 'semaine'){
      gantt.config.scale_unit = 'week';
      gantt.config.date_scale = '%W ème semaine';
      tab.push({unit: 'month', step: 1, date: '%F'});
      tab.push({unit: 'year', step: 1, date: '%Y'});
    }
    if (value === 'annee'){
      gantt.config.scale_unit = 'year';
      gantt.config.date_scale = '%Y';
    }
    if (value === 'jour'){
      gantt.config.scale_unit = 'day';
      gantt.config.date_scale = '%d %M';
      tab.push({unit: 'week', step: 1, date: '%W ème semaine'});
      tab.push({unit: 'month', step: 1, date: '%F'});
      tab.push({unit: 'year', step: 1, date: '%Y'});
    }
    gantt.config.subscales = tab;
    gantt.render();
  }
  completTableGantt(): void{
    const headerGantt = jQuery('.gantt_grid_scale').height();
    const heightGantt = headerGantt + (this.nombreElement + 5) * 27;
    const height = Math.floor(heightGantt * 100 / 585);

    jQuery('#ganttDiv').css('height', height + '%');
    jQuery('#containerGantt').css('height', (heightGantt + 100).toString());
    jQuery('.ganttTable .gantt_last_cell').each(function (index) {
      let menu = '<div class="menu" id="menuContextuel' + index + '" style="float: left; position: absolute;z-index: 100; display: none">\n' +
        '  <ul style="list-style: none; background: white; border: 1px solid; padding: 10px; text-align: left">\n' +
        '    <li style="cursor: pointer;" class="ressource">Ressources</li>\n' +
        '    <li style="cursor: pointer;" class="organisation">Organisations</li>\n' +
        '    <li style="cursor: pointer;" class="contrainte">Contraintes</li>\n' +
        '  </ul>\n' +
        '</div>';
      menu = menu + '<button style="text-align: right" class="btn-white btn btn-xs">\n' +
        '            <i class="fa fa-plus addTaskGantt" style="size: 15px; color:#1ab394; margin-right: 10px"></i>\n' +
        '            <i class="fa fa-edit editTaskGantt" style="size: 15px; color:#1ab394; margin-right: 10px"></i>\n' +
        '            <i class="fa fa-trash deleteTaskGantt" style="size: 15px; color:#1ab394; margin-right: 10px"></i>\n' +
        '            <i class="fa fa-info-circle menuContextuel" id="' + index + '" style="size: 15px; color:#1ab394; margin-right: 10px"></i>\n' +
        '          </button>';
      menu = '<div class="text-right">' + jQuery(this).html() + ' jours  <span style="margin-left: 30px">' + menu + '</span></div>';
      jQuery(this).html(menu);

      jQuery('.gantt_grid_head_duration').html('<div style="margin-right: 20px"><span style="margin-right: 50px">Durée</span><span style="margin-left: 10px">Actions</span></div>');

    });

  }
  ngOnChanges(){
    this.nombreElement = this.tasks.data.length;
  }
  ngOnDestroy(){
    gantt.clearAll();
  }
  ngAfterViewInit() {
    const $this = this;
    jQuery(document.body).on('click', '.gantt_close', function () {
      gantt.close($this.taskSelectedId);
    });
    jQuery(document.body).on('click', '.gantt_open', function () {
      gantt.open($this.taskSelectedId);
    });

    jQuery('body *').click(function() {
        jQuery('.menu').hide();
    });
    jQuery(document.body).on('click', '.ressource', function () {
      const id = $this.taskSelectedId.split('-');
      $this.taskSelectedId = '';
      if (id[0] === 'projet'){
        return;
      }
      else {
        if (id[0] === 'lot'){
          return;
        }
        else {
          for (let i = 0; i < $this.projet.lots.length; i++){
            for (let k = 0; k < $this.projet.lots[i].activites.length; k++){
              if ($this.projet.lots[i].activites[k].id.toString() === id[0]){
                $this.activiteSelected = $this.projet.lots[i].activites[k];
                $this.ngxSmartModalService.getModal('modalRessource').open();
                return;
              }
            }
          }
        }
      }
    });

    jQuery(document.body).on('click', '.menuContextuel', function (event) {
        jQuery('.menu').hide();
        jQuery(this).parent().parent().find('.menu').show();
    });
    jQuery(document.body).on('click', '.addTaskGantt', function () {
      const id = $this.taskSelectedId.split('-');
      $this.taskSelectedId = '';
      if (id[0] === 'projet'){
        $this.titreModalLot = 'Ajouter un lot';
        $this.ngxSmartModalService.getModal('modalLot').open();
        $this.lot = new Lot();
        $this.lot.projet = $this.projet;
      }
      else {
        if (id[0] === 'lot'){
          for (let i = 0; i < $this.projet.lots.length; i++){
            if ($this.projet.lots[i].id.toString() === id[1]){
              $this.titreModalActivite = 'Ajouter une activite';
              $this.activite = new Activite();
              $this.activite.statutActivite = 'Nouveau';
              $this.activite.lot = $this.projet.lots[i];
              $this.ngxSmartModalService.getModal('modalActivite').open();
              break;
            }
          }
        }
        else {
          for (let i = 0; i < $this.projet.lots.length; i++){
            for (let k = 0; k < $this.projet.lots[i].activites.length; k++){
              if ($this.projet.lots[i].activites[k].id.toString() === id[0]){
                $this.titreModalActivite = 'Ajouter une activite';
                $this.activite = new Activite();
                $this.activite.lot = $this.projet.lots[i];
                $this.activite.statutActivite = 'Nouveau';
                $this.activite.activiteParent = $this.projet.lots[i].activites[k];
                $this.ngxSmartModalService.getModal('modalActivite').open();
                return;
              }
            }
          }
        }
      }
    });
    jQuery(document.body).on('click', '.editTaskGantt', function () {
      const id = $this.taskSelectedId.split('-');
      $this.taskSelectedId = '';
      if (id[0] === 'projet'){
        return;
      }
      else {
        if (id[0] === 'lot'){
          for (let i = 0; i < $this.projet.lots.length; i++){
            if ($this.projet.lots[i].id.toString() === id[1]){
              $this.titreModalLot = 'Modifier un lot';
              $this.ngxSmartModalService.getModal('modalLot').open();
              $this.lot = $this.projet.lots[i];
              break;
            }
          }
        }
        else {
          for (let i = 0; i < $this.projet.lots.length; i++){
            for (let k = 0; k < $this.projet.lots[i].activites.length; k++){
              if ($this.projet.lots[i].activites[k].id.toString() === id[0]){
                $this.titreModalActivite = 'Modifier une activite';
                $this.activite = $this.projet.lots[i].activites[k];
                $this.activite.dateExecutionPrevisionnelle = new Date($this.activite.dateExecutionPrevisionnelle);
                $this.activite.dateExecutionReelle = new Date($this.activite.dateExecutionReelle);
                $this.ngxSmartModalService.getModal('modalActivite').open();
                return;
              }
            }
          }
        }
      }
    });
    jQuery(document.body).on('click', '.deleteTaskGantt', function () {
      const id = $this.taskSelectedId.split('-');
      $this.taskSelectedId = '';
      if (id[0] === 'projet'){
        return;
      }
      else {
        if (id[0] === 'lot'){
          for (let i = 0; i < $this.projet.lots.length; i++){
            if ($this.projet.lots[i].id.toString() === id[1]){
              gantt.confirm({
                text: 'êtes-vous sûr de vouloir supprimer le lot ' + $this.projet.lots[i].libelle,
                ok: 'Ok',
                cancel: 'Annuler',
                callback: function (res) {
                  if (res === true){
                    $this.lotService.deleteLot($this.projet.id.toString(), $this.projet.lots[i].id.toString())
                      .subscribe(data => {
                          gantt.deleteTask('lot-' + $this.projet.lots[i].id.toString());
                          $this.nombreElement = $this.nombreElement - $this.projet.lots[i].activites.length - 1;
                          $this.projet.lots.splice(i, 1);
                          $this.updateProjet($this.projet.id);
                          return;
                        },
                        error => {
                          gantt.message({type: 'error', text: 'Lot non supprimé'});
                        }
                      );
                  }
                  return;
                }
              });
            }
          }
        }
        else {
          for (let i = 0; i < $this.projet.lots.length; i++){
            for (let k = 0; k < $this.projet.lots[i].activites.length; k++){
              if ($this.projet.lots[i].activites[k].id.toString() === id[0]){
                gantt.confirm({
                  text: 'êtes-vous sûr de vouloir supprimer l\'activité ' + $this.projet.lots[i].activites[k].libelle,
                  ok: 'Ok',
                  cancel: 'Annuler',
                  callback: function (res) {
                      if (res === true){
                        $this.activiteService.deleteActivite($this.projet.id.toString(), $this.projet.lots[i].activites[k].id.toString())
                          .subscribe(data => {
                              gantt.deleteTask($this.projet.lots[i].activites[k].id.toString());
                              $this.projet.lots[i].activites.splice(k, 1);
                              $this.updateLot($this.projet.lots[i].id);
                              $this.updateProjet($this.projet.lots[i].projet.id);
                              $this.nombreElement--;
                              return;
                            },
                            error => {
                              gantt.message({type: 'error', text: 'Activite non supprimée'});
                            }
                          );
                      }
                      return;
                  }
                });
              }
            }
          }
        }
      }
    });
  }
}
