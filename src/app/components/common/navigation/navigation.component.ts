import { Component } from '@angular/core';
import {Router} from '@angular/router';
import 'jquery-slimscroll';
import {Utilisateur} from "../../../model/utilisateur";

declare var jQuery:any;

@Component({
  selector: 'navigation',
  templateUrl: 'navigation.template.html'
})

export class NavigationComponent {
  utilisateur: Utilisateur;

  constructor(private router: Router) {
    this.utilisateur = JSON.parse(localStorage.getItem('utilisateur'));
  }
  ngOnInit(){
    if (this.utilisateur === undefined || this.utilisateur === null){
      this.router.navigate(['/login']);
    }
  }
  ngAfterViewInit() {
    jQuery('#side-menu').metisMenu();

    if (jQuery("body").hasClass('fixed-sidebar')) {
      jQuery('.sidebar-collapse').slimscroll({
        height: '100%'
      })
    }
  }

  activeRoute(routename: string): boolean{
    return this.router.url.indexOf(routename) > -1;
  }

}
