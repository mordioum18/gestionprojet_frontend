import { Component } from '@angular/core';
import {Router} from '@angular/router';
import { smoothlyMenu } from '../../../app.helpers';
import {PartageService} from '../../../service/partage.service';
import {UtilisateurService} from '../../../service/utilisateur.service';
declare var jQuery:any;

@Component({
  selector: 'topnavigationnavbar',
  templateUrl: 'topnavigationnavbar.template.html',
  styleUrls: ['./topnavigationnavbar.component.css'],
  providers: [UtilisateurService]
})
export class TopNavigationNavbarComponent {
  constructor(private partageService: PartageService,
              private utilisateurService: UtilisateurService,
              private router: Router) {
  }
  ngOnInit(){
    if (this.partageService.idProjet === null){
      this.router.navigate(['/dashboards/projets']);
    }
  }
  toggleNavigation(): void {
    jQuery("body").toggleClass("mini-navbar");
    smoothlyMenu();
  }
  getNomProjet(){
    return this.partageService.getNomProjet();
  }
  getIdProjet(){
    return this.partageService.getIdProjet();
  }
  show(value: string): void{
    jQuery('html, body').animate({
      scrollTop: jQuery('#' + value).offset().top - 120
    }, 1000);
  }
  goTo(value): void{
    if (value === 'detail'){
      this.router.navigate(['dashboards/projets', this.getIdProjet()]);
      return;
    }
    if (value === 'suivi'){
      this.router.navigate(['dashboards/projet-suivi', this.getIdProjet()]);
      return;
    }
    if (value === 'analyse'){
      this.router.navigate(['dashboards/projet-analyse', this.getIdProjet()]);
      return;
    }
  }
  getHeader(){
    return this.partageService.getHeader();
  }
  logout(){
    this.utilisateurService.logout().subscribe(
      data =>{
        this.router.navigate(['/login']);
      }
    );
  }

}
