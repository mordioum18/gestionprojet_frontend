import { Component } from '@angular/core';
import {Router} from '@angular/router';
import { smoothlyMenu } from '../../../app.helpers';
import {UtilisateurService} from '../../../service/utilisateur.service';
declare var jQuery:any;

@Component({
  selector: 'topnavbar',
  templateUrl: 'topnavbar.template.html',
  styleUrls: ['./topnavbar.component.css'],
  providers: [UtilisateurService]
})
export class TopNavbarComponent {
  constructor(private utilisateurService: UtilisateurService,
              private router: Router){
  }
  toggleNavigation(): void {
    jQuery("body").toggleClass("mini-navbar");
    smoothlyMenu();
  }
  logout(){
    this.utilisateurService.logout().subscribe(
      data =>{
        this.router.navigate(['/login']);
      }
    );
  }
}
