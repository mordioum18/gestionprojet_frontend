import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LotService} from '../../../../service/lot.service';
import {ProjetService} from '../../../../service/projet.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Projet} from '../../../../model/projet';
import {Lot} from '../../../../model/lot';
import {LotOrganisation} from '../../../../model/lot-organisation';
import {OrganisationService} from '../../../../service/organisation.service';
import {Organisation} from '../../../../model/organisation';
import {LotOrganisationPk} from '../../../../model/lot-organisation-pk';

@Component({
  selector: 'app-lot-add-organisation',
  templateUrl: './lot-add-organisation.component.html',
  styleUrls: ['./lot-add-organisation.component.css'],
  providers: [ProjetService, LotService, OrganisationService],
})
export class LotAddOrganisationComponent implements OnInit {
  errorMessage: any;
  @Input() lotOrganisation: LotOrganisation = new LotOrganisation();
  @Input() lot: Lot;
  @Input() assignedOrganisations: Organisation[];
  @Input() organisations: Organisation[];
  @Output() retourLotOrganisation: EventEmitter<LotOrganisation> = new EventEmitter();
  organisation: Organisation;
  formLotOrganisation: FormGroup;
  en: any;
  private error: string = null;

  constructor(private projetService: ProjetService,
              private lotService: LotService,
              private organisationService: OrganisationService,
              public ngxSmartModalService: NgxSmartModalService,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.formLotOrganisation = this.fb.group({
      'organisation': [null, Validators.required],
      'dateDebutAttribution': ['', Validators.required],
      'dateFinAttribution': ['', Validators.required]
    });
    this.en = {
      firstDayOfWeek: 0,
      dayNames: ["Sunday", "Monday", "Tuesday","Wednesday", "Thursday", "Friday", "Saturday"],
      dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      monthNames: [ "January","February","March","April","May","June","July","August","September","October","November","December" ],
      monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ]
    };
    this.lotOrganisation.pk = new LotOrganisationPk();
    this.organisation = new Organisation();
  }

  private onSubmit(form) {
    console.log('======== organisation============', this.organisation);
    this.lotOrganisation.pk.lot = this.lot;
    this.lotOrganisation.pk.organisation = this.organisation;
      this.lotService.assignOrganisationToLot(this.lot.id.toString(),
                                              this.lotOrganisation,
                                              this.lot.projet.id.toString())
        .subscribe(
          data => {
            this.lot.lotOrganisations.push(data);
            this.retourLotOrganisation.emit(data);
            this.lotOrganisation.pk = new LotOrganisationPk();
            this.lotOrganisation = new LotOrganisation();
            this.organisation = new Organisation();
            this.ngxSmartModalService.getModal('modalLotOrganisation').close();
          },
          (error) => {
            console.log('Erreur submit lotOrganisation');
            this.errorMessage = error;
            this.lotOrganisation = new LotOrganisation();
            this.lotOrganisation.pk = new LotOrganisationPk();
            this.organisation = new Organisation();
          }
        );
  }
}
