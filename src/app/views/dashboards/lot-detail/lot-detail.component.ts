import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilsService} from '../../../service/utils.service';
import {ActiviteService} from '../../../service/activite.service';
import {ProjetService} from '../../../service/projet.service';
import {LotService} from '../../../service/lot.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Lot} from '../../../model/lot';
import {OrganisationService} from '../../../service/organisation.service';
import {Organisation} from '../../../model/organisation';
import {LotOrganisation} from '../../../model/lot-organisation';
import {LotOrganisationPk} from '../../../model/lot-organisation-pk';

@Component({
  selector: 'app-lot-detail',
  templateUrl: './lot-detail.component.html',
  styleUrls: ['./lot-detail.component.css'],
  providers: [ProjetService, LotService, ActiviteService, OrganisationService]
})
export class LotDetailComponent implements OnInit {
  public nav: any;
  public body: any;
  lot: Lot;
  titreModalLotOrganisation: string;
  modal = false;
  errorMessage: any;
  lotOrganisation: LotOrganisation;
  assignedOrganisations: Organisation[];
  organisations: Organisation[];

  constructor(private projetService: ProjetService,
              private lotService: LotService,
              private activiteService: ActiviteService,
              private organisationService: OrganisationService,
              private utilsService: UtilsService,
              public ngxSmartModalService: NgxSmartModalService,
              private router: Router,
              private route: ActivatedRoute) {
    this.nav = document.querySelector('nav.navbar');
    this.body = document.querySelector('body');
  }

  ngOnInit(): void {
    this.nav.className += ' white-bg';
    this.body.className += ' top-navigation';
    const idProjet = this.route.snapshot.paramMap.get('projetId');
    const idLot = this.route.snapshot.paramMap.get('lotId');
    this.findLot(idProjet, idLot);
    this.lotOrganisation = new LotOrganisation();
    this.lotOrganisation.pk = new LotOrganisationPk();
  }

  findLot(projetId: string, lotId: string) {
    this.lotService.getLot(projetId, lotId)
      .subscribe( lot => {
        this.lot = lot;
          this.lotService.getActivites(projetId, lotId)
            .subscribe(activites => {
                this.lot.activites = activites;
              },
              error => {
                this.errorMessage = <any> error;
              }
            );
          this.lotService.getContraintes(projetId, lotId)
            .subscribe(contraintes => {
                this.lot.contraintes = contraintes;
              },
              error => {
                this.errorMessage = <any> error;
              }
            );
          this.lotService.getLotOrganisations((this.lot.id).toString(), this.lot.projet.id.toString())
            .subscribe( lotOrganisations => {
                this.lot.lotOrganisations = lotOrganisations;
              },
              error => {
                console.log('************* error to get lotOrganisations of lot *******');
                this.errorMessage = <any> error;
              }
            );
        },
        error => {
          this.errorMessage = <any> error;
        }
      );

  }

  modalOpen() {
    this.modal = true;
  }

  assignLotToOrganisation() {
    this.organisationService.getOrganisations()
      .subscribe(organisations => {
          this.organisations = organisations;
          this.lotService.getOrganisations(this.lot.id.toString(), this.lot.projet.id.toString())
            .subscribe(assignedOrganisations => {
                this.assignedOrganisations = assignedOrganisations;
                console.log('success get assigned Organisations');
                for (let k = 0; k < this.assignedOrganisations.length; k++) {
                  let assignOrg = this.assignedOrganisations[k];
                  for (let i = 0; i < this.organisations.length; i++) {
                    if ( this.organisations[i].id === assignOrg.id) {
                      console.log('$$$$$$$$$$$$$$$ Organisation  ' + this.organisations[i].id +
                        '$$$$$$$$$$$$$$$ ASSIGNED£££££££    ' + assignOrg.id);
                      this.organisations.splice(i, 1);
                    }
                  }
                }
              },
              error => {
                console.log('Erreur get assigned Organisations');
                this.errorMessage = <any> error;
              }
            );
        },
        error => {
          console.log('success get all organisations');
          this.errorMessage = <any> error;
        }
      );
    this.titreModalLotOrganisation = 'Affecter ce lot à une organisation';
    this.ngxSmartModalService.getModal('modalLotOrganisation').open();
  }

  addNewLotOrganisation() {
    this.ngxSmartModalService.getModal('modalLotOrganisation').close();
    this.lotOrganisation = new LotOrganisation();
    this.lotOrganisation.pk = new LotOrganisationPk();
  }


}
