import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DatePipe} from '@angular/common';

declare var jQuery: any;
@Component({
  selector: 'app-my-date-picker',
  templateUrl: './my-date-picker.component.html',
  styleUrls: ['./my-date-picker.component.css'],
  providers: []
})
export class MyDatePickerComponent implements OnInit {
  public dt: Date = new Date();
  private start_dt: Date = new Date();
  private end_dt: Date = new Date();
  private first: Date;
  private last: Date;

  public formats: string[] = ['DD-MM-YYYY', 'dd/MM/yyyy', 'YYYY/MM/DD', 'DD.MM.YYYY',
    'shortDate'];
  public format: string;
  public dateOptions: any = {
    formatYear: 'YY',
    startingDay: 1
  };

  @Output() start_dt_selectedChange: EventEmitter<any> = new EventEmitter();
  @Output() end_dt_selectedChange: EventEmitter<any> = new EventEmitter();
  private datePipe: DatePipe;

  public constructor() {
    this.datePipe = new DatePipe('fr');
    this.format = this.formats[1];
  }

  ngOnInit(): void {
    this.end_dt = new Date();
    this.start_dt = new Date();
    this.first = new Date(this.start_dt);
    this.last = new Date(this.end_dt);
    this.start_dt.setDate(this.start_dt.getDate() - 7);
  }

  public getStartDate(event: any) {
    // this.start_dt = event;
    if (event !== null) {
      if (this.last < event) {
        this.start_dt = new Date(this.last);
      } else {
        this.first = new Date(event);
        this.start_dt_selectedChange.emit(event.value);
      }
      // console.log('============== start date selected ++++++++++++++++++++', this.start_dt);
    }
  }

  public getEndDate(event: any) {
    // this.end_dt = event;
    if (event !== null) {
      if (event < this.first) {
        this.end_dt = new Date(this.first);
      } else {
        this.last = new Date(event);
        this.end_dt_selectedChange.emit(event.value);
      }
      // console.log('============== end date selected ++++++++++++++++++++', this.end_dt);
    }
  }

}
