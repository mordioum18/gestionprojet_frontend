import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Projet} from '../../../../model/projet';
import {Router} from '@angular/router';
import {ProjetService} from '../../../../service/projet.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {RessourceService} from '../../../../service/ressource.service';
import {Ressource} from '../../../../model/ressource';
import {RessourceGroupe} from '../../../../model/ressource-groupe';
import {Decaissement} from '../../../../model/decaissement';
import {ActiviteRessource} from '../../../../model/activite-ressource';
import {Activite} from '../../../../model/activite';

@Component({
  selector: 'app-table-ressources',
  templateUrl: './table-ressources.component.html',
  styleUrls: ['./table-ressources.component.css'],
  providers: [ProjetService, RessourceService]
})
export class TableRessourcesComponent implements OnInit, OnChanges {
  @Input() projet: Projet;
  ressource: Ressource;
  ressourceGroupe: RessourceGroupe;
  titreModalRessource: string;
  modal = false;
  private errorMessage: string;
  /*chargementFini = false;
  FTA: Date;
  activites: Activite[];
*/
  constructor(private projetService: ProjetService,
              private ressourceService: RessourceService,
              public ngxSmartModalService: NgxSmartModalService,
              private router: Router) {
  }
  ngOnInit(): void {
    this.ressource = new Ressource();
    this.ressourceGroupe = new RessourceGroupe();
  }

  ngOnChanges() {
    if (this.projet !== undefined) {
      this.getRessources(this.projet.id);
    }
  }

  modalOpen() {
    this.modal = true;
  }

  ajouterRessource() {
    this.titreModalRessource = 'Ajouter une nouvelle ressource au projet';
    this.ngxSmartModalService.getModal('modalRessource').open();
  }

  addNewRessource(ressource) {
    this.ngxSmartModalService.getModal('modalRessource').close();
    this.ressource = new Ressource();
  }

  getRessources(projet: number) {
    this.ressourceService.getRessources(projet)
      .subscribe(
        ressources => {
          this.projet.ressources = ressources;
/*          for (let i = 0; i < this.projet.ressources.length; i++) {
            this.ressourceService.getActiviteRessources(this.projet.ressources[i].id.toString(), this.projet.id.toString())
              .subscribe( activiteRessources => {
                  this.projet.ressources[i].activiteRessources = activiteRessources;
                },
                error => {
                  console.log('************* error to get activiteRessources of ressource *******');
                  this.errorMessage = <any> error;
                }
              );
            this.ressourceService.getActivites(this.projet.ressources[i].id.toString(), this.projet.id.toString())
              .subscribe( activites => {
                  this.activites = activites;
                  console.log('success get assigned activities');
                    let dates: Date[];
                    for (let k = 0; k < this.activites.length; k++) {
                        this.projet.ressources[i].disponibiliteAuPlusTot = this.activites[k].dateExecutionPrevisionnelle;
                  console.log('=======================',this.projet.ressources[i].disponibiliteAuPlusTot);

                    }


                },
                (error) => {
                  console.log('Erreur get assigned activites');
                  this.errorMessage = error;
                }
              );
          }*/
        },
        error => {
          console.log('error get all ressources of project');
          this.errorMessage = <any> error;
        }
      );
  }

 /* quantiteTotale(activiteRessources: ActiviteRessource[]): number {
    let somme = 0;
    if (activiteRessources !== undefined && activiteRessources !== null) {
      for (let i = 0; i < activiteRessources.length; i++) {
        somme = Number.parseFloat(somme.toString()) + Number.parseFloat(activiteRessources[i].quantite.toString());
      }
    }
    return somme;
  }*/

  min_date(all_dates: Date[]) {
   /* let min_dt: Date = all_dates[0];
    let min_dtObj = new Date(all_dates[0]);
    all_dates.forEach(function(dt, index)
    {
      if ( new Date( dt ).getTime() < min_dtObj.getTime()) {
        min_dt = dt;
        min_dtObj = new Date(dt);
      }
    });
    return min_dt;*/
  }

}
