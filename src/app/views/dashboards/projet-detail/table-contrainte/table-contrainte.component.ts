import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Projet} from '../../../../model/projet';
import {Contrainte} from '../../../../model/contrainte';
import {ContrainteService} from '../../../../service/contrainte.service';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Component({
  selector: 'app-table-contraintes',
  templateUrl: './table-contrainte.component.html',
  styleUrls: ['./table-contrainte.component.css'],
  providers: [ContrainteService]
})

export class TableContrainteComponent implements OnInit, OnChanges {
 @Input() projet: Projet;
 contrainte: Contrainte;
 titreModalContrainte: string;
 modal = false;
 contraintes: Contrainte[];

 constructor(private contrainteService: ContrainteService, private ngxSmartModalService: NgxSmartModalService) {}

 ngOnInit(): void {
   this.contrainte = new Contrainte();
 }

 modalOpen() {
   this.modal = true;
 }

 addConstraints() {
   this.contrainte = new Contrainte();
   this.contrainte.lot = null;
   this.titreModalContrainte = 'Ajouter une contrainte';
   this.ngxSmartModalService.getModal('modalContrainte').open();
 }

 addNew(contrainte) {
   this.ngxSmartModalService.getModal('modalContrainte').close();
   this.contrainte = new Contrainte();
 }

 onEdit(contrainte: Contrainte) {
   this.ngxSmartModalService.getModal('modalContrainte').open();
   this.titreModalContrainte = 'Modifier la contrainte';
   this.contrainte = contrainte;
 }

 onDelete(contrainte) {
      const $this = this;
      gantt.confirm({
        'text': 'Voulez vous supprimer la contrainte ' + contrainte.libelle + '?',
        'ok': 'Oui',
        'cancel': 'Non',
        callback: function (result) {
          if (result === true) {
            $this.contrainteService.deleteContrainte($this.projet.id, contrainte.id).subscribe(
              data => {
                for (let i = 0; i < $this.projet.lots.length; i++) {
                  for (let j = 0; j < $this.projet.lots[i].contraintes.length; j++) {
                    if (contrainte.id === $this.projet.lots[i].contraintes[j].id) {
                      $this.projet.lots[i].contraintes.splice(j, 1);
                    }
                  }
                }
              }
            );
          }
        }
      });
 }

  ngOnChanges(changes: SimpleChanges): void {
  }

}
