import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Projet} from '../../../../model/projet';
import {Contrainte} from '../../../../model/contrainte';
import {ContrainteService} from '../../../../service/contrainte.service';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Component({
  selector: 'app-activite-table-contraintes',
  templateUrl: './table-activite-contrainte.component.html',
  styleUrls: ['./table-contrainte.component.css'],
  providers: [ContrainteService]
})

export class TableActiviteContrainteComponent implements OnInit, OnChanges {
 @Input() projet: Projet;
 contrainte: Contrainte;
 titreModalContrainte: string;
 modal = false;
 contraintes: Contrainte[];

 constructor(private contrainteService: ContrainteService, private ngxSmartModalService: NgxSmartModalService) {}

 ngOnInit(): void {
   this.contrainte = new Contrainte();
 }

 modalOpen() {
   this.modal = true;
 }

 addConstraints() {
   this.contrainte = new Contrainte();
   this.contrainte.activite = null;
   this.titreModalContrainte = 'Ajouter une contrainte';
   this.ngxSmartModalService.getModal('modalContrainte2').open();
 }

 addNew(contrainte) {
   this.ngxSmartModalService.getModal('modalContrainte2').close();
   this.contrainte = new Contrainte();
 }

 onEdit(contrainte: Contrainte) {
   this.ngxSmartModalService.getModal('modalContrainte2').open();
   this.titreModalContrainte = 'Modifier la contrainte';
   this.contrainte = contrainte;
 }

 onDelete(contrainte) {
      const $this = this;
      gantt.confirm({
        'text': 'Voulez vous supprimer la contrainte ' + contrainte.libelle + '?',
        'ok': 'Oui',
        'cancel': 'Non',
        callback: function (result) {
          if (result === true) {
            $this.contrainteService.deleteContrainte($this.projet.id, contrainte.id).subscribe(
              data => {
                for (let i = 0; i < $this.projet.lots.length; i++) {
                  for (let j = 0; j < $this.projet.lots[i].activites.length; j++) {
                    for (let k = 0; k < $this.projet.lots[i].activites[j].contraintes.length; k++) {
                      if (contrainte.id === $this.projet.lots[i].activites[j].contraintes[k].id) {
                        $this.projet.lots[i].activites[j].contraintes.splice(k, 1);
                      }
                    }
                  }
                }
              }
            );
          }
        }
      });
 }

  ngOnChanges(changes: SimpleChanges): void {
  }

}
