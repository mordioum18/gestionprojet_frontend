import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {FinancementService} from '../../../../service/financement.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Financement} from '../../../../model/financement';
import {Projet} from '../../../../model/projet';

@Component({
  selector: 'app-form-financement',
  templateUrl: './form.financement.component.html',
  styleUrls: ['./form.financement.component.css'],
  providers: [FinancementService],
})

export class FormFinancementComponent implements OnInit {
  errorMessage: any;
  @Input() projet: Projet;
  @Input() financement: Financement;
  @Output() retourFinancement: EventEmitter<Financement> = new EventEmitter();
  formFinancement: FormGroup;
  constructor(private financementService: FinancementService,
              public ngxSmartModalService: NgxSmartModalService,
              private fb: FormBuilder) {
  }
  ngOnInit(): void {
    this.formFinancement = this.fb.group({
      'libelle': ['', Validators.required],
      'description': [''],
      'montant': ['', Validators.pattern('((^[1-9][0-9]*$)|(^(0|([1-9][0-9]*))[.][0-9]{1,}$))')],
      'dateFinancement': ['', Validators.required],
    });
  }
  private onSubmit(form){
    if (this.financement.id){
      this.financementService.updateFinancement(this.financement).subscribe(
        data => {
          this.retourFinancement.emit(data);
          this.financement = new Financement();
        },
        (error) => {
          console.log('Erreur submit');
          this.errorMessage = error;
        }
      );
    }
    else {
      this.financementService.postFinancement(this.projet.id, this.financement).subscribe(
        data => {
          this.projet.financements.push(data);
          this.retourFinancement.emit(data);
          this.financement = new Financement();
        },
        (error) => {
          console.log('Erreur submit');
          this.errorMessage = error;
        }
      );
    }
  }
}
