import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import {Projet} from '../../../model/projet';
import {ProjetService} from '../../../service/projet.service';
import {LotService} from '../../../service/lot.service';
import {UtilsService} from '../../../service/utils.service';
import {PartageService} from '../../../service/partage.service';
import {ActiviteService} from '../../../service/activite.service';
import {Router, ActivatedRoute} from '@angular/router';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Lot} from '../../../model/lot';
import {Contrainte} from '../../../model/contrainte';
import {ContrainteService} from '../../../service/contrainte.service';

declare var jQuery: any;

@Component({
  selector: 'app-projet-detail',
  templateUrl: './projet.detail.component.html',
  styleUrls: ['./projet.detail.component.css'],
  providers: [ProjetService, LotService, ActiviteService, ContrainteService],
})

export class ProjetDetailComponent implements OnInit, OnDestroy {
  public nav: any;
  public body: any;
  projet: Projet;
  lot: Lot;
  lots: Lot[];
  lotsCharge = false;
  activitesCharge = false;
  errorMessage: any;
  barChartLabels: string[];
  barChartData: any[];
  tasks: {data: any[], links: any[]};
  lat = 14.754515;
  lng = -17.190949;
  private details: Array<any>;
  @Input() startDateSelected: Date;
  @Input() endDateSelected: Date;

  constructor(private projetService: ProjetService,
              private lotService: LotService,
              private activiteService: ActiviteService,
              private utilsService: UtilsService,
              private partageService: PartageService,
              public ngxSmartModalService: NgxSmartModalService,
              private router: Router,
              private route: ActivatedRoute,
              private contrainteService: ContrainteService) {
    this.nav = document.querySelector('nav.navbar');
    this.body = document.querySelector('body');
  }
  ngOnInit(): void {
    this.nav.className += ' white-bg';
    this.body.className += ' top-navigation';
    this.lot = new Lot ();
    const id = this.route.snapshot.paramMap.get('id');
    this.getProjetsById(id);
  }
  ngOnDestroy(){
    this.body.classList.remove('top-navigation');
    this.activitesCharge = false;
  }
  updatePartage(projet: Projet){
    this.partageService.setHeader('Gestion et Suivi-Evaluation de Projets');
    this.partageService.setNomProjet(projet.nom);
    this.partageService.setIdProjet(projet.id);
  }
  getProjetsById(projetId: string) {
    this.projetService.getProjetsById(projetId)
      .subscribe(data => {
          this.projet = data;
          this.updatePartage(this.projet);
          this.getActeurs(this.projet.id);
          this.getLots(this.projet.id);
          this.chargerTache(this.projet.id);
          localStorage.setItem('projet', JSON.stringify(this.projet));
        },
        error => {
          this.errorMessage = <any> error;
        }
      );
  }
  startDateSelectedEvent(eventStartDate: Date) {
    this.startDateSelected = eventStartDate;
    console.log('============== projet detail start date selected ++++++++++++++++++++', this.startDateSelected);
  }

  endDateSelectedEvent(eventEndDate: Date) {
    this.endDateSelected = eventEndDate;
    console.log('============== projet detail end date selected ++++++++++++++++++++', this.endDateSelected);
  }

  getLots(projet: number){
    this.lotService.getLots(projet)
      .subscribe(lots => {
          this.projet.lots = lots;
          this.lots = lots;
          for (let i = 0; i < this.projet.lots.length; i++){
            this.details = [];
            /*console.log('**************** element du tab projet de lots **********' + this.projet.lots[i].libelle);
            console.log('$$$$$$$$$$$$$$ element du tab lots $$$$$$$$$$$$$' + this.lots[i].libelle);*/
            this.contrainteService.contraintesLot(projet.toString(), this.projet.lots[i].id.toString())
              .subscribe(contraintes => {
                  this.projet.lots[i].contraintes = contraintes;
                },
                error2 => {
                  this.errorMessage = <any> error2;
                }
              );
            this.lotService.getActivites(projet.toString(), (this.projet.lots[i].id).toString())
              .subscribe(activites => {
                  this.projet.lots[i].activites = activites;

                  for (let j = 0; j < this.projet.lots[i].activites.length; j++) {
                    this.contrainteService.contraintesActivite(projet.toString(), this.projet.lots[i].activites[j].id.toString())
                      .subscribe(contraintes => {
                          this.projet.lots[i].activites[j].contraintes = contraintes;
                        },
                        error2 => {
                          this.errorMessage = <any> error2;
                        }
                      );
                  }

                  this.details.push({
                      lot: this.projet.lots[i],
                      activites: this.projet.lots[i].activites,
                      longueur: this.projet.lots[i].activites.length
                    }
                  );
                  localStorage.setItem('details', JSON.stringify(this.details));
                },
                error => {
                  this.errorMessage = <any> error;
                }
              );
            this.lotService.getLotOrganisations((this.projet.lots[i].id).toString(), projet.toString())
              .subscribe( lotOrganisations => {
                  this.projet.lots[i].lotOrganisations = lotOrganisations;
              },
                error => {
                  console.log('************* error to get lotOrganisations of lot *******');
                  this.errorMessage = <any> error;
                }
              );
          }
          localStorage.setItem('lots', JSON.stringify(this.projet.lots));
          this.lotsCharge = true;
        },
        error => {
          this.errorMessage = <any> error;
        }
      );
  }
  getActeurs(projet: number){
    this.projetService.getActeurs(projet)
      .subscribe(utilisateurProjets => {
          this.projet.utilisateurProjets = utilisateurProjets;
        },
        error => {
          this.errorMessage = <any> error;
        }
      );
  }
  chargerTache(projet: number): any{
    this.activiteService.getTasks(projet).subscribe(
      tasks => {
        this.tasks = tasks;
        const debutProjet = this.utilsService.dateFormatTache(this.projet.dateDemarragePrevisionnelle);
        const finProjet = this.utilsService.dateFormatTache(this.projet.dateFinPrevisionnelle);
        this.tasks.data.push({id: 'projet', text: this.projet.nom, type: 'projet', start_date: debutProjet, end_date: finProjet});
        this.activitesCharge = true;
      }
    );
  }
  barChart(): void{
    const data = [];
    const colors = [];
    const labels = [];
    let  i = 90;
    for (const lot of this.projet.lots) {
      data.push(i);
      colors.push('#7681E1');
      labels.push(lot.libelle);
      i = i - 3;
    }
    this.barChartData = [
      {data: data, label: 'Evolution des lots', backgroundColor: colors},
    ];
    this.barChartLabels = labels;
  }

  printProjet(id) {
    let tab = window.open('/projet-print/' + id);
    /*this.router.navigate(['/projet-print', id]);*/
  }
}
