import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Projet} from '../../../../model/projet';
import {Contrainte} from '../../../../model/contrainte';
import {ContrainteService} from '../../../../service/contrainte.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Lot} from '../../../../model/lot';
import {Activite} from '../../../../model/activite';

@Component({
  selector: 'app-activite-form-contrainte',
  templateUrl: './form-activite-contrainte.component.html',
  styleUrls: ['./form-contrainte.component.css'],
  providers: [ContrainteService]
})

export class FormActiviteContrainteComponent implements OnInit, OnChanges {
  @Input()projet: Projet;
  @Input()contrainte: Contrainte;
  @Output()retourContrainte: EventEmitter<Contrainte> = new EventEmitter();
  private errorMessage: string;
  formContrainte1: FormGroup;

  constructor(private contrainteService: ContrainteService, private ngxSmartModalService: NgxSmartModalService, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.formContrainte1 = this.fb.group({
      'libelle': ['', Validators.required],
      'description': [''],
      'activite': [null, Validators.required]
    });
  }

  private onSubmit(value) {
    console.log(this.contrainte);
    if (!this.contrainte.id) {
      this.contrainteService.postContrainte(this.projet.id, this.contrainte).subscribe(
        data => {
          for (let i = 0; i < this.projet.lots.length; i++) {
            for (let j = 0; j < this.projet.lots[i].activites.length; j++) {
              if (this.contrainte.activite === this.projet.lots[i].activites[j]) {
                this.projet.lots[i].activites[j].contraintes.push(data);
                break;
              }
            }
          }
          this.retourContrainte.emit(data);
          this.contrainte = new Contrainte();
        },
        error2 => {
          this.errorMessage = error2;
          console.log(this.errorMessage);
        }
      );
    } else {
      this.contrainteService.updateContrainteWithConstraint(this.contrainte).subscribe(
        data => {
          this.retourContrainte.emit(data);
          this.contrainte = new Contrainte();
        },
        error2 => {
          this.errorMessage = error2;
        }
      );
    }
  }

  compareActivite(c1: Activite, c2: Activite): boolean {
    if (c1 && c2 && (c1.id === c2.id)) {
      return true;
    } else {
      return false;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
  }
}
