import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Projet} from '../../../../model/projet';
import {Ressource} from '../../../../model/ressource';
import {UtilsService} from '../../../../service/utils.service';
import {RessourceService} from '../../../../service/ressource.service';
import {ProjetService} from '../../../../service/projet.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {EnumService} from '../../../../service/enum.service';
import {EnumObject} from '../../../../model/enum-object';
import {RessourceGroupeService} from '../../../../service/ressource-groupe.service';
import {RessourceGroupe} from '../../../../model/ressource-groupe';
import {document} from 'ngx-bootstrap/utils/facade/browser';

declare var jQuery: any;

@Component({
  selector: 'app-ressource-add',
  templateUrl: './ressource-add.component.html',
  styleUrls: ['./ressource-add.component.css'],
  providers: [ProjetService, RessourceService, RessourceGroupeService]
})

export class RessourceAddComponent implements OnInit {
  errorMessage: any;
  @Input() ressource: Ressource = new Ressource();
  @Input() projet: Projet;
  @Output() retourRessource: EventEmitter<Ressource> = new EventEmitter();
  ressourceTypes: EnumObject[];
  ressourceUnites: EnumObject[];
  ressourceGroupes: RessourceGroupe[];
  ressourceGroupe: RessourceGroupe;
  newRessourceGroupe: RessourceGroupe;
  formRessource: FormGroup;

  constructor(private projetService: ProjetService,
              private ressourceService: RessourceService,
              private ressourceGroupeService: RessourceGroupeService,
              public ngxSmartModalService: NgxSmartModalService,
              private utilsService: UtilsService,
              private enumService: EnumService,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.getRessourceTypes();
    this.getRessourceUnites();
    this.getRessourceGroupes(this.projet.id);
    this.formRessource = this.fb.group({
      'nom': ['', Validators.required],
      'uniteRessource': [null, Validators.required],
      'email': ['', Validators.required],
      'typeRessource': [null, Validators.required],
      'ressourceGroupe': [''],
      'newRessourceGroupe': [''],
      'quantitePrevue': ['', Validators.compose([Validators.required, Validators.pattern('(^[1-9][0-9]*$)')])]
    });
    this.ressourceGroupe = new RessourceGroupe();
  }

  getRessourceUnites() {
    this.enumService.getRessourceUnites()
      .subscribe(
        ressourceUnites => {
          this.ressourceUnites = ressourceUnites;
        },
        (error) => {
          console.log('Erreur get unite of ressources');
          this.errorMessage = error;
        }
      );
  }

  getRessourceTypes() {
    this.enumService.getRessourceTypes()
      .subscribe(
      ressourceTypes => {
        this.ressourceTypes = ressourceTypes;
      },
        (error) => {
          console.log('Erreur get type of ressources');
          this.errorMessage = error;
        }
    );
  }

  getRessourceGroupes(projet: number) {
    this.ressourceGroupeService.getRessourceGroupes(projet)
      .subscribe(
        ressourceGroupes => {
          this.ressourceGroupes = ressourceGroupes;
        },
        (error) => {
          console.log('Erreur get ressource groupes');
          this.errorMessage = error;
        }
      );
  }

  yesnoCheck() {
    // console.log('================== HAAAAAAAAAA');
    if (jQuery('#ressourceGroupe').val() === 'new') {
      jQuery('#newRessourceGroupeInput').show();
      // console.log('======================HOOOOOOOOOOOOOOOOOOOOO');
    } else {
      jQuery('#newRessourceGroupeInput').hide();
      // console.log('===================== UUUUUUUUUUUUUUUUUU');
    }
  }

  private onSubmit(form) {
    let nom: string;
    nom = document.getElementById('newRessourceGroupe').value;
    // console.log('$$$$$$$$$ nom du nouveau groupe de ressources' + nom);
    if (nom !== null && nom !== '') {
      this.ressourceGroupe.nom = nom;
      this.ressourceGroupe.projet = this.projet;
      this.ressourceGroupeService.postRessourceGroupe(this.ressourceGroupe, this.projet.id.toString())
        .subscribe( data => {
            this.newRessourceGroupe = data;
            this.ressource.ressourceGroupe = this.newRessourceGroupe;
            // console.log('$$$$$$$$$ THISSSSSS   nom du nouveau groupe de ressources   ==== ' + this.ressource.ressourceGroupe.nom);
            this.ressourceService.postRessource(this.ressource, this.projet.id.toString())
              .subscribe(
                ressource => {
                  this.projet.ressources.push(ressource);
                  this.retourRessource.emit(ressource);
                  this.ressource = new Ressource();
                  this.ressourceGroupe = new RessourceGroupe();
                  this.ngxSmartModalService.getModal('modalRessource').close();
                },
                (error) => {
                  console.log('Erreur submit ressource');
                  this.errorMessage = error;
                }
              );
          },
          (error) => {
            console.log('Erreur submit nouvelle équipe de ressources');
            this.errorMessage = error;
          }
        );
    } else {
      this.ressourceService.postRessource(this.ressource, this.projet.id.toString())
        .subscribe(
          ressource => {
            this.projet.ressources.push(ressource);
            this.retourRessource.emit(ressource);
            this.ressource = new Ressource();
            this.ressourceGroupe = new RessourceGroupe();
            this.ngxSmartModalService.getModal('modalRessource').close();
          },
          (error) => {
            console.log('Erreur submit ressource');
            this.errorMessage = error;
          }
        );
    }
    jQuery('#newRessourceGroupeInput').hide();
  }

}
