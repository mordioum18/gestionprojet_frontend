import {Component, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import {ProjetService} from '../../../../service/projet.service';
import {LotService} from '../../../../service/lot.service';
import {UtilsService} from '../../../../service/utils.service';
import {Projet} from '../../../../model/projet';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Lot} from '../../../../model/lot';
import {Router} from '@angular/router';

@Component({
  selector: 'app-table-lot',
  templateUrl: './table.lot.component.html',
  styleUrls: ['./table.lot.component.css'],
  providers: [ProjetService, LotService, UtilsService],
})

export class TableLotComponent implements OnInit, OnChanges {
  @Input() projet: Projet;
  @Output() editLot: EventEmitter<Lot> = new EventEmitter();
  lot: Lot;
  titreModalLot: string;
  modal = false;
  private errorMessage: string;
  constructor(private projetService: ProjetService,
              private lotService: LotService,
              public ngxSmartModalService: NgxSmartModalService,
              private utilsService: UtilsService,
              private router: Router) {
  }
  ngOnInit(): void {
    this.lot = new Lot();
  }
  ngOnChanges() {
  }

  modalOpen() {
    this.modal = true;
  }

  onInfoLot(projetId, lotId) {
    this.router.navigate(['dashboards/lots', projetId, lotId]);
    // console.log('==============onInfoLot================');
  }

  ajouterLot() {
    this.titreModalLot = 'Ajouter un lot';
    this.ngxSmartModalService.getModal('modalLot').open();
  }

  addNewLot(lot) {
    this.ngxSmartModalService.getModal('modalLot').close();
    this.lot = new Lot();
  }

  onEdit(lot){
    this.editLot.emit(lot);
  }
  onDelete(lot){
    const $this = this;
    gantt.confirm({
      text: 'Vous  êtes sur le point de supprimer le lot : ' + lot.libelle + '. Cela entrainera la suppression de toutes ses dépendances',
      ok: 'Supprimer',
      cancel: 'Annuler',
      callback: function(result){
        if (result === true){
          for (let i = 0; i < $this.projet.lots.length; i++){
            if ($this.projet.lots[i].id === lot.id){
              if (!$this.projet.lots[i].activites || $this.projet.lots[i].activites.length === 0){
                $this.lotService.deleteLot($this.projet.id.toString(), lot.id.toString())
                  .subscribe(data => {
                      $this.projet.lots.splice(i, 1);
                    },
                    error => {
                      $this.errorMessage = <any> error;
                    }
                  );
              }
              else {
                let nbActivite = $this.projet.lots[i].activites.length;
                for (let k = 0; k < $this.projet.lots[i].activites.length; k++){
                  if ($this.projet.lots[i].activites[k].lot.id === lot.id){
                    gantt.confirm({
                      text: 'Supprimer : ' + $this.projet.lots[i].activites[k].libelle,
                      ok: 'Ok',
                      cancel: 'Annuler',
                      callback: function(res){
                        if (res === true){
                          gantt.deleteTask($this.projet.lots[i].activites[k].id.toString());
                          nbActivite--;
                          if (nbActivite === 0){
                            $this.lotService.deleteLot($this.projet.id.toString(), lot.id.toString())
                              .subscribe(data => {
                                  $this.projet.lots.splice(i, 1);
                                },
                                error => {
                                  $this.errorMessage = <any> error;
                                }
                              );
                          }
                        }
                        else {
                          return;
                        }
                      }
                    });
                  }
                }
              }
            }
          }
        }
      }
    });
  }
  dureeRestante = function(dateLivraison: number): string {
    if (dateLivraison != null && dateLivraison > 0){
      return this.utilsService.date_diff_indays(Date.now(), dateLivraison) + ' jours';
    }
    return 'indéfinie';
  };
  classTable = function(dateLivraison: number): string{
    if (dateLivraison != null && dateLivraison > 0){
      if (this.utilsService.date_diff_indays(Date.now(), dateLivraison) <= 0){
        return 'bg-danger';
      }
    }
    return '';
  };
}
