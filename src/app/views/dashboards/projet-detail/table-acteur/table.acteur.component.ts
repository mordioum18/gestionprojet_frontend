import {Component, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import {ProjetService} from '../../../../service/projet.service';
import {UtilsService} from '../../../../service/utils.service';
import {EnumService} from '../../../../service/enum.service';
import {OrganisationService} from '../../../../service/organisation.service';
import {Projet} from '../../../../model/projet';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {UtilisateurProjet} from '../../../../model/utilisateur-projet';
import {UtilisateurProjetPk} from '../../../../model/utilisateur-projet-pk';
import {Enum} from '../../../../model/Enum';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Utilisateur} from '../../../../model/utilisateur';
import {Organisation} from '../../../../model/organisation';
declare var jQuery:any;

@Component({
  selector: 'app-table-acteur',
  templateUrl: './table.acteur.component.html',
  styleUrls: ['./table.acteur.component.css'],
  providers: [ProjetService, OrganisationService],
})

export class TableActeurComponent implements OnInit, OnChanges {
  @Input() projet: Projet;
  titreModalActeur: string;
  titreModalOrganisation: string;
  utilisateur: Utilisateur;
  roles: Enum[];
  organisations: Organisation[];
  organisation: Organisation;
  formActeur: FormGroup;
  private errorMessage: string;
  constructor(private projetService: ProjetService,
              public ngxSmartModalService: NgxSmartModalService,
              private enumService: EnumService,
              private organisationService: OrganisationService,
              private fb: FormBuilder,
              private utilsService: UtilsService) {
  }
  ngOnInit(): void {
    this.utilisateur = new Utilisateur();
    this.organisation = new Organisation();
    this.getRoles();
    this.getOrganisation();
    this.formActeur = this.fb.group({
      'acteurEmail': ['', Validators.pattern('^[a-z0-9.]{1,}@[a-z]{1,}[.][a-z]{2,5}$')],
      'role': ['', Validators.required],
      'organisation': [null, Validators.required],
    });
  }
  getRoles(){
    this.enumService.getRoles().subscribe(
      roles =>{
        this.roles = roles;
      }
    );
  }
  getOrganisation(){
    this.organisationService.getOrganisations().subscribe(
      organisations =>{
        this.organisations = organisations;
      }
    );
  }
  AjouterActeur(){
    this.titreModalActeur = 'Ajouter un acteur';
    this.ngxSmartModalService.getModal('modalActeur').open();
  }
  ajouterOrganisation(){
    this.titreModalOrganisation = 'Ajouter une organisation';
    this.ngxSmartModalService.getModal('modalOrganisation').open();
  }
  addNewOrganisation(organisation){
    this.organisations.push(organisation);
    this.ngxSmartModalService.getModal('modalOrganisation').close();
    this.utilisateur.organisation = organisation;
    this.organisation = organisation;
    jQuery('#ajoutorganisation').hide();
    jQuery('#supprimeorganisation').show();
  }
  supprimerOrganisation(){
    this.organisationService.deleteOrganisation(this.organisation.id.toString()).subscribe(
      data =>{
        for (let i = 0; i < this.organisations.length; i++){
          if (this.organisations[i].id === this.organisation.id){
            this.organisations.splice(i, 1);
            this.organisation = new Organisation;
            jQuery('#ajoutorganisation').show();
            jQuery('#supprimeorganisation').hide();
            break;
          }
        }
      }
    );
  }

  equals(o1: any, o2: any) {
    if (o1 && o2 && o1.id && o2.id){
      return o1.id === o2.id;
    }
    else {
      return false;
    }
  }
  onDelete(up: UtilisateurProjet){
    const $this = this;
    gantt.confirm({
      text: 'Voulez vous supprimer cet acteur : ' + up.pk.utilisateur.email,
      ok: 'Ok',
      cancel: 'Annuler',
      callback: function(result){
        if (result === true){
          $this.projetService.deleteActeur(up).subscribe(
            data =>{
              for (let i = 0; i < $this.projet.utilisateurProjets.length; i++){
                if ($this.projet.utilisateurProjets[i].pk.utilisateur.id === up.pk.utilisateur.id &&
                  $this.projet.utilisateurProjets[i].pk.projet.id === up.pk.projet.id){
                  $this.projet.utilisateurProjets.splice(i, 1);
                  break;
                }
              }
            },
            erreur =>{
              if(erreur.error && erreur.error.errorMessage){
                $this.errorMessage = erreur.error.errorMessage;
                setTimeout(() =>{
                  $this.errorMessage = '';
                }, 5000);
              }
            }
          );
        }
      }
    });
  }
  onSubmit(value){
    const up = new UtilisateurProjet();
    up.pk = new UtilisateurProjetPk();
    up.pk.utilisateur = this.utilisateur;
    up.pk.projet = this.projet;
    up.role = value.role;
    this.projetService.postActeur(up).subscribe(
      data =>{
        this.projet.utilisateurProjets.push(data);
        this.organisation = new Organisation();
        this.utilisateur = new Utilisateur();
        jQuery('#ajoutorganisation').show();
        jQuery('#supprimeorganisation').hide();
        this.ngxSmartModalService.getModal('modalActeur').close();
      },
      erreur =>{
        if(erreur.error && erreur.error.errorMessage){
          this.errorMessage = erreur.error.errorMessage;
          setTimeout(() =>{
            this.errorMessage = '';
          }, 5000);
        }
      }
    );

  }
  ngOnChanges() {
  }

}
