import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {OrganisationService} from '../../../../service/organisation.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Organisation} from '../../../../model/organisation';

@Component({
  selector: 'app-form-organisation',
  templateUrl: './form.organisation.component.html',
  styleUrls: ['./form.organisation.component.css'],
  providers: [OrganisationService],
})

export class FormOrganisationComponent implements OnInit {
  errorMessage: any;
  @Input() organisation: Organisation = new Organisation();
  @Output() retourOrganisation: EventEmitter<Organisation> = new EventEmitter();
  formOrganisation: FormGroup;
  constructor(private organisationService: OrganisationService,
              public ngxSmartModalService: NgxSmartModalService,
              private fb: FormBuilder) {
  }
  ngOnInit(): void {
    this.formOrganisation = this.fb.group({
      'raisonSociale': ['', Validators.required],
      'adresse': [''],
    });
  }
  private onSubmit(type){
    if (this.organisation.id){
      this.organisationService.updateOrganisation(this.organisation).subscribe(
        data => {
          this.retourOrganisation.emit(data);
        },
        (error) => {
          console.log('Erreur submit');
          this.errorMessage = error;
        }
      );
    }
    else {
      this.organisationService.postOrganisation(this.organisation).subscribe(
        data => {
          this.retourOrganisation.emit(data);
        },
        (error) => {
          console.log('Erreur submit');
          this.errorMessage = error;
        }
      );
    }
  }
}
