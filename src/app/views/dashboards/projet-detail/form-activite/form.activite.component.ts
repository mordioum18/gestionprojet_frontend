import {Component, OnInit, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {ActiviteService} from '../../../../service/activite.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Activite} from '../../../../model/activite';
import {EnumService} from '../../../../service/enum.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Projet} from '../../../../model/projet';
import {Enum} from '../../../../model/Enum';
declare var jQuery:any;

@Component({
  selector: 'app-form-activite',
  templateUrl: './form.activite.component.html',
  styleUrls: ['./form.activite.component.css'],
  providers: [ActiviteService],
})

export class FormActiviteComponent implements OnInit, OnChanges {
  errorMessage: any;
  @Input() activite: Activite;
  @Input() projet: Projet;
  @Output() retourTache: EventEmitter<Activite>= new EventEmitter();
  formActivite: FormGroup;
  statutActivite: Enum[];
  hideDate = {
    prevue: false,
    reelle : false
  }
  constructor(private activiteService: ActiviteService,
              private enumService: EnumService,
              private fb: FormBuilder,
              public ngxSmartModalService: NgxSmartModalService) {
  }
  ngOnInit(): void {
    this.getStatut()
    this.formActivite = this.fb.group({
      'libelle': ['', Validators.required],
      'budget': ['', Validators.required],
      'description': [''],
      'statutActivite': ['', Validators.required],
     'dateExecutionPrevisionnelle': [''],
     'dateExecutionReelle': [''],
      'dureePrevisionnelle': ['', Validators.compose([Validators.required, Validators.pattern('(^[1-9][0-9]*$)')])],
    });
  }
  ngOnChanges(){
    if (this.activite.id !== undefined && this.activite.id !== null){
      const task = gantt.getTask(this.activite.id);
      if(task.$target.length !== 0){
        this.hideDate.prevue = true;
        this.hideDate.reelle = false;
      }
      else {
        this.hideDate.prevue = false;
        this.hideDate.reelle = false;
      }
    }
    else {
      this.hideDate.prevue = false;
      this.hideDate.reelle = true;
    }
  }
  compareStatut(c1: string, c2: string): boolean {
    if (c1 && c2 && (c1 === c2)){
      return true
    }
    else {
      return false;
    }
  }
  private onSubmit(value){
    if (this.activite.id){
      this.activiteService.updateActivite(this.projet.id.toString(), this.activite).subscribe(
        data => {
          this.ngxSmartModalService.getModal('modalActivite').close();
          jQuery('#lot').prop('disabled', false);
          for(let i = 0; i < this.projet.lots.length; i++) {
            if (this.projet.lots[i].id === this.activite.lot.id){
              for(let k = 0; k < this.projet.lots[i].activites.length; k++){
                  if (this.projet.lots[i].activites[k].id === this.activite.id){
                    this.projet.lots[i].activites.splice(k, 1);
                    this.projet.lots[i].activites.splice(k, 0, this.activite);
                    break;
                  }
              }
            }
          }
          this.retourTache.emit(data);
        },
        (error) => {
          console.log('Erreur submit');
          this.errorMessage = error;
        }
      );
    }
    else {
      this.activiteService.postActivite(this.projet.id.toString(), this.activite).subscribe(
        data => {
          this.ngxSmartModalService.getModal('modalActivite').close();
          jQuery('#lot').prop('disabled', false);
          for(let i = 0; i < this.projet.lots.length; i++) {
            if (this.projet.lots[i].id === this.activite.lot.id){
              this.projet.lots[i].activites.push(data);
              break;
            }
          }
          this.retourTache.emit(data);
        },
        (error) => {
          console.log('Erreur submit');
          this.errorMessage = error;
        }
      );
    }
  }
  private getStatut(){
    this.enumService.getStatuts().subscribe(
      data => {
        this.statutActivite = data;
      },
      error => {
        this.errorMessage = error;
      }
    );
  }
}
