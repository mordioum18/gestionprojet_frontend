import {Component, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import {ProjetService} from '../../../../service/projet.service';
import {FinancementService} from '../../../../service/financement.service';
import {Projet} from '../../../../model/projet';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Financement} from '../../../../model/financement';
import {tr} from "ngx-bootstrap";
declare var jQuery:any;

@Component({
  selector: 'app-table-financement',
  templateUrl: './table.financement.component.html',
  styleUrls: ['./table.financement.component.css'],
  providers: [ProjetService, FinancementService],
})

export class TableFinancementComponent implements OnInit, OnChanges {
  @Input() projet: Projet;
  financement: Financement;
  totalFinancement = 0;
  titreModalFinancement: string;
  modal = false;
  private errorMessage: string;
  constructor(private projetService: ProjetService,
              public ngxSmartModalService: NgxSmartModalService,
              private financementService: FinancementService) {
  }
  ngOnInit(): void {
    this.financement = new Financement();
  }
  getFinancements(projet: number){
    this.projetService.getFinancements(projet)
      .subscribe(financements => {
          this.projet.financements = financements;
          this.calculMontantTotal();
        },
        error => {
          this.errorMessage = <any> error;
        }
      );
  }
  calculMontantTotal(){
    this.totalFinancement = 0;
    for (let i = 0; i < this.projet.financements.length; i++){
      if (this.projet.financements[i].montant !== null){
        this.totalFinancement = Number.parseFloat(this.totalFinancement.toString()) + Number.parseFloat(this.projet.financements[i].montant.toString());
      }
    }
  }
  modalOpen(){
    this.modal = true;
  }
  ajouterFinancement(){
    this.titreModalFinancement = 'Ajouter un Financement';
    this.ngxSmartModalService.getModal('modalFinancement').open();
  }
  addNewFinancement(financement){
    this.ngxSmartModalService.getModal('modalFinancement').close();
    this.financement = new Financement();
    this.calculMontantTotal();
  }
  onEdit(financement){
    this.titreModalFinancement = 'Modifier un financement';
    financement.dateFinancement = new Date(financement.dateFinancement);
    this.financement = financement;
    this.ngxSmartModalService.getModal('modalFinancement').open();
  }
  onDelete(financement){
    const $this = this;
    gantt.confirm({
      text: 'Voulez vous supprimer ce financment : ' + financement.libelle,
      ok: 'Ok',
      cancel: 'Annuler',
      callback: function(result){
        if (result === true){
          $this.financementService.deleteFinancement($this.projet.id, financement.id).subscribe(
            data =>{
              for (let i = 0; i < $this.projet.financements.length; i++){
                if ($this.projet.financements[i].id === financement.id){
                  $this.projet.financements.splice(i, 1);
                  $this.calculMontantTotal();
                  break;
                }
              }
            }
          );
        }
      }
    });
  }
  ngOnChanges() {
    if (this.projet !== undefined){
      this.getFinancements(this.projet.id);
    }
  }

}
