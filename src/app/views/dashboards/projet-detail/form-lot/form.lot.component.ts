import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ProjetService} from '../../../../service/projet.service';
import {LotService} from '../../../../service/lot.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Lot} from '../../../../model/lot';
import {Projet} from "../../../../model/projet";
import {Activite} from "../../../../model/activite";

@Component({
  selector: 'app-form-lot',
  templateUrl: './form.lot.component.html',
  styleUrls: ['./form.lot.component.css'],
  providers: [ProjetService, LotService],
})

export class FormLotComponent implements OnInit {
  errorMessage: any;
  @Input() lot: Lot = new Lot();
  @Input() projet: Projet;
  @Output() retourLot: EventEmitter<Lot> = new EventEmitter();
  formLot: FormGroup;
  constructor(private projetService: ProjetService,
              private lotService: LotService,
              public ngxSmartModalService: NgxSmartModalService,
              private fb: FormBuilder) {
  }
  ngOnInit(): void {
    this.formLot = this.fb.group({
      'libelle': ['', Validators.required],
      'description': [''],
    });
  }
  private onSubmit(form){
    if (this.lot.id){
      this.lotService.updateLot(this.projet.id.toString(), this.lot).subscribe(
        lot => {
          for (let i = 0; i < this.projet.lots.length; i++){
            if (this.projet.lots[i].id === lot.id){
              const activites = this.projet.lots[i].activites;
              this.projet.lots.splice(i, 1);
              this.projet.lots.splice(i, 0, lot);
              this.projet.lots[i].activites = activites;
              this.retourLot.emit(lot);
              this.ngxSmartModalService.getModal('modalLot').close();
              break;
            }
          }
        },
        (error) => {
          console.log('Erreur submit');
          this.errorMessage = error;
        }
      );
    }
    else {
      this.lotService.postLot(this.projet.id.toString(), this.lot).subscribe(
        data => {
          let lot = new Lot();
          const activites: Activite[] = [];
          lot = data;
          lot.activites = activites;
          this.projet.lots.push(lot);
          this.retourLot.emit(lot);
          this.ngxSmartModalService.getModal('modalLot').close();
        },
        (error) => {
          console.log('Erreur submit');
          this.errorMessage = error;
        }
      );
    }
  }
}
