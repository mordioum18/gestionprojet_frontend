import {Component, OnInit, OnDestroy} from '@angular/core';
import {Projet} from '../../../model/projet';
import {ProjetService} from '../../../service/projet.service';
import {LotService} from '../../../service/lot.service';
import {UtilsService} from '../../../service/utils.service';
import {PartageService} from '../../../service/partage.service';
import {ActiviteService} from '../../../service/activite.service';
import {Router, ActivatedRoute} from '@angular/router';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Activite} from '../../../model/activite';
import {SuiviActivite} from "../../../model/suivi-activite";
declare var jQuery:any;
@Component({
  selector: 'app--analyse-projet',
  templateUrl: './analyse.projet.component.html',
  styleUrls: ['./analyse.projet.component.css'],
  providers: [ProjetService, LotService, ActiviteService],
})

export class AnalyseProjetComponent implements OnInit, OnDestroy {
  public nav: any;
  public body: any;
  projet: Projet;
  activites: Activite[];
  errorMessage: any;
  barReady = false;
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  barActiviteChartLabels: string[];
  barChartType: string = 'bar';
  barChartLegend: boolean = true;
  public barActiviteChartColors: Array<any> = [
    {
      backgroundColor: '#3498DB',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  pieChartLabels: string[] = ['Nouveaux', 'En cours', 'Terminées', 'Arrêtées'];
  pieChartData: number[];
  pieChartType: string = 'pie';
  pieActiviteReady = false;

  lineRetardChartLabels: Array<any> = [];
  lineRetardChartData: Array<any> = [];
  lineRetardChartType: string = 'line';
  lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors: Array<any> = [];
  lineRetardReady = false;

  public barActiviteChartData: any[] ;
  constructor(private projetService: ProjetService,
              private partageService: PartageService,
              private activiteService: ActiviteService,
              private router: Router,
              private route: ActivatedRoute) {
    this.nav = document.querySelector('nav.navbar');
    this.body = document.querySelector('body');
  }
  ngOnInit(): void {
    this.nav.className += ' white-bg';
    this.body.className += ' top-navigation';
    this.barReady = false;
    this.pieActiviteReady = false;
    this.lineRetardReady = false;
    const id = this.route.snapshot.paramMap.get('id');
    this.getProjetsById(id);
  }
  ngOnDestroy(){
    this.body.classList.remove('top-navigation');
  }
  getProjetsById(projetId: string) {
    this.projetService.getProjetsById(projetId)
      .subscribe(data => {
          this.projet = data;
          this.updatePartage(this.projet);
          this.getAllActivites(this.projet.id);
        },
        error => {
          this.errorMessage = <any> error;
        }
      );
  }
  updatePartage(projet: Projet){
    this.partageService.setHeader('Gestion de projets');
    this.partageService.setNomProjet(projet.nom);
    this.partageService.setIdProjet(projet.id);
  }
  getAllActivites(projet: number){
    this.activiteService.getAllActivites(this.projet.id).subscribe(
      activites =>{
        this.activites = activites;
        this.barActiviteChart();
        this.pieActiviteChart();
        for (let i = 0; i < this.activites.length; i++){
          this.activiteService.getDecaissements(this.projet.id, this.activites[i].id)
            .subscribe(decaissements => {
                this.activites[i].decaissements = decaissements;
              },
              error => {
                this.errorMessage = <any> error;
              }
            );
          this.activiteService.getSuivis(this.projet.id, this.activites[i].id)
            .subscribe(suiviActivites => {
                this.activites[i].suiviActivites = suiviActivites;
                const fin: boolean = (i === (this.activites.length - 1));
                this.lineRetardChart(this.activites[i], suiviActivites, fin);
              },
              error => {
                this.errorMessage = <any> error;
              }
            );
          this.activiteService.getActiviteRessources(this.activites[i].id.toString(), this.projet.id.toString())
            .subscribe( activiteRessources => {
              this.activites[i].activiteRessources = activiteRessources;
            },
              error => {
              console.log('************* error to get activiteRessources of activity *******');
                this.errorMessage = <any> error;
              }
            );
        }
      }
    );
  }
  barActiviteChart(){
    const data: number[] = [];
    this.barActiviteChartLabels = [];
    for (let i = 0; i < this.activites.length; i++){
      this.barActiviteChartLabels.push(this.activites[i].libelle);
      data.push(this.activites[i].tauxExecutionPhysique);
    }
    this.barActiviteChartData = [
      {data: data, label: 'Progression des activités'}
    ];
    this.barReady = true;
  }
  pieActiviteChart(){
    const data: number[] = [];
    this.pieChartData = [];
    let encours = 0;
    let nouveau = 0;
    let termine = 0;
    let arrete = 0;
    for (let i = 0; i < this.activites.length; i++){
      if (this.activites[i].statutActivite === 'Nouveau'){
        nouveau++;
      }
      if (this.activites[i].statutActivite === 'En cours'){
        encours++;
      }
      if (this.activites[i].statutActivite === 'Terminé'){
        termine++;
      }
      if (this.activites[i].statutActivite === 'Arrêté'){
        arrete++;
      }
    }
    this.pieChartData = [nouveau, encours, termine, arrete];
    this.pieActiviteReady = true;
  }
  lineRetardChart(activite: Activite, suiviActivites: SuiviActivite[], finish: boolean){
    const data: number[] = [];
    const r = (Math.floor((Math.random() * 255)) * Date.now() * activite.id) % 255;
    const g = (Math.floor((Math.random() * 255)) * Date.now() + activite.tauxExecutionPhysique) % 255;
    const b = (Math.floor((Math.random() * 255)) * Date.now()) % 255;
    //this.lineRetardChartLabels.push(activite.libelle);
    const color = {
        backgroundColor: 'rgba(' + r + ',' + g + ',' + b + ',0.2)',
        borderColor: 'rgba(' + r + ',' + g + ',' + b + ',1)',
        pointBackgroundColor: 'rgba(' + r + ',' + g + ',' + b + ',1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(' + r + ',' + g + ',' + b + ',0.8)',
      };
    this.lineChartColors.push(color);
    for (let i = 0; i < suiviActivites.length; i++){
      data.push(suiviActivites[i].retard);
    }
    this.lineRetardChartData.push({data: data, label: activite.libelle});
    if (finish){
      this.lineRetardReady = true;
    }
  }
}
