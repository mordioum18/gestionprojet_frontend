import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ProjetService} from '../../../../service/projet.service';
import {Projet} from '../../../../model/projet';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TypeProjetService} from '../../../../service/type-projet.service';
import {EnumService} from '../../../../service/enum.service';
import {TypeProjet} from '../../../../model/type-projet';
import {Enum} from '../../../../model/Enum';

@Component({
  selector: 'app-form-projet',
  templateUrl: './form.projet.component.html',
  styleUrls: ['./form.projet.component.css'],
  providers: [ProjetService, TypeProjetService, EnumService],
})

export class FormProjetComponent implements OnInit {
  errorMessage: any;
  @Input() projet: Projet;
  typeProjet: TypeProjet[];
  statutProjet: Enum[];
  formProjet: FormGroup;
  @Output() addProjet: EventEmitter<Projet> = new EventEmitter();
  en: any;
  constructor(private projetService: ProjetService,
              private enumService: EnumService,
              private fb: FormBuilder,
              private typeProjetService: TypeProjetService) {
    this.projet = new Projet();
  }
  ngOnInit(): void {
    this.getTypeProjet();
    this.getStatutProjet();
    this.formProjet = this.fb.group({
      'nom': ['', Validators.required],
      'description': [''],
      'statutProjet': ['', Validators.required],
      'typeProjet': ['', Validators.required],
      'dateDemarragePrevisionnelle': ['', Validators.required],
      'dateDemarrageReelle': [''],
      'dateReceptionPrevisionnelle': ['', Validators.required],
      'dateReceptionReelle': [''],
      'dateFinReelle': [''],
    });
    this.en = {
      firstDayOfWeek: 0,
      dayNames: ["Sunday", "Monday", "Tuesday","Wednesday", "Thursday", "Friday", "Saturday"],
      dayNamesShort: ["Dim", "Lun", "Tue", "Wed", "Thu", "Fri", "Sat"],
      monthNames: [ "January","February","March","April","May","June","July","August","September","October","November","December" ],
      monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ]
    };
  }
  compareStatut(c1: string, c2: string): boolean {
    if (c1 && c2 && (c1 === c2)){
      return true
    }
    else {
      return false;
    }
  }
  compareType(c1: TypeProjet, c2: TypeProjet): boolean {
    if (c1 && c2 && (c1.id === c2.id)){
      return true
    }
    else {
      return false;
    }
  }
  private onSubmit(type){
    if (this.projet.id){
      this.projetService.updateProjet(this.projet).subscribe(
        data => {
          this.addProjet.emit(data);
        },
        (error) => {
          console.log('Erreur submit');
          this.errorMessage = error;
        }
      );
    }
    else {
      console.log(this.projet)
      this.projetService.postProjet(this.projet).subscribe(
        data => {
          this.addProjet.emit(data);
        },
        (error) => {
          console.log('Erreur submit');
          this.errorMessage = error;
        }
      );
    }
  }
  private getTypeProjet(){
    this.typeProjetService.getTypeProjets().subscribe(
      data => {
        this.typeProjet = data;
      },
      error => {
        this.errorMessage = error;
      }
    );
  }
  private getStatutProjet(){
    this.enumService.getStatuts().subscribe(
      data => {
        this.statutProjet = data;
      },
      error => {
        this.errorMessage = error;
      }
    );
  }
  dateValide(){
    if (this.projet.dateDemarragePrevisionnelle === undefined || this.projet.dateReceptionPrevisionnelle === undefined){
      return false;
    }
    if (this.projet.dateDemarragePrevisionnelle.getTime() >this.projet.dateReceptionPrevisionnelle.getTime()){
      return false;
    }
    if (this.projet.dateDemarrageReelle !== undefined && this.projet.dateDemarrageReelle !== null){
      if (this.projet.dateDemarrageReelle.getTime() < this.projet.dateDemarragePrevisionnelle.getTime()){
        return false;
      }
      if (this.projet.dateReceptionPrevisionnelle.getTime() < this.projet.dateDemarrageReelle.getTime()){
        return false;
      }
      if (this.projet.dateReceptionReelle !== undefined && this.projet.dateReceptionReelle !== null){
        if (this.projet.dateReceptionReelle.getTime() < this.projet.dateDemarragePrevisionnelle.getTime()){
          return false;
        }
      }
    }
    if (this.projet.dateReceptionReelle !== undefined && this.projet.dateReceptionReelle !== null){
      if (this.projet.dateReceptionReelle.getTime() < this.projet.dateReceptionPrevisionnelle.getTime()){
        return false;
      }
      if (this.projet.dateReceptionReelle.getTime() < this.projet.dateDemarragePrevisionnelle.getTime()){
        return false;
      }
    }
    if (this.projet.dateFinRelle !== undefined && this.projet.dateFinRelle !== null){
      if (this.projet.dateFinRelle.getTime() < this.projet.dateDemarragePrevisionnelle.getTime()){
        return false;
      }
      if (this.projet.dateDemarrageReelle !== undefined && this.projet.dateDemarrageReelle !== null){
        if (this.projet.dateFinRelle.getTime() < this.projet.dateDemarrageReelle.getTime()){
          return false;
        }
      }
    }
    return true;
  }
}
