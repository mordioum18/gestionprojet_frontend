import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {Router} from '@angular/router';
import {Projet} from '../../../../model/projet';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {ProjetService} from '../../../../service/projet.service';

@Component({
  selector: 'app-table-projet',
  templateUrl: './table.projet.component.html',
  styleUrls: ['./table.projet.component.css'],
  providers: [ProjetService],
})

export class TableProjetComponent implements OnInit, OnChanges {
  @Input() projets: Projet[];
  projet: Projet;
  titreModalProjet: string;
  modalProjetOpen = false;
  public constructor(public ngxSmartModalService: NgxSmartModalService,
                     private projetService: ProjetService,
                     private router: Router){
  }
  ngOnInit(): void {
    this.projet = new Projet();
  }
  ngOnChanges() {
  }
  ajouter(){
    this.titreModalProjet = 'Ajouter un projet';
    this.projet = new Projet();
    this.projet.statutProjet = 'Nouveau';
    this.ngxSmartModalService.getModal('modalProjet').open();
  }
  onEdit(projet: Projet){
    this.titreModalProjet = 'Modifier un projet';
    this.projet = projet;
    this.projet.dateDemarragePrevisionnelle = new Date(this.projet.dateDemarragePrevisionnelle);
    this.projet.dateReceptionPrevisionnelle = new Date(this.projet.dateReceptionPrevisionnelle);
    if (this.projet.dateDemarrageReelle !== undefined && this.projet.dateDemarrageReelle !== null){
      this.projet.dateDemarrageReelle = new Date(this.projet.dateDemarrageReelle);
    }
    if (this.projet.dateReceptionReelle !== undefined && this.projet.dateReceptionReelle !== null){
      this.projet.dateReceptionReelle = new Date(this.projet.dateReceptionReelle);
    }
    if (this.projet.dateFinRelle !== undefined && this.projet.dateFinRelle !== null){
      this.projet.dateFinRelle = new Date(this.projet.dateFinRelle);
    }
    this.ngxSmartModalService.getModal('modalProjet').open();
  }
  onDelete(projet: Projet){
    const $this = this;
    gantt.confirm({
      text: 'Vous  êtes sur le point de supprimer le projet : ' + projet.nom + '. Cela entrainera la suppression de toutes ses dépendances',
      ok: 'Supprimer',
      cancel: 'Annuler',
      callback: function (result) {
        if (result === true) {
          $this.projetService.deleteProjet(projet.id).subscribe(
            data => {
              for (let i = 0; i < $this.projets.length; i++){
                if ($this.projets[i].id === projet.id){
                  $this.projets.splice(i, 1);
                  return;
                }
              }
            },
            (error) => {
              console.log('Erreur submit');
            }
          );
        }
      }
    });
  }
  modalProjet(){
    this.modalProjetOpen = true;
  }
  addNewProjet(projet){
    this.ngxSmartModalService.getModal('modalProjet').close();
    for (let i = 0; i < this.projets.length; i++){
      if (this.projets[i].id === projet.id){
        this.projets.splice(i, 1);
        this.projets.splice(i, 0, projet);
        return;
      }
    }
    this.projets.push(projet);
  }

  projetDetail(id){
    this.router.navigate(['dashboards/projets', id]);
  }
}
