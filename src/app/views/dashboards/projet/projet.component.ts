import {Component, OnInit, OnDestroy} from '@angular/core';
import {ProjetService} from '../../../service/projet.service';
import {UtilsService} from '../../../service/utils.service';
import {PartageService} from '../../../service/partage.service';
import {Projet} from '../../../model/projet';
import {Router} from '@angular/router';


declare var jQuery:any;

@Component({
  selector: 'app-projet',
  templateUrl: './projet.component.html',
  styleUrls: ['./projet.component.css'],
  providers: [UtilsService, ProjetService],
})

export class ProjetComponent implements OnInit, OnDestroy {
  public nav: any;
  public body: any;
  projets: Projet[];
  projetsCharge: Boolean = false;
  errorMessage: string;

  public constructor(private projetService: ProjetService,
                     private partageService: PartageService,
                     private router: Router) {
    this.nav = document.querySelector('nav.navbar');
    this.body = document.querySelector('body');
  }

  public ngOnInit(): any {
    this.nav.className += ' white-bg';
    this.body.className += ' top-navigation';
    this.setPartage();
    this.getProjets();
  }


  public ngOnDestroy(): any {
    this.nav.classList.remove('white-bg');
    this.body.classList.remove('top-navigation');
  }
  setPartage(){
    this.partageService.setHeader('Gestion et Suivi-Evaluation de Projets');
    this.partageService.setIdProjet(null);
    this.partageService.setNomProjet(null);
  }
  getProjets() {
    this.projetService.getProjets()
      .subscribe(data => {
          this.projetsCharge = false;
          this.projets = data;
          this.projetsCharge = true;
        },
        error => {
          this.errorMessage = <any> error;
        }
      );
  }


}
