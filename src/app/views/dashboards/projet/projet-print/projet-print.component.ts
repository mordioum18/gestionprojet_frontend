import {Component, OnInit} from '@angular/core';
import {Projet} from '../../../../model/projet';
import {Lot} from '../../../../model/lot';

@Component({
  selector: 'app-projet-print',
  templateUrl: './projet-print.component.html',
  styleUrls: ['./projet-print.component.css']
})
export class ProjetImpressionComponent implements OnInit {
  private selectedProjet: Projet;
  private detailLots: Array<Lot>;
  private details: Array<any>;

  constructor() {
  }

  ngOnInit(): void {
    this.selectedProjet = JSON.parse(localStorage.getItem('projet'));
    this.detailLots = JSON.parse(localStorage.getItem('lots'));
    this.details = JSON.parse(localStorage.getItem('details'));
  }

}
