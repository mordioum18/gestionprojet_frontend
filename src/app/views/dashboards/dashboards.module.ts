import {NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import {Dashboard1Component} from './dashboard1.component';
import {Dashboard2Component} from './dashboard2.component';
import {Dashboard3Component} from './dashboard3.component';
import {Dashboard4Component} from './dashboard4.component';
import {Dashboard41Component} from './dashboard41.component';
import {Dashboard5Component} from './dashboard5.component';

// Chart.js Angular 2 Directive by Valor Software (npm)
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { FlotModule } from '../../components/charts/flotChart';
import { IboxtoolsModule } from '../../components/common/iboxtools/iboxtools.module';
import { PeityModule } from '../../components/charts/peity';
import { SparklineModule } from '../../components/charts/sparkline';
import { JVectorMapModule } from '../../components/map/jvectorMap';

import { DateTimePickerModule } from 'ng-pick-datetime';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { Ng4FilesModule } from 'angular4-files-upload';

import { ProjetComponent } from './projet/projet.component';
import { ProjetDetailComponent } from './projet-detail/projet.detail.component';
import { AnalyseProjetComponent } from './analyse/analyse.projet.component';
import { TableProjetComponent } from './projet/table-projet/table.projet.component';
import { TableLotComponent } from './projet-detail/table-lot/table.lot.component';
import { TableFinancementComponent } from './projet-detail/table-financement/table.financement.component';
import { ProjetSuiviComponent } from './projet-suivi/projet.suivi.component';
import { TableSituationFinanceComponent } from './projet-suivi/table-situation-finance/table.situation.finance.component';
import { TableDecaissementComponent } from './projet-suivi/table-decaissement/table.decaissement.component';
import { TableMargeComponent } from './projet-suivi/table-marge/table.marge.component';
import { TableSuiviActiviteComponent } from './projet-suivi/table-suivi-activite/table.suivi.activite.component';
import { TableProgressionActiviteComponent } from './projet-suivi/table-progression-activite/table.progression.activite.component';
import { FormDecaissementComponent } from './projet-suivi/form-decaissement/form.decaissement.component';
import { FormSuiviActiviteComponent } from './projet-suivi/form-suivi-activite/form.suivi.activite.component';
import { TableActeurComponent } from './projet-detail/table-acteur/table.acteur.component';
import { FormProjetComponent } from './projet/form-projet/form.projet.component';
import { FormLotComponent } from './projet-detail/form-lot/form.lot.component';
import { FormActiviteComponent } from './projet-detail/form-activite/form.activite.component';
import { FormOrganisationComponent } from './projet-detail/form-organisation/form.organisation.component';
import { FormFinancementComponent } from './projet-detail/form-financement/form.financement.component';
import { BarChartComponent } from '../../components/ng2Chart/bar-chart.component';
import { PopoverModule } from 'ngx-bootstrap';
import { CarouselModule } from 'ngx-bootstrap';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';
import { UtilsService } from '../../service/utils.service';
import { EnumService } from '../../service/enum.service';
import { PartageService } from '../../service/partage.service';
import { GanttModule } from 'gantt-ui-component';
import { GanttComponent } from '../../components/charts/gantt/gantt.component';
import { AgmCoreModule } from '@agm/core';
import {ROUTES} from '../../app.routes';
import {RouterModule} from '@angular/router';
import {LotDetailComponent} from './lot-detail/lot-detail.component';
import {LotAddOrganisationComponent} from './lot-detail/lot-add-organisation/lot-add-organisation.component';
import {RessourceListComponent} from './projet-suivi/ressource-list/ressource-list.component';
import {RessourceAddComponent} from './projet-detail/ressource-add/ressource-add.component';
import {RessourceAddActiviteComponent} from './projet-suivi/ressource-add-activite/ressource-add-activite.component';
import {TableRessourcesComponent} from './projet-detail/table-ressources/table-ressources.component';
import {WelcomeComponent} from '../appviews/welcome/welcome.component';
import { SeparateurMillePipe } from '../../components/common/pipe/separateur.mille.pipe';
import {LayoutsModule} from '../../components/common/layouts/layouts.module';
import {ProjetPrintComponent} from './projet-print/projet-print.component';
import {ProjetImpressionComponent} from './projet/projet-print/projet-print.component';
import {TableContrainteComponent} from './projet-detail/table-contrainte/table-contrainte.component';
import {FormContrainteComponent} from './projet-detail/form-contrainte/form-contrainte.component';
import {TableActiviteContrainteComponent} from './projet-detail/table-contrainte/table-activite-contrainte.component';
import {FormActiviteContrainteComponent} from './projet-detail/form-contrainte/form-activite-contrainte.component';
import {ListeRessourcesComponent} from './projet-suivi/liste-ressources/liste-ressources.component';
import {TableSuiviRessourceComponent} from './projet-suivi/table-suivi-ressource/table-suivi-ressource.component';
import {FormSuiviRessourceComponent} from './projet-suivi/form-suivi-ressource/form-suivi-ressource.component';
import {MyDatePickerComponent} from './my-date-picker/my-date-picker.component';

@NgModule({
  declarations: [
    ProjetComponent,
    MyDatePickerComponent,
    ListeRessourcesComponent,
    TableSuiviRessourceComponent,
    FormSuiviRessourceComponent,
    AnalyseProjetComponent,
    WelcomeComponent,
    SeparateurMillePipe,
    ProjetPrintComponent,
    ProjetImpressionComponent,
    TableRessourcesComponent,
    RessourceListComponent,
    RessourceAddComponent,
    RessourceAddActiviteComponent,
    LotAddOrganisationComponent,
    LotDetailComponent,
    ProjetSuiviComponent,
    TableSituationFinanceComponent,
    TableProgressionActiviteComponent,
    TableDecaissementComponent,
    TableMargeComponent,
    TableSuiviActiviteComponent,
    FormSuiviActiviteComponent,
    FormDecaissementComponent,
    FormProjetComponent,
    TableProjetComponent,
    ProjetDetailComponent,
    TableLotComponent,
    FormLotComponent,
    TableActeurComponent,
    FormActiviteComponent,
    FormOrganisationComponent,
    TableFinancementComponent,
    FormFinancementComponent,
    BarChartComponent,
    GanttComponent,
    Dashboard1Component,
    Dashboard2Component,
    Dashboard3Component,
    Dashboard4Component,
    Dashboard41Component,
    Dashboard5Component,
    TableContrainteComponent,
    FormContrainteComponent,
    TableActiviteContrainteComponent,
    FormActiviteContrainteComponent
  ],
  imports     : [
    LayoutsModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBsnmjmQZoyf4rCBdN_b8q4R9uf_pg0vf8'
    }),
    PopoverModule.forRoot(),
    CarouselModule.forRoot(),
    NgxSmartModalModule.forRoot(),
    GanttModule.forRoot(),
    DateTimePickerModule,
    Ng4FilesModule,
    ChartsModule,
    FlotModule,
    IboxtoolsModule,
    PeityModule,
    SparklineModule,
    JVectorMapModule,
    RouterModule.forRoot(ROUTES)
  ],
  exports     : [
    ProjetComponent,
    ProjetSuiviComponent,
    Dashboard1Component,
    Dashboard2Component,
    Dashboard3Component,
    Dashboard4Component,
    Dashboard41Component,
    Dashboard5Component
  ],
  providers: [NgxSmartModalService, UtilsService, EnumService, PartageService]
})

export class DashboardsModule {}
