import {ProjetService} from '../../../service/projet.service';
import {ActiviteService} from '../../../service/activite.service';
import {LotService} from '../../../service/lot.service';
import {Component, Input, OnInit} from '@angular/core';
import {UtilsService} from '../../../service/utils.service';
import {PartageService} from '../../../service/partage.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Projet} from '../../../model/projet';
import {Document} from '../../../model/document';
import {RessourceService} from '../../../service/ressource.service';
import {ActiviteRessource} from '../../../model/activite-ressource';
import {document} from 'ngx-bootstrap/utils/facade/browser';
import {DatePipe} from '@angular/common';
import {Ressource} from '../../../model/ressource';
import {SuiviRessource} from '../../../model/suivi-ressource';
import {Activite} from '../../../model/activite';
import {SuiviActivite} from '../../../model/suivi-activite';

declare var jQuery: any;
@Component({
  selector: 'app-projet-print',
  templateUrl: './projet-print.component.html',
  styleUrls: ['./projet-print.component.css'],
  providers: [ProjetService, LotService, ActiviteService, RessourceService],
})
export class ProjetPrintComponent implements OnInit {
  projet: Projet;
  errorMessage: any;
  @Input() startDateSelected: Date;
  @Input() endDateSelected: Date;
  public lineChartLabels: Array<any> = [];
  private tabRessources: Array<any>;
  private ressources: Array<any>;
  private tabActivites: Array<any>;
  private allActivites: Array<any>;
  activites: Activite[];

  constructor(private projetService: ProjetService,
              private lotService: LotService,
              private partageService: PartageService,
              private activiteService: ActiviteService,
              private ressourceService: RessourceService,
              private utilsService: UtilsService,
              public ngxSmartModalService: NgxSmartModalService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    console.log('===================' , document.title);
    console.log('+++++++++++++++++++' , document.URL);
   /* console.log('============== projet print START date selected BY DEFAULT ++++++++++++++++++++', this.startDateSelected);
    console.log('============== projet print END date selected BY DEFAULT++++++++++++++++++++', this.endDateSelected);*/
    const id = this.route.snapshot.paramMap.get('id');
    this.getProjetsById(id);
    document.title = 'Suivi et Evaluation de Projets du Ministère de l\'Enseignement Supérieur de la Recherche et de l\'Innovation';
  }

  startDateSelectedEvent(eventStartDate: Date) {
    this.startDateSelected = eventStartDate;
  /*  console.log('============== projet print start date selected ++++++++++++++++++++', this.startDateSelected);
    console.log('============== projet print start date selected getTime ++++++++++++++++++++', this.startDateSelected.getTime());*/
    if (this.endDateSelected != null) {
      this.onStartAndEndDateSelectedEvent();
    }
  }

  endDateSelectedEvent(eventEndDate: Date) {
    this.endDateSelected = eventEndDate;
   /* console.log('============== projet print end date selected ++++++++++++++++++++', this.endDateSelected);
    console.log('============== projet print end date selected getTime++++++++++++++++++++', this.endDateSelected.getTime());*/
    this.onStartAndEndDateSelectedEvent();
  }

  onStartAndEndDateSelectedEvent() {
    this.getRessources(this.projet.id, this.startDateSelected, this.endDateSelected);
    this.getLots(this.projet.id, this.startDateSelected, this.endDateSelected);
    this.getActivites(this.projet.id, this.startDateSelected, this.endDateSelected);
  }

  getProjetsById(projetId: string) {
    this.projetService.getProjetsById(projetId)
      .subscribe(data => {
          this.projet = data;
          this.updatePartage(this.projet);
        /*  let t = document.getElementsByTagName('footer');
          t.className += 'myFooter';
          let x = document.getElementsByClassName('myFooter');
          x.value = 'mon footer';
          console.log('=================== OHHHHHHHHHHHH ============= ', x);
          console.log('=================== OHHHHHHHHHHHH +++++++++++++ ' + x);
          console.log('=================== OHHHHHHHHHHHH +++++++++++++ ', x.value);
          console.log('=================== OHHHHHHHHHHHH +++++++++++++ ' + x.value);*/
        },
        error => {
          this.errorMessage = <any> error;
        }
      );
  }
  updatePartage(projet: Projet) {
    this.partageService.setNomProjet(projet.nom);
    this.partageService.setIdProjet(projet.id);
  }

  getRessources(projet: number, startDate: Date, endDate: Date) {
    const dateStart = new Date(startDate);
    const dateEnd = new Date(endDate);
    dateStart.setHours(0, 0, 0, 0);
    dateEnd.setHours(0, 0, 0, 0);
    this.ressourceService.getRessources(projet)
      .subscribe(
        ressources => {
          this.projet.ressources = ressources;
          this.tabRessources = [];
          this.ressources = [];
          for (let k = 0; k < this.projet.ressources.length; k++) {
            this.ressourceService.getSuivis(this.projet.ressources[k].id, projet)
              .subscribe(suiviRessources => {
                  this.projet.ressources[k].suiviRessources = suiviRessources;
                  for (let i = 0; i < this.projet.ressources[k].suiviRessources.length; i++) {
                    const suiviDate = new Date(this.projet.ressources[k].suiviRessources[i].dateSuivi);
                    suiviDate.setHours(0, 0, 0, 0);
                   /* console.log('=====suivi ressource ========', (this.projet.ressources[k].suiviRessources[i]));
                    console.log('=====suivi quantite ========', (this.projet.ressources[k].suiviRessources[i].quantite));
                    console.log('=====suivi nom========', (this.projet.ressources[k].suiviRessources[i].ressource.nom));
                    console.log('+++++++++++ suiviDate getTime +++++++++++', suiviDate.getTime());
                    console.log('+++++++++++ suiviDate +++++++++++', suiviDate);
                    console.log('=====start date getTime========', dateStart.getTime());
                    console.log('=====start date========', dateStart);
                    console.log('=====end date getTime========', dateEnd.getTime());
                    console.log('=====end date ========', dateEnd);*/

                    if (suiviDate.getTime() === dateStart.getTime()) {
                      /*console.log('EGALITE START DATE ****************************');
                      console.log('$$$$$$$$$$$', this.projet.ressources[k].suiviRessources[i]);
                      console.log('$$$$$$$$$$$', this.projet.ressources[k].suiviRessources[i].quantite);
                      console.log('$$$$$$$$$$$', 0);*/
                      this.tabRessources.push({
                        suivi : this.projet.ressources[k].suiviRessources[i],
                        startQuantite : this.projet.ressources[k].suiviRessources[i].quantite,
                        endQuantite: 0,
                        date: suiviDate
                      });
                    } else {
                       if (suiviDate.getTime() === dateEnd.getTime()) {
                        /*console.log('EGALITE END DATE ****************************');*/
                        this.tabRessources.push({
                          suivi : this.projet.ressources[k].suiviRessources[i],
                          endQuantite : this.projet.ressources[k].suiviRessources[i].quantite,
                          startQuantite: 0,
                          date: suiviDate
                        });
                      }
                    }
                      /*if (suiviDate < dateStart || suiviDate > dateEnd) {*/
                  }
                  for (let l = 0; l < this.tabRessources.length; l++) {
                    for (let m = l + 1; m < this.tabRessources.length; m++) {
                      if (this.tabRessources[l].suivi.ressource.id === this.tabRessources[m].suivi.ressource.id) {
                       /* console.log('*********** lll *********', this.tabRessources[l].suivi);
                        console.log('*********** m *********', this.tabRessources[m].suivi);*/
                        this.ressources.push({
                          suivi : this.tabRessources[l].suivi,
                          endQuantite : this.tabRessources[l].endQuantite + this.tabRessources[m].endQuantite,
                          startQuantite: this.tabRessources[l].startQuantite + this.tabRessources[m].startQuantite
                        });
                        this.tabRessources.splice(m, 1);
                      }
                    }
                    if (this.ressources.length === l) {
                     /* console.log('*********** l *********', this.tabRessources[l].suivi);*/
                      this.ressources.push({
                        suivi : this.tabRessources[l].suivi,
                        endQuantite : this.tabRessources[l].endQuantite,
                        startQuantite: this.tabRessources[l].startQuantite
                      });
                    }
                  }
                },
                error => {
                  console.log('************* error to get suiviRessources of ressource *******');
                  this.errorMessage = <any> error;
                }
              );
          }
                  
        },
        error => {
          console.log('************* error to get ressources of project *******');
          this.errorMessage = <any> error;
        }
      );
  }

    getActivites(projet: number, startDate: Date, endDate: Date) {
    const dateStart = new Date(startDate);
    const dateEnd = new Date(endDate);
    dateStart.setHours(0, 0, 0, 0);
    dateEnd.setHours(0, 0, 0, 0);
    this.activiteService.getAllActivites(projet)
    .subscribe(
      activites => {
        this.activites = activites;
        this.tabActivites = [];
        this.allActivites = [];
        for (let k = 0; k < this.activites.length; k++){
          this.activiteService.getSuivis(projet, this.activites[k].id)
            .subscribe(suiviActivites => {
                this.activites[k].suiviActivites = suiviActivites;
                for (let i = 0; i < this.activites[k].suiviActivites.length; i++) {
                    const suiviDate = new Date(this.activites[k].suiviActivites[i].dateSuivi);
                    suiviDate.setHours(0, 0, 0, 0);
                    console.log('=====suivi activite ========', (this.activites[k].suiviActivites[i]));
                    console.log('=====suivi niveauRealisation ========', (this.activites[k].suiviActivites[i].niveauRealisation));
                    console.log('=====suivi nom========', (this.activites[k].suiviActivites[i].activite.libelle));
                    console.log('+++++++++++ suiviDate getTime +++++++++++', suiviDate.getTime());
                    console.log('+++++++++++ suiviDate +++++++++++', suiviDate);
                    console.log('=====start date getTime========', dateStart.getTime());
                    console.log('=====start date========', dateStart);
                    console.log('=====end date getTime========', dateEnd.getTime());
                    console.log('=====end date ========', dateEnd);
                    if (suiviDate.getTime() === dateStart.getTime()) {
                      console.log('EGALITE START DATE ****************************');
                      console.log('$$$$$$$$$$$ object EGALITE START ', this.activites[k].suiviActivites[i]);
                      console.log('$$$$$$$$$$$ realisation EGALITE START ', this.activites[k].suiviActivites[i].niveauRealisation);
                      this.tabActivites.push({
                        suivi : this.activites[k].suiviActivites[i],
                        startNiveauRealisation : this.activites[k].suiviActivites[i].niveauRealisation,
                        endNiveauRealisation: 0,
                        date: suiviDate
                      });
                    } else {
                       if (suiviDate.getTime() === dateEnd.getTime()) {
                        console.log('EGALITE END DATE ****************************');
                        console.log('$$$$$$$$$$$ object EGALITE END  ', this.activites[k].suiviActivites[i]);
                        console.log('$$$$$$$$$$$ realisation EGALITE END ', this.activites[k].suiviActivites[i].niveauRealisation);
                        this.tabActivites.push({
                          suivi : this.activites[k].suiviActivites[i],
                          endNiveauRealisation : this.activites[k].suiviActivites[i].niveauRealisation,
                          startNiveauRealisation: 0,
                          date: suiviDate
                        });
                      }
                    }
                  }
                  for (let l = 0; l < this.tabActivites.length; l++) {
                    for (let m = l + 1; m < this.tabActivites.length; m++) {
                      if (this.tabActivites[l].suivi.activite.id === this.tabActivites[m].suivi.activite.id) {
                        console.log('*********** lll *********', this.tabActivites[l].suivi);
                        console.log('*********** m *********', this.tabActivites[m].suivi);
                        console.log('*********** ppppp end LLL *********', this.tabActivites[l].endNiveauRealisation);
                        console.log('*********** ppppp start LLL *********', this.tabActivites[l].startNiveauRealisation);
                        console.log('*********** ppppp end MMM *********', this.tabActivites[m].endNiveauRealisation);
                        console.log('*********** ppppp start MMM *********', this.tabActivites[m].startNiveauRealisation);
                        this.allActivites.push({
                          suivi : this.tabActivites[l].suivi,
                          endNiveauRealisation : this.tabActivites[l].endNiveauRealisation + this.tabActivites[m].endNiveauRealisation,
                          startNiveauRealisation: this.tabActivites[l].startNiveauRealisation + this.tabActivites[m].startNiveauRealisation
                        });
                         console.log('*********** ppppp start ALLLLL  *********', this.allActivites[l].startNiveauRealisation);
                         console.log('*********** ppppp end ALLLLL  *********', this.allActivites[l].endNiveauRealisation);
                        this.tabActivites.splice(m, 1);
                      }
                    }
                    if (this.allActivites.length === l) {
                       console.log('*********** l *********', this.tabActivites[l].suivi);
                      this.allActivites.push({
                        suivi : this.tabActivites[l].suivi,
                        endNiveauRealisation : this.tabActivites[l].endNiveauRealisation,
                        startNiveauRealisation: this.tabActivites[l].startNiveauRealisation
                      });
                    }
                  }
              },
              error => {
                this.errorMessage = <any> error;
              }
            );
        }
      }
    );
  }

  getLots(projet: number, startDate: Date, endDate: Date) {
    const dateStart = new Date(startDate);
    const dateEnd = new Date(endDate);
    dateStart.setHours(0, 0, 0, 0);
    dateEnd.setHours(0, 0, 0, 0);
    this.lotService.getLots(projet)
      .subscribe(lots => {
          this.projet.lots = lots;
          for (let i = 0; i < this.projet.lots.length; i++) {
            this.lotService.getActivites(projet.toString(), (this.projet.lots[i].id).toString())
              .subscribe(activites => {
                  this.projet.lots[i].activites = activites;
                  for (let j = 0; j < this.projet.lots[i].activites.length; j++) {
                    this.activiteService.getSuivis(projet, this.projet.lots[i].activites[j].id)
                      .subscribe(suiviActivites => {
                        this.projet.lots[i].activites[j].suiviActivites = suiviActivites;
                        for (let k = 0; k < this.projet.lots[i].activites[j].suiviActivites.length; k++) {
                          const suiviDate = new Date(this.projet.lots[i].activites[j].suiviActivites[k].dateSuivi);
                          suiviDate.setHours(0, 0, 0, 0);
                         /* console.log('=====suivi activite ========', (this.projet.lots[i].activites[j].suiviActivites[k]));
                          console.log('=====suivi nom========', (this.projet.lots[i].activites[j].suiviActivites[k].activite.libelle));
                          console.log('+++++++++++ suiviDate +++++++++++', suiviDate);
                          console.log('=====start date========', dateStart);
                          console.log('=====end date ========', dateEnd);*/
                          if (suiviDate < dateStart || suiviDate > dateEnd) {
                          /*  console.log('=====elimined suivi========', (this.projet.lots[i].activites[j].suiviActivites[k]));
                            console.log('=====elimined nom========', (this.projet.lots[i].activites[j].suiviActivites[k].activite.libelle));*/
                            this.projet.lots[i].activites[j].suiviActivites.splice(i, 1);
                          }
                        }
                      },
                      error => {
                        this.errorMessage = <any> error;
                      }
                    );
                  }
                },
                error => {
                  this.errorMessage = <any> error;
                }
              );
            this.lotService.getLotOrganisations((this.projet.lots[i].id).toString(), projet.toString())
              .subscribe( lotOrganisations => {
                  this.projet.lots[i].lotOrganisations = lotOrganisations;
                },
                error => {
                  console.log('************* error to get lotOrganisations of lot *******');
                  this.errorMessage = <any> error;
                }
              );
          }
        },
        error => {
          this.errorMessage = <any> error;
        }
      );
  }

  getActeurs(id: number) {
    this.projetService.getActeurs(id)
      .subscribe(utilisateurProjets => {
          this.projet.utilisateurProjets = utilisateurProjets;
        },
        error => {
          this.errorMessage = <any> error;
        }
      );
  }

  endBudgetTotauxHumains() {
    let somme = 0;
    if (this.ressources !== undefined && this.ressources !== null) {
      for (let i = 0; i < this.ressources.length; i++) {
          if (this.ressources[i].suivi.ressource.typeRessource === 'Ressource Humaine') {
            somme = Number.parseFloat(somme.toString()) + Number.parseFloat(this.ressources[i].endQuantite.toString());
          }
      }
    }
    return somme;
  }

  endBudgetTotauxMateriel() {
    let somme = 0;
    if (this.ressources !== undefined && this.ressources !== null) {
      for (let i = 0; i < this.ressources.length; i++) {
          if (this.ressources[i].suivi.ressource.typeRessource === 'Ressource Matérielle') {
            somme = Number.parseFloat(somme.toString()) + Number.parseFloat(this.ressources[i].endQuantite.toString());
          }
      }
    }
    return somme;
  }

  endBudgetTotauxMateriaux() {
    let somme = 0;
    if (this.ressources !== undefined && this.ressources !== null) {
      for (let i = 0; i < this.ressources.length; i++) {
          if (this.ressources[i].suivi.ressource.typeRessource === 'Matériaux') {
            somme = Number.parseFloat(somme.toString()) + Number.parseFloat(this.ressources[i].endQuantite.toString());
          }
      }
    }
    return somme;
  }

  startBudgetTotauxHumains() {
    let somme = 0;
    if (this.ressources !== undefined && this.ressources !== null) {
      for (let i = 0; i < this.ressources.length; i++) {
        if (this.ressources[i].suivi.ressource.typeRessource === 'Ressource Humaine') {
          somme = Number.parseFloat(somme.toString()) + Number.parseFloat(this.ressources[i].startQuantite.toString());
        }
      }
    }
    return somme;
  }

  startBudgetTotauxMateriel() {
    let somme = 0;
    if (this.ressources !== undefined && this.ressources !== null) {
      for (let i = 0; i < this.ressources.length; i++) {
        if (this.ressources[i].suivi.ressource.typeRessource === 'Ressource Matérielle') {
          somme = Number.parseFloat(somme.toString()) + Number.parseFloat(this.ressources[i].startQuantite.toString());
        }
      }
    }
    return somme;
  }

  startBudgetTotauxMateriaux() {
    let somme = 0;
    if (this.ressources !== undefined && this.ressources !== null) {
      for (let i = 0; i < this.ressources.length; i++) {
        if (this.ressources[i].suivi.ressource.typeRessource === 'Matériaux') {
          somme = Number.parseFloat(somme.toString()) + Number.parseFloat(this.ressources[i].startQuantite.toString());
        }
      }
    }
    return somme;
  }

 /* quantiteTotale(activiteRessources: ActiviteRessource[]): number {
    let somme = 0;
    if (activiteRessources !== undefined && activiteRessources !== null) {
      for (let i = 0; i < activiteRessources.length; i++) {
        somme = Number.parseFloat(somme.toString()) + Number.parseFloat(activiteRessources[i].quantite.toString());
      }
    }
    return somme;
  }*/

   sourceBase64(doc: Document) {
    if (doc !== undefined && doc !== null) {
      return doc.typeDocument + ',' + doc.chemin;
    }
  }

  addRow() {
    const someRow = '<tr class=\'someClass\' ><th id=\'header1\' >text1</th><th>text2</th></tr>'; // add resources
    jQuery('#human-ressources-suivi').append(someRow);
    jQuery('#header1').text('Banana');
  }


}
