import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {SuiviActiviteService} from '../../../../service/suivi.activite.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SuiviActivite} from '../../../../model/suivi-activite';
import {Activite} from '../../../../model/activite';
import {
  Ng4FilesStatus,
  Ng4FilesSelected,
  Ng4FilesService,
  Ng4FilesConfig,
} from 'angular4-files-upload';
import {Document} from '../../../../model/document';

@Component({
  selector: 'app-form-suivi-activite',
  templateUrl: './form.suivi.activite.component.html',
  styleUrls: ['./form.suivi.activite.component.css'],
  providers: [SuiviActivite],
})

export class FormSuiviActiviteComponent implements OnInit {
  errorMessage: any;
  @Input() activite: Activite;
  @Input() suiviActivite: SuiviActivite;
  @Output() retourSuiviActivite: EventEmitter<SuiviActivite> = new EventEmitter();
  formSuiviActivite: FormGroup;
  images: Document[] = [];
  errorFile: string;
  imageConfig: Ng4FilesConfig = {
    acceptExtensions: ['png', 'PNG', 'jpg', 'JPG'],
    maxFilesCount: 10,
    maxFileSize: 2000000,
    totalFilesSize: 20000000
  };
  constructor(private suiviActiviteService: SuiviActiviteService,
              public ngxSmartModalService: NgxSmartModalService,
              private ng4FilesService: Ng4FilesService,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.ng4FilesService.addConfig(this.imageConfig, 'image-config');
    this.formSuiviActivite = this.fb.group({
      'dateSuiviActivite': ['', Validators.required],
      'niveauRealisation': ['', Validators.required],
      'observation': [''],
    });
  }

  public filesSelect(selectedFiles: Ng4FilesSelected): void {
    if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_TOTAL_SIZE_EXCEED) {
      this.errorMessage = 'trop d\'image';
      return;
    }
    if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_COUNT_EXCEED) {
      this.errorMessage = 'trop d\'image';
      return;
    }
    for (let i = 0 ; i < selectedFiles.files.length; i++){
      const reader = new FileReader();
      const file = selectedFiles.files[i];
      reader.readAsDataURL(file);
      reader.onload = () => {
        const doc = new Document();
        doc.chemin = reader.result;
        const tab = file.name.split('.');
        if (tab.length >= 2){
          const ext = tab[tab.length - 1];
          if (ext === 'png' || ext === 'PNG' || ext === 'jpg' || ext === 'JPG'){
            doc.extension = ext;
            this.images.push(doc);
          }
        }
      };
    }
  }
  removeImage(i){
    this.images.splice(i, 1);
  }
  private onSubmit(form){
    const debutActivite: any = this.activite.dateExecutionPrevisionnelle;
    if (debutActivite < this.suiviActivite.dateSuivi.getTime()){
      this.suiviActivite.activite = this.activite;
      this.suiviActivite.documents = [];
      for (let i = 0; i < this.images.length; i++){
        const doc = this.images[i];
        const base64Encode = doc.chemin.split(',');
        doc.chemin = base64Encode[1];
        doc.typeDocument = base64Encode[0];
        this.suiviActivite.documents.push(doc);
      }
      this.suiviActiviteService.postSuiviActivite(this.suiviActivite).subscribe(
        data => {
          this.activite.suiviActivites.push(data);
          this.retourSuiviActivite.emit(data);
          this.images = [];
          if (this.activite.tauxExecutionPhysique < this.suiviActivite.niveauRealisation){
            this.activite.tauxExecutionPhysique = this.suiviActivite.niveauRealisation;
          }
          this.suiviActivite = new SuiviActivite();
        },
        (error) => {
          console.log('Erreur submit');
          this.images = [];
          this.errorMessage = error;
        }
      );
    }
    else {
      this.errorMessage = 'Date de suivi incorrecte';
    }
  }
}
