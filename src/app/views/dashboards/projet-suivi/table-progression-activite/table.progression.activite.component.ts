import {Component, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import {ActiviteService} from '../../../../service/activite.service';
import {SuiviActiviteService} from '../../../../service/suivi.activite.service';
import {Activite} from '../../../../model/activite';
import {SuiviActivite} from '../../../../model/suivi-activite';
declare var jQuery:any;

@Component({
  selector: 'app-table-progression-activite',
  templateUrl: './table.progression.activite.component.html',
  styleUrls: ['./table.progression.activite.component.css'],
  providers: [ActiviteService, SuiviActiviteService],
})

export class TableProgressionActiviteComponent implements OnInit, OnChanges {
  @Input() activites: Activite[];
  activite: Activite = null;
  suiviActivite: SuiviActivite;
  private errorMessage: string;

  ngOnInit(): void {
    this.suiviActivite = new SuiviActivite();
  }
  listeSuiviActivite(activite){
    this.activite = activite;
    jQuery('.iconesuivantsuivi').hide();
    jQuery('#iconesuivantsuivi' + activite.id).show();
  }
  dernierRetard(activite: Activite):number{
    if (activite !== undefined && activite !== null &&
        activite.suiviActivites !== undefined && activite.suiviActivites !== null){
      if (activite.suiviActivites.length !==0){
        let dernierSuivi = activite.suiviActivites[0];
        for (let i = 1; i < activite.suiviActivites.length; i++){
          if (dernierSuivi.dateSuivi < activite.suiviActivites[i].dateSuivi){
            dernierSuivi = activite.suiviActivites[i];
          }
        }
        return dernierSuivi.retard;
      }
      else {
        const date = new Date();
        const debut: any = activite.dateExecutionPrevisionnelle;
        return Math.floor((date.getTime() - debut) / (1000 * 3600 * 24));
      }
    }
    else {
      return 0;
    }
  }
  etatActivite(activite: Activite){
    const today = new Date();
    const fin: any = activite.dateFinPrevisionnelle;
    if (this.dernierRetard(activite) > 0 && fin > today.getTime()){
      return 'text-warning';
    }
    if (this.dernierRetard(activite) > 0 && fin < today.getTime()){
      return 'text-danger';
    }
    const debut: any = activite.dateExecutionPrevisionnelle;
    if (this.dernierRetard(activite) < 0 && debut < today.getTime()){
      return 'text-success';
    }
    if (this.dernierRetard(activite) === 0){
      return 'text-info';
    }
  }
  ngOnChanges() {
    if (this.activites !== undefined){
      if (this.activites.length > 0){
        this.activite = this.activites[0];
      }
      else {
        this.activite = null;
      }
    }
  }

}
