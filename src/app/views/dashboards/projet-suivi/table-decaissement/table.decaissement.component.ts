import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {ActiviteService} from '../../../../service/activite.service';
import {DecaissementService} from '../../../../service/decaissement.service';
import {Activite} from '../../../../model/activite';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Decaissement} from '../../../../model/decaissement';
import {Document} from "../../../../model/document";
declare var jQuery:any;

@Component({
  selector: 'app-table-decaissement',
  templateUrl: './table.decaissement.component.html',
  styleUrls: ['./table.decaissement.component.css'],
  providers: [ActiviteService, DecaissementService],
})

export class TableDecaissementComponent implements OnInit, OnChanges {
  @Input() activite: Activite;
  decaissement: Decaissement;
  titreModalDecaissement: string;
  modal = false;
  private errorMessage: string;
  constructor(private activiteService: ActiviteService,
              public ngxSmartModalService: NgxSmartModalService,
              private decaissementService: DecaissementService) {
  }
  ngOnInit(): void {
    this.decaissement = new Decaissement();
  }
  calculMontantTotal(): number{
    let somme = 0;
    if (this.activite !== undefined && this.activite !== null){
      if (this.activite.decaissements !== undefined && this.activite.decaissements !== null){
        for (let i = 0; i < this.activite.decaissements.length; i++){
          if (this.activite.decaissements[i].montant !== null){
            somme = Number.parseFloat(somme.toString()) + Number.parseFloat(this.activite.decaissements[i].montant.toString());
          }
        }
      }
    }
    return somme;
  }
  modalOpen(){
    this.modal = true;
  }
  ajouterDecaissement(){
    this.titreModalDecaissement = 'Ajouter un décaissement';
    this.ngxSmartModalService.getModal('modalDecaissement').open();
  }
  addNewDecaissement(decaissement){
    this.ngxSmartModalService.getModal('modalDecaissement').close();
    this.decaissement = new Decaissement();
  }
  openDocument(doc: Document){
    const byteCharacters = atob(doc.chemin);

    const byteNumbers = new Array(byteCharacters.length);

    for (let i = 0; i < byteCharacters.length; i++)
      byteNumbers[i] = byteCharacters.charCodeAt(i);

    const byteArray = new Uint8Array(byteNumbers);

    const file = new Blob([byteArray], { type: 'application/pdf' });
    const fileURL = URL.createObjectURL(file);
    window.open(fileURL);
  }
  onDelete(decaissement){
    const $this = this;
    gantt.confirm({
      text: 'Voulez vous supprimer ce décaissement : ' + decaissement.libelle,
      ok: 'Ok',
      cancel: 'Annuler',
      callback: function(result){
        if (result === true){
          $this.decaissementService.deleteDecaissement($this.activite.lot.projet.id, decaissement.id).subscribe(
            data =>{
              for (let i = 0; i < $this.activite.decaissements.length; i++){
                if ($this.activite.decaissements[i].id === decaissement.id){
                  $this.activite.decaissements.splice(i, 1);
                  break;
                }
              }
            }
          );
        }
      }
    });
  }
  ngOnChanges() {
  }

}
