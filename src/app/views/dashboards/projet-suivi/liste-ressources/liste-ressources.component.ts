import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Projet} from '../../../../model/projet';
import {RessourceGroupe} from '../../../../model/ressource-groupe';
import {Activite} from '../../../../model/activite';
import {Ressource} from '../../../../model/ressource';
import {Router} from '@angular/router';
import {ProjetService} from '../../../../service/projet.service';
import {RessourceService} from '../../../../service/ressource.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {SuiviRessource} from '../../../../model/suivi-ressource';
import {ActiviteService} from '../../../../service/activite.service';
import {el} from '@angular/platform-browser/testing/src/browser_util';

@Component({
  selector: 'app-liste-ressources',
  templateUrl: './liste-ressources.component.html',
  styleUrls: ['./liste-ressources.component.css'],
  providers: [RessourceService]
})
export class ListeRessourcesComponent implements OnInit, OnChanges {
  @Input() ressources: Ressource[];
  ressource: Ressource;
  suiviRessource: SuiviRessource;
  modal = false;
  private errorMessage: string;
  activites: Activite[];

  constructor(private ressourceService: RessourceService,
              public ngxSmartModalService: NgxSmartModalService,
              private router: Router) {
  }

  ngOnInit(): void {
    // this.suiviRessource = new SuiviRessource();
  }

  ngOnChanges() {
      if (this.ressources !== undefined) {
        if (this.ressources.length > 0) {
          this.ressource = this.ressources[0];
        } else {
          this.ressource = null;
        }
      }
  }

  derniereQuantiteSuivi(ressource: Ressource): number {
    let dernierSuiviRessource = new SuiviRessource();
    if (this.ressource !== undefined && ressource !== null &&
      ressource.suiviRessources !== undefined && ressource.suiviRessources !== null && ressource.suiviRessources.length !== 0) {
        dernierSuiviRessource = ressource.suiviRessources[0];
        for (let i = 0; i < ressource.suiviRessources.length; i++) {
          if (dernierSuiviRessource.dateSuivi <= ressource.suiviRessources[i].dateSuivi) {
            dernierSuiviRessource = ressource.suiviRessources[i];
            console.log('=========== dernierSuiviRessource ===========', dernierSuiviRessource.quantite);
          }
        }
      return dernierSuiviRessource.quantite;
    } else {
      console.log('=========== dernierSuiviRessource ZERO ===========', 0);
      return 0;
    }
  }

  listeSuiviRessources (ressource) {
    this.ressource = ressource;
  }

}
