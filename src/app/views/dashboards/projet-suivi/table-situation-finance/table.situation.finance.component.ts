import {Component, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import {ActiviteService} from '../../../../service/activite.service';
import {DecaissementService} from '../../../../service/decaissement.service';
import {Activite} from '../../../../model/activite';
import {Decaissement} from '../../../../model/decaissement';
declare var jQuery:any;

@Component({
  selector: 'app-table-situation-finance',
  templateUrl: './table.situation.finance.component.html',
  styleUrls: ['./table.situation.finance.component.css'],
  providers: [ActiviteService, DecaissementService],
})

export class TableSituationFinanceComponent implements OnInit, OnChanges {
  @Input() activites: Activite[];
  activite: Activite = null;
  decaissement: Decaissement;
  totalDecaissement = 0;
  private errorMessage: string;

  ngOnInit(): void {
    this.decaissement = new Decaissement();
  }
  listeDecaissement(activite){
    this.activite = activite;
    jQuery('.iconesuivant').hide();
    jQuery('#iconesuivant' + activite.id).show();
  }

  decaissementTotal(decaissements: Decaissement[]):number{
    let somme = 0;
    if (decaissements !== undefined && decaissements !== null){
      for (let i = 0; i < decaissements.length; i++){
        somme= Number.parseFloat(somme.toString()) + Number.parseFloat(decaissements[i].montant.toString());
      }
    }
    return somme;
  }
  decaissementTotaux(): number{
    let somme =0;
    if (this.activites !== undefined && this.activites !== null){
      for (let i = 0; i < this.activites.length; i++){
        if (this.activites[i].decaissements !== undefined && this.activites[i].decaissements !== null){
          for (let k = 0; k < this.activites[i].decaissements.length; k++){
            somme = Number.parseFloat(somme.toString()) + Number.parseFloat(this.activites[i].decaissements[k].montant.toString());
          }
        }
      }
    }
    return somme;
  }
  budgetTotaux(){
    let somme =0;
    if (this.activites !== undefined && this.activites !== null){
      for (let i = 0; i < this.activites.length; i++){
        somme= Number.parseFloat(somme.toString()) + Number.parseFloat(this.activites[i].budget.toString());
      }
    }
    return somme;
  }
  ngOnChanges() {
    if (this.activites !== undefined){
      if (this.activites.length > 0){
        this.activite = this.activites[0];
      }
      else {
        this.activite = null;
      }
    }
  }

}
