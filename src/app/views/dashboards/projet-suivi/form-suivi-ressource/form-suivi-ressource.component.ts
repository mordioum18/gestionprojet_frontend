import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SuiviRessourceService} from '../../../../service/suivi-ressource.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Ressource} from '../../../../model/ressource';
import {SuiviRessource} from '../../../../model/suivi-ressource';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {SuiviActiviteService} from '../../../../service/suivi.activite.service';
import {Ng4FilesService} from 'angular4-files-upload/index';
import {SuiviActivite} from '../../../../model/suivi-activite';

@Component({
  selector: 'app-form-suivi-ressource',
  templateUrl: './form-suivi-ressource.component.html',
  styleUrls: ['./form-suivi-ressource.component.css'],
  providers: [SuiviRessourceService]
})
export class FormSuiviRessourceComponent implements OnInit {
  errorMessage: any;
  @Input() ressource: Ressource;
  @Input() suiviRessource: SuiviRessource;
  @Output() retourSuiviRessource: EventEmitter<SuiviRessource> = new EventEmitter();
  formSuiviRessource: FormGroup;

  constructor(private suiviRessourceService: SuiviRessourceService,
              public ngxSmartModalService: NgxSmartModalService,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.formSuiviRessource = this.fb.group({
      'dateSuiviRessource': ['', Validators.required],
      'quantite': ['', Validators.required]
    });
  }

  derniereDateSuivi(ressource: Ressource): Date {
    let dernierSuiviRessource = new SuiviRessource();
    if (this.ressource !== undefined && ressource !== null &&
      ressource.suiviRessources !== undefined && ressource.suiviRessources !== null && ressource.suiviRessources.length !== 0) {
      dernierSuiviRessource = ressource.suiviRessources[0];
      for (let i = 0; i < ressource.suiviRessources.length; i++) {
        if (dernierSuiviRessource.dateSuivi <= ressource.suiviRessources[i].dateSuivi) {
          dernierSuiviRessource = ressource.suiviRessources[i];
          console.log('=========== dernierSuiviRessource dateSuivi===========', dernierSuiviRessource.dateSuivi);
        }
      }
      return dernierSuiviRessource.dateSuivi;
    } else {
      console.log('=========== dernierSuiviRessource zero suivi dateSuivi egal CREATION ressource ===========', ressource.dateCreation);
      return ressource.dateCreation;
    }
  }

  private onSubmit(form) {
    let dateRes: any = this.derniereDateSuivi(this.ressource);
    if (dateRes < this.suiviRessource.dateSuivi.getTime()) {
      this.suiviRessource.ressource = this.ressource;
      this.suiviRessourceService.postSuiviRessource(this.suiviRessource)
        .subscribe (
        data => {
          this.ressource.suiviRessources.push(data);
          this.retourSuiviRessource.emit(data);
          this.suiviRessource = new SuiviRessource();
          this.ngxSmartModalService.getModal('modalSuiviRessource').close();
        },
        (error) => {
          console.log('Erreur submit suiviRessource');
          this.errorMessage = error;
          this.suiviRessource = new SuiviRessource();
        }
      );
    } else {
      this.errorMessage = 'Date de suivi incorrecte';
      this.suiviRessource = new SuiviRessource();
    }
  }
}
