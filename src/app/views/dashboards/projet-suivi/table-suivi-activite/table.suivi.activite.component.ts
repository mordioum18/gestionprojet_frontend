import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {ActiviteService} from '../../../../service/activite.service';
import {SuiviActiviteService} from '../../../../service/suivi.activite.service';
import {Activite} from '../../../../model/activite';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {SuiviActivite} from '../../../../model/suivi-activite';
import {Document} from "../../../../model/document";
declare var jQuery:any;

@Component({
  selector: 'app-table-suivi-activite',
  templateUrl: './table.suivi.activite.component.html',
  styleUrls: ['./table.suivi.activite.component.css'],
  providers: [ActiviteService, SuiviActiviteService],
})

export class TableSuiviActiviteComponent implements OnInit, OnChanges {
  @Input() activite: Activite;
  suiviActivite: SuiviActivite;
  titreModalSuiviActivite: string;
  modal = false;
  private errorMessage: string;
  constructor(private activiteService: ActiviteService,
              public ngxSmartModalService: NgxSmartModalService,
              private suiviActiviteService: SuiviActiviteService) {
  }
  ngOnInit(): void {
    this.suiviActivite = new SuiviActivite();
  }
  modalOpen(){
    this.modal = true;
  }
  ajouterSuiviActivite(){
    this.titreModalSuiviActivite = 'Progression - observations';
    this.ngxSmartModalService.getModal('modalSuiviActivite').open();
  }
  addNewSuiviActivite(suiviActivite){
    this.ngxSmartModalService.getModal('modalSuiviActivite').close();
    this.suiviActivite = new SuiviActivite();
  }
  sourceBase64(doc: Document){
    if (doc !== undefined && doc !== null){
      return doc.typeDocument + ',' + doc.chemin;
    }
  }
  onDelete(suiviActivite){
    const $this = this;
    gantt.confirm({
      text: 'Voulez vous supprimer ce suivi',
      ok: 'Ok',
      cancel: 'Annuler',
      callback: function(result){
        if (result === true){
          $this.suiviActiviteService.deleteSuiviActivite($this.activite.lot.projet.id, suiviActivite.id).subscribe(
            data =>{
              for (let i = 0; i < $this.activite.suiviActivites.length; i++){
                if ($this.activite.suiviActivites[i].id === suiviActivite.id){
                  $this.activite.suiviActivites.splice(i, 1);
                  break;
                }
              }
              let tauxExecution = 0;
              for (let i = 0; i < $this.activite.suiviActivites.length; i++){
                if ($this.activite.suiviActivites[i].niveauRealisation > tauxExecution){
                  tauxExecution = $this.activite.suiviActivites[i].niveauRealisation;
                }
              }
              $this.activite.tauxExecutionPhysique = tauxExecution;
            }
          );
        }
      }
    });
  }
  ngOnChanges() {
  }

}
