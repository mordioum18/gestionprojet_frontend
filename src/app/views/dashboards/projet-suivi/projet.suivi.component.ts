import {Component, OnInit, OnDestroy} from '@angular/core';
import {Projet} from '../../../model/projet';
import {ProjetService} from '../../../service/projet.service';
import {LotService} from '../../../service/lot.service';
import {UtilsService} from '../../../service/utils.service';
import {PartageService} from '../../../service/partage.service';
import {ActiviteService} from '../../../service/activite.service';
import {Router, ActivatedRoute} from '@angular/router';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Activite} from '../../../model/activite';
import {RessourceService} from '../../../service/ressource.service';
import {SuiviRessourceService} from '../../../service/suivi-ressource.service';
import {Ressource} from '../../../model/ressource';
declare var jQuery:any;
@Component({
  selector: 'app-projet-detail',
  templateUrl: './projet.suivi.component.html',
  styleUrls: ['./projet.suivi.component.css'],
  providers: [ProjetService, LotService, ActiviteService, RessourceService, SuiviRessourceService],
})

export class ProjetSuiviComponent implements OnInit, OnDestroy {
  public nav: any;
  public body: any;
  projet: Projet;
  activites: Activite[];
  ressources: Ressource[];
  lotsCharge = false;
  activitesCharge = false;
  errorMessage: any;
  barChartLabels: string[];
  barChartData: any[];
  constructor(private projetService: ProjetService,
              private partageService: PartageService,
              private activiteService: ActiviteService,
              private ressourceService: RessourceService,
              private suiviRessourceService: SuiviRessourceService,
              private utilsService: UtilsService,
              public ngxSmartModalService: NgxSmartModalService,
              private router: Router,
              private route: ActivatedRoute) {
    this.nav = document.querySelector('nav.navbar');
    this.body = document.querySelector('body');
  }
  ngOnInit(): void {
    this.nav.className += ' white-bg';
    this.body.className += ' top-navigation';
    const id = this.route.snapshot.paramMap.get('id');
    this.getProjetsById(id);
  }
  ngOnDestroy(){
    this.body.classList.remove('top-navigation');
  }
  getProjetsById(projetId: string) {
    this.projetService.getProjetsById(projetId)
      .subscribe(data => {
          this.projet = data;
          this.updatePartage(this.projet);
          this.getAllActivites(this.projet.id);
          this.getRessources(this.projet.id);
        },
        error => {
          this.errorMessage = <any> error;
        }
      );
  }
  updatePartage(projet: Projet){
    this.partageService.setHeader('Gestion et Suivi-Evaluation de Projets');
    this.partageService.setNomProjet(projet.nom);
    this.partageService.setIdProjet(projet.id);
  }
  getAllActivites(projet: number){
    this.activiteService.getAllActivites(this.projet.id).subscribe(
      activites =>{
        this.activites = activites;
        for (let i = 0; i < this.activites.length; i++){
          this.activiteService.getDecaissements(this.projet.id, this.activites[i].id)
            .subscribe(decaissements => {
                this.activites[i].decaissements = decaissements;
              },
              error => {
                this.errorMessage = <any> error;
              }
            );
          this.activiteService.getSuivis(this.projet.id, this.activites[i].id)
            .subscribe(suiviActivites => {
                this.activites[i].suiviActivites = suiviActivites;
              },
              error => {
                this.errorMessage = <any> error;
              }
            );
          this.activiteService.getActiviteRessources(this.activites[i].id.toString(), this.projet.id.toString())
            .subscribe( activiteRessources => {
              this.activites[i].activiteRessources = activiteRessources;
            },
              error => {
                console.log('************* error to get activiteRessources of activity *******');
                this.errorMessage = <any> error;
              }
            );
        }
      }
    );
  }

  getRessources(projet: number) {
    this.ressourceService.getRessources(this.projet.id)
      .subscribe(
      ressources => {
        this.projet.ressources = ressources;
        this.ressources = ressources;
        for (let k = 0; k < this.ressources.length; k++) {
          this.ressourceService.getSuivis(this.ressources[k].id, this.projet.id)
            .subscribe(suiviRessources => {
                this.ressources[k].suiviRessources = suiviRessources;
              },
              error => {
                console.log('************* error to get suiviRessources of ressource *******');
                this.errorMessage = <any> error;
              }
            );
        }
      },
        error => {
          console.log('************* error to get ressources of project *******');
          this.errorMessage = <any> error;
        }
    );
  }
  barChart(): void{
    let data = [];
    let colors = [];
    let labels = [];
    let  i=90;
    for (let lot of this.projet.lots) {
      data.push(i);
      colors.push('#7681E1');
      labels.push(lot.libelle);
      i=i-3;
    }
    this.barChartData = [
      {data: data, label: 'Evolution des lots', backgroundColor: colors},
    ];
    this.barChartLabels = labels;
  }
}
