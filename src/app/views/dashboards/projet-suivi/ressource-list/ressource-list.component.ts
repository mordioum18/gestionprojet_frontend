import {Component, Input, OnInit, OnChanges} from '@angular/core';
import {Activite} from '../../../../model/activite';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {ActiviteService} from '../../../../service/activite.service';
import {ActiviteRessource} from '../../../../model/activite-ressource';
import {Ressource} from '../../../../model/ressource';
import {RessourceService} from '../../../../service/ressource.service';
import {ActiviteRessourcePk} from '../../../../model/activite-ressource-pk';
@Component({
  selector: 'app-ressource-list',
  templateUrl: './ressource-list.component.html',
  styleUrls: ['./ressource-list.component.css'],
  providers: [ActiviteService, RessourceService]
})

export class RessourceListComponent implements OnInit, OnChanges {
  @Input() activite: Activite;
  activiteRessource: ActiviteRessource;
  titreModalActiviteRessource: string;
  modal = false;
  private errorMessage: string;
  assignedRessources: Ressource[];
  ressources: Ressource[];

  constructor(private activiteService: ActiviteService,
              private ressourceService: RessourceService,
              public ngxSmartModalService: NgxSmartModalService) {
  }
  ngOnInit(): void {
    this.activiteRessource = new ActiviteRessource();
    this.activiteRessource.pk = new ActiviteRessourcePk();
  }
  ngOnChanges() {
    if (this.activite !== undefined && this.activite !== null &&
        this.activite.id !== undefined && this.activite.id !== null) {
      this.activiteService.getActiviteRessources(this.activite.id.toString(), this.activite.lot.projet.id.toString())
        .subscribe( activiteRessources => {
            this.activite.activiteRessources = activiteRessources;
          },
          error => {
            console.log('************* error to get activiteRessources of activity *******');
            this.errorMessage = <any> error;
          }
        );
    }
  }

  modalOpen() {
    this.modal = true;
  }

  assignActiviteToRessource() {
    this.ressourceService.getRessources(this.activite.lot.projet.id)
      .subscribe( ressources => {
          this.ressources = ressources;
          console.log('success get all Ressources of a project');
          this.activiteService.getRessources(this.activite.id.toString(), this.activite.lot.projet.id.toString())
            .subscribe( assignedRessources => {
                this.assignedRessources = assignedRessources;
                console.log('success get assigned Ressources');
                for (let k = 0; k < this.assignedRessources.length; k++) {
                  let assignRes = this.assignedRessources[k];
                  for (let i = 0; i < this.ressources.length; i++) {
                    if ( this.ressources[i].id === assignRes.id) {
                      console.log('$$$$$$$$$$$$$$$ Ressource  ' + this.ressources[i].id +
                        '$$$$$$$$$$$$$$$ ASSIGNED£££££££    ' + assignRes.id);
                      this.ressources.splice(i, 1);
                    }
                  }
                }
              },
              (error) => {
                console.log('Erreur get assigned Ressources');
                this.errorMessage = error;
              }
            );
        },
        (error) => {
          console.log('Erreur get all Ressources of a project');
          this.errorMessage = error;
        }
      );
    this.titreModalActiviteRessource = 'Affecter cette activité à une ressource du projet';
    this.ngxSmartModalService.getModal('modalActiviteRessource').open();
  }

  addNewActiviteRessource() {
    this.ngxSmartModalService.getModal('modalActiviteRessource').close();
    this.activiteRessource = new ActiviteRessource();
    this.activiteRessource.pk = new ActiviteRessourcePk();
  }
}
