import {ActiviteService} from '../../../../service/activite.service';
import {SuiviActiviteService} from '../../../../service/suivi.activite.service';
import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {SuiviRessourceService} from '../../../../service/suivi-ressource.service';
import {RessourceService} from '../../../../service/ressource.service';
import {Ressource} from '../../../../model/ressource';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {SuiviRessource} from '../../../../model/suivi-ressource';
import {SuiviActivite} from '../../../../model/suivi-activite';

@Component({
  selector: 'app-table-suivi-ressource',
  templateUrl: './table-suivi-ressource.component.html',
  styleUrls: ['./table-suivi-ressource.component.css'],
  providers: [RessourceService, SuiviRessourceService],
})
export class TableSuiviRessourceComponent implements OnInit, OnChanges {
  @Input() ressource: Ressource;
  suiviRessource: SuiviRessource;
  titreModalSuiviRessource: string;
  modal = false;
  private errorMessage: string;
  constructor(private ressourceService: RessourceService,
              private suiviRessourceService: SuiviRessourceService,
              public ngxSmartModalService: NgxSmartModalService) {
  }

  ngOnInit(): void {
    this.suiviRessource = new SuiviRessource();
  }

  modalOpen() {
    this.modal = true;
  }
  ajouterSuiviRessource() {
    this.titreModalSuiviRessource = 'Suivi de la ressource';
    this.ngxSmartModalService.getModal('modalSuiviRessource').open();
  }
  addNewSuiviRessource(suiviRessource) {
    this.ngxSmartModalService.getModal('modalSuiviRessource').close();
    this.suiviRessource = new SuiviRessource();
  }

  onDelete(suiviRessource) {
    const $this = this;
    gantt.confirm({
      text: 'Voulez vous supprimer ce suivi sur cette ressource',
      ok: 'Ok',
      cancel: 'Annuler',
      callback: function(result){
        if (result === true) {
          $this.suiviRessourceService.deleteSuiviRessource(suiviRessource.id, $this.ressource.projet.id).subscribe(
            data => {
              for (let i = 0; i < $this.ressource.suiviRessources.length; i++) {
                if ($this.ressource.suiviRessources[i].id === suiviRessource.id) {
                  $this.ressource.suiviRessources.splice(i, 1);
                  break;
                }
              }
            }
          );
        }
      }
    });
  }

  ngOnChanges() {
  }

}
