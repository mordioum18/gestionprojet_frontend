import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {DecaissementService} from '../../../../service/decaissement.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Decaissement} from '../../../../model/decaissement';
import {Activite} from '../../../../model/activite';
import {
  Ng4FilesStatus,
  Ng4FilesSelected,
  Ng4FilesService,
  Ng4FilesConfig,
} from 'angular4-files-upload';
import {Document} from "../../../../model/document";

@Component({
  selector: 'app-form-decaissement',
  templateUrl: './form.decaissement.component.html',
  styleUrls: ['./form.decaissement.component.css'],
  providers: [Decaissement],
})

export class FormDecaissementComponent implements OnInit {
  errorMessage: any;
  @Input() activite: Activite;
  @Input() decaissement: Decaissement;
  @Output() retourDecaissement: EventEmitter<Decaissement> = new EventEmitter();
  formDecaissement: FormGroup;
  documents: Document[] = [];
  errorFile: string;
  documentConfig: Ng4FilesConfig = {
    acceptExtensions: ['pdf', 'PDF'],
    maxFilesCount: 10,
    maxFileSize: 2000000,
    totalFilesSize: 20000000
  };
  constructor(private decaissementService: DecaissementService,
              public ngxSmartModalService: NgxSmartModalService,
              private ng4FilesService: Ng4FilesService,
              private fb: FormBuilder) {
  }
  ngOnInit(): void {
    this.ng4FilesService.addConfig(this.documentConfig, 'document-config');
    this.formDecaissement = this.fb.group({
      'libelle': ['', Validators.required],
      'description': [''],
      'montant': ['', Validators.compose([Validators.required, Validators.pattern('((^[1-9][0-9]*$)|(^(0|([1-9][0-9]*))[.][0-9]{1,}$))')])],
      'dateDecaissement': ['', Validators.required],
    });
  }
  public filesSelect(selectedFiles: Ng4FilesSelected): void {
    if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_TOTAL_SIZE_EXCEED) {
      this.errorFile = 'trop de fichiers';
      return;
    }
    if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_COUNT_EXCEED) {
      this.errorFile = 'trop de fichier';
      return;
    }
    for (let i = 0 ; i < selectedFiles.files.length; i++){
      const reader = new FileReader();
      const file = selectedFiles.files[i];
      reader.readAsDataURL(file);
      reader.onload = () => {
        const doc = new Document();
        doc.chemin = reader.result;
        const tab = file.name.split('.');
        if (tab.length >= 2){
          const ext = tab[tab.length - 1];
          if (ext === 'pdf' || ext === 'PDF'){
            doc.extension = ext;
            doc.nom = file.name;
            this.documents.push(doc);
          }
        }
      };
    }
  }
  removeImage(i){
    this.documents.splice(i, 1);
  }
  private onSubmit(form){
    this.decaissement.activite = this.activite;
    this.decaissement.documents = [];
    for (let i = 0; i < this.documents.length; i++){
      const doc = this.documents[i];
      const base64Encode = doc.chemin.split(',');
      doc.chemin = base64Encode[1];
      doc.typeDocument = base64Encode[0];
      this.decaissement.documents.push(doc);
    }
    this.decaissementService.postDecaissement(this.decaissement).subscribe(
      data => {
        this.activite.decaissements.push(data);
        this.retourDecaissement.emit(data);
        this.documents = [];
        this.decaissement = new Decaissement();
      },
      (error) => {
        console.log('Erreur submit');
        this.documents = [];
        this.errorMessage = error;
      }
    );
  }
}
