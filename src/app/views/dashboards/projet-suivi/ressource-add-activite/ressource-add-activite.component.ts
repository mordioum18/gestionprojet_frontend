import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActiviteRessource} from '../../../../model/activite-ressource';
import {Activite} from '../../../../model/activite';
import {Ressource} from '../../../../model/ressource';
import {ActiviteService} from '../../../../service/activite.service';
import {RessourceService} from '../../../../service/ressource.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {ActiviteRessourcePk} from '../../../../model/activite-ressource-pk';

@Component({
  selector: 'app-ressource-add-activite',
  templateUrl: './ressource-add-activite.component.html',
  styleUrls: ['./ressource-add-activite.component.css'],
  providers: [RessourceService, ActiviteService]
})
export class RessourceAddActiviteComponent implements OnInit {
  errorMessage: any;
  @Input() activiteRessource: ActiviteRessource = new ActiviteRessource();
  @Input() activite: Activite;
  @Input() assignedRessources: Ressource[];
  @Input() ressources: Ressource[];
  @Output() retourActiviteRessource: EventEmitter<ActiviteRessource> = new EventEmitter();
  ressource: Ressource;
  formActiviteRessource: FormGroup;

  constructor(private activiteService: ActiviteService,
              private ressourceService: RessourceService,
              public ngxSmartModalService: NgxSmartModalService,
              private fb: FormBuilder) {
  }


  ngOnInit(): void {
    this.formActiviteRessource = this.fb.group({
      'quantite': ['', Validators.compose([Validators.required, Validators.pattern('(^[1-9][0-9]*$)')])],
      'coutParUnite': ['', Validators.compose([Validators.required, Validators.pattern('(^[1-9][0-9]*$)')])],
      'roleDansLaTache': [''],
      'ressource': [null, Validators.required]
    });
    this.activiteRessource.pk = new ActiviteRessourcePk();
    this.ressource = new Ressource();
  }

  private onSubmit(form) {
      console.log('$$$$$$$$$$$$$ unite  $$$$$$$$' + this.activiteRessource.quantite);
      console.log('$$$$$$$$$$$$$ cout $$$$$$$$' + this.activiteRessource.coutParUnite);
      console.log('$$$$$$$$$$$$$ role  $$$$$$$$' + this.activiteRessource.roleDansLaTache);
      console.log('============ ressource  ==========', this.ressource);
      this.activiteRessource.pk.activite = this.activite;
    this.activiteRessource.pk.ressource = this.ressource;
    this.activiteService.assignRessourceToActivite(this.activite.id.toString(),
                                                    this.activiteRessource,
                                                    this.activite.lot.projet.id.toString())
        .subscribe(
          data => {
            this.activite.activiteRessources.push(data);
            this.retourActiviteRessource.emit(data);
            this.activiteRessource.pk = new ActiviteRessourcePk();
            this.activiteRessource = new ActiviteRessource();
            this.ressource = new Ressource();
            this.ngxSmartModalService.getModal('modalActiviteRessource').close();
          },
          (error) => {
            console.log('Erreur submit activiteRessource');
            this.errorMessage = error;
            this.activiteRessource.pk = new ActiviteRessourcePk();
            this.activiteRessource = new ActiviteRessource();
            this.ressource = new Ressource();
          }
        );
  }

}
