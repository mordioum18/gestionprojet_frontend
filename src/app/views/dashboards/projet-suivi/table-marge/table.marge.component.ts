import {Component, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import {ActiviteService} from '../../../../service/activite.service';
import {DecaissementService} from '../../../../service/decaissement.service';
import {Activite} from '../../../../model/activite';
import {Decaissement} from '../../../../model/decaissement';
declare var jQuery:any;

@Component({
  selector: 'app-table-marge',
  templateUrl: './table.marge.component.html',
  styleUrls: ['./table.marge.component.css'],
  providers: [ActiviteService],
})

export class TableMargeComponent implements OnInit, OnChanges {
  @Input() activites: Activite[];
  private errorMessage: string;
  chargementFini = false;
  FTA: Date;
  constructor(private activiteService: ActiviteService){
  }
  ngOnInit(): void {
  }
  dateDTA(activite: Activite): Date{
    if (activite !== undefined && activite !== null &&
      activite.successeurs !== undefined && activite.successeurs !== null &&
      this.chargementFini){
      if (activite.successeurs.length === 0){
        const fin: any = this.FTA.getTime();
        return new Date(fin - activite.dureeExecutionPrevisionnelle*24*3600*1000);
      }
      else {
        if (activite.successeurs.length === 1){
          for (let i = 0; i < this.activites.length; i++){
            if(this.activites[i].id === activite.successeurs[0].id){
              const minDTA = this.dateDTA(this.activites[i]);
              return new Date(minDTA.getTime() - activite.dureeExecutionPrevisionnelle*24*3600*1000);
            }
          }
        }
        else {
          let minDTA: Date;
          for (let i = 0; i < this.activites.length; i++){
            if(this.activites[i].id === activite.successeurs[0].id){
              minDTA = this.dateDTA(this.activites[i]);
              break;
            }
          }
          for (let i = 1; i < activite.successeurs.length; i++){
            let dta: Date;
            for (let k = 0; k < this.activites.length; k++){
              if(this.activites[k].id === activite.successeurs[i].id){
                dta = this.dateDTA(this.activites[k]);
                if (minDTA !== undefined && minDTA.getTime() > dta.getTime()){
                  minDTA = dta;
                }
                break;
              }
            }
          }
          if (minDTA !== undefined){
            return new Date(minDTA.getTime() - activite.dureeExecutionPrevisionnelle*24*3600*1000);
          }
          else {
            return new Date();
          }
        }
      }
    }
    else {
      return new Date();
    }
  }
  dateFTA(activite: Activite): Date{
    if (activite !== undefined && activite !== null &&
      activite.successeurs !== undefined && activite.successeurs !== null &&
      this.chargementFini){
      const dta = this.dateDTA(activite).getTime();
      return new Date(dta + activite.dureeExecutionPrevisionnelle*24*3600*1000);
    }
    else {
      return new Date();
    }
  }
  margeTotale(activite: Activite): number{
    if (activite !== undefined && activite !== null &&
      activite.successeurs !== undefined && activite.successeurs !== null &&
      this.chargementFini){
      const dta = this.dateDTA(activite).getTime();
      const dto: any = activite.dateExecutionPrevisionnelle;
      return Math.floor((dta - dto) / (24 * 3600 * 1000));
    }
    else {
      return null;
    }
  }
  margeLibre(activite: Activite): number{
    if (activite !== undefined && activite !== null &&
      activite.successeurs !== undefined && activite.successeurs !== null &&
      this.chargementFini){
      if (activite.successeurs.length === 0){
        return 0;
      }
      else {
        if (activite.successeurs.length === 1){
          const dto: any = activite.successeurs[0].dateExecutionPrevisionnelle;
          const fto: any = activite.dateFinPrevisionnelle;
          return Math.floor((dto - fto) / (24 * 3600 * 1000));
        }
        else {
          const dto: any = activite.successeurs[0].dateExecutionPrevisionnelle;
          const fto: any = activite.dateFinPrevisionnelle;
          let minML = dto - fto;
          for (let i = 1; i < activite.successeurs.length; i++){
            const d: any = activite.successeurs[i].dateExecutionPrevisionnelle;
            const f: any = activite.dateFinPrevisionnelle;
            if (minML > (d - f)){
              minML = d - f;
            }
          }
          return Math.floor(minML / (24 * 3600 * 1000));
        }
      }
    }
    else {
      return null;
    }
  }
  iscritique(activite: Activite):string{
    if (this.margeTotale(activite) === 0){
      return 'bg-danger';
    }
    else {
      return null;
    }
  }

  ngOnChanges() {
      if (this.activites !== undefined && this.activites !== null){
        for (let i = 0; i < this.activites.length; i++){
          this.activiteService.getSuccesseurs(this.activites[i].lot.projet.id, this.activites[i].id)
            .subscribe(successeurs => {
                this.activites[i].successeurs = successeurs;
                if (i === (this.activites.length - 1)){
                  this.chargementFini = true;
                  this.FTA = new Date(this.activites[i].lot.projet.dateFinPrevisionnelle);
                }
              },
              error => {
                this.errorMessage = <any> error;
              }
            );
        }
      }
  }

}
