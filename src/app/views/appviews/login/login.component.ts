import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../../service/login.service';
import {UtilisateurService} from '../../../service/utilisateur.service';
import {PartageService} from '../../../service/partage.service';
import { Router } from '@angular/router';
import {Token} from "../../../model/Token";
@Component({
  selector: 'app-login',
  templateUrl: './login.template.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService, UtilisateurService],
})

export class LoginComponent implements OnInit {
  form: FormGroup;
  errorMessage: string;
  constructor(private fb: FormBuilder,
              private loginService: LoginService,
              private utilisateurService: UtilisateurService,
              private partageService: PartageService,
              private _router: Router) {
  }
  ngOnInit(): void {
    this.form = this.fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required],
    });
  }
  onSubmit(value): void {
    this.loginService
      .login(value.username, value.password)
      .subscribe(
        data => {
          const date = new Date();
          const token: Token = data;
          token.expires_at = date.getTime() + (1000 * token.expires_in);
          console.log(token);
          localStorage.setItem('token', JSON.stringify(data));
          this.getUtilisateur(token.access_token);
        },
        error => {
          this.errorMessage = 'combinaison incorrecte';
        }
      );
  }
  getUtilisateur(token: string){
    this.utilisateurService
      .getUtilisateurByToken(token)
      .subscribe(
        utilisateur => {
          localStorage.setItem('utilisateur', JSON.stringify(utilisateur));
          this._router.navigate(['/welcome']);
        },
        error => {
          this.errorMessage = 'combinaison incorrecte';
        }
      );
  }
}
