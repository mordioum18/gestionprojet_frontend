import {LoginService} from '../../../service/login.service';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import 'jquery-slimscroll';

declare var jQuery: any;

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css'],
  providers: [LoginService],
})
export class WelcomeComponent implements OnDestroy, OnInit{
  public nav:any;
  public body:any;
  constructor(private router: Router) {
    this.nav = document.querySelector('nav.navbar');
  }
  public ngOnInit():any {
    this.nav.className += " white-bg";
    this.body = document.querySelector('body');
  }


  public ngOnDestroy():any {
    this.nav.classList.remove("white-bg");
    this.body.classList.remove('top-navigation');
  }
}
