import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UtilisateurService} from '../../../service/utilisateur.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Utilisateur} from '../../../model/utilisateur';
declare var jQuery: any;
@Component({
  selector: 'app-login',
  templateUrl: './register.template.html',
  styleUrls: ['./register.component.css'],
  providers: [UtilisateurService],
})

export class RegisterComponent implements OnInit {
  form: FormGroup;
  formSendEmail: FormGroup;
  token: string;
  errorMessage: any;
  confirmationMotDepasse: string;
  errorToken = true;
  utilisateur: Utilisateur;
  constructor(private fb: FormBuilder,
              private utilisateurService: UtilisateurService,
              private route: ActivatedRoute,
              private router: Router) {
  }
  ngOnInit(): void {
    this.utilisateur = new Utilisateur();
    this.token = this.route.snapshot.paramMap.get('access');
    this.getUserByToken(this.token);
    this.form = this.fb.group({
      'prenom': ['', Validators.required],
      'nom': ['', Validators.required],
      'telephone': ['', Validators.required],
      'email': ['', Validators.compose([Validators.required, Validators.pattern('^[a-z0-9._-]{1,}@[a-z._-]{1,}[.][a-z]{1,5}$')])],
      'username': ['', Validators.required],
      'password': ['', Validators.required],
      'confirmationMotDepasse': ['', Validators.required],
    });
    this.formSendEmail = this.fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.pattern('^[a-z0-9._-]{1,}@[a-z._-]{1,}[.][a-z]{1,5}$')])],
    });
  }
  getUserByToken(token: string){
    this.utilisateurService
      .getUtilisateurByToken(token)
      .subscribe(
        data => {
          this.utilisateur = data;
          this.utilisateur.username = '';
          this.errorToken = false;
        },
        error => {
          this.errorToken = true;
        }
      );
  }
  formEmail(){
    jQuery('#formEmail').show();
    jQuery('#btnEmail').hide();
  }
  onSubmit(value): void {
    jQuery('#errorMotDepasse').hide();
    jQuery('#errorData').hide();
    if (this.utilisateur.passwordPlain === this.confirmationMotDepasse){
      this.utilisateurService
        .updateUtilisateur(this.utilisateur, this.token)
        .subscribe(
          data => {
            this.router.navigate(['login']);
          },
          error => {
            this.errorMessage = error;
            jQuery('#errorMotDepasse').hide();
            jQuery('#errorData').show();
            jQuery('#errorData').html(this.errorMessage.error.error.errorMessage);
          }
        );
    }
    else {
      jQuery('#errorMotDepasse').show();
      jQuery('#errorMotDepasse').html('les deux mots de passe ne correspondent pas');
      jQuery('#errorData').hide();
      this.utilisateur.passwordPlain = '';
      this.confirmationMotDepasse = '';
    }
  }
  newToken(value){
    this.utilisateurService
      .postUrl(value.email)
      .subscribe(
        data => {
          jQuery('#success').show();
          jQuery('#error').hide();
          jQuery('#success').html('Lien envoyé à l\'adresse email ' + value.email + '\n. Si vous ne recevez pas de message d\'ici 5 min envoyer à nouveau.');
        },
        error => {
          jQuery('#success').hide();
          jQuery('#error').show();
          jQuery('#error').html('Adresse email non autorisé');
        }
      );
  }
}
